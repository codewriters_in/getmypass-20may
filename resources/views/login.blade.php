@extends('f_layout')

@section('content')
<div class="new-slider" style="background-image:url({!! asset('assets/images/static-slider/slider10/img1.jpg') !!})">
	<div class="container">
		<!-- Row  -->
		<div class="row justify-content-center ">
			<!-- Column -->
			<div class="col-md-6 align-self-center text-center" data-aos="fade-down" data-aos-duration="1200">
				<span class="label label-rounded label-inverse">getmypass</span>
				<h1 class="title">Login</h1>
			</div>
			<!-- Column -->

		</div>
	</div>
</div>

<div class="contact1">
	<div class="row">
		<div class="container">
			<div class="spacer">
				<div class="row m-0">
					<div class="col-lg-7">
						<div class="contact-box p-r-40">
							<h3 class="title">Login</h3>
							<form class="form-horizontal" role="form" method="POST" action="{{ route('login_user') }}">
								{!! csrf_field() !!}

								<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
									<label class="col-md-4 control-label">{{ trans('backpack::base.email_address') }}</label>

									<div class="col-md-12">
										<input type="email" class="form-control" name="email" value="{{ old('email') }}" required>

										@if ($errors->has('email'))
										<span class="help-block">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
									<label class="col-md-4 control-label">{{ trans('backpack::base.password') }}</label>

									<div class="col-md-12">
										<input type="password" class="form-control" name="password" required>

										@if ($errors->has('password'))
										<span class="help-block">
											<strong>{{ $errors->first('password') }}</strong>
										</span>
										@endif
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-12 col-md-offset-4">
										<div class="checkbox">
											<label>
												<input type="checkbox" name="remember"> {{ trans('backpack::base.remember_me') }}
											</label>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-12 col-md-offset-4">
										<button type="submit" class="btn btn-primary">
											{{ trans('backpack::base.login') }}
										</button>

										<a class="btn btn-link" href="{{ route('backpack.auth.password.reset') }}">{{ trans('backpack::base.forgot_your_password') }}</a>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="col-lg-5">
						<div class="detail-box p-40 bg-info" data-aos="fade-right" data-aos-duration="1200">
							<h3 class="text-white">Order Summary</h3>
							<p class="text-white m-t-30 op-8">Ticket 1 [Rs.1000] x3</p>
							<p class="text-white op-8">Ticket 2 [Rs.400] x2</p>
							<h5 class="text-white">Total: Rs.4500</h5>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection