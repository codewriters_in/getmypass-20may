@extends('frontend.layouts.master')
@section('content')
<section id="subheader" style="background-image: url({{asset('assets/images/static-slider/slider10/img1.jpg')}});">
	<div class="container">
	  <div class="row">
	    <div class="col-md-12">
	      <h1>
	        Signup
	      </h1>
	    </div>
	    
	    <!-- devider -->
	    <div class="col-md-12 ">
	      <div class="devider-page">
	        <div class="devider-img-right">
	        </div>
	      </div>
	    </div>

	    <div class="col-md-12">
	      <ul class="subdetail">
	        <li>
	          <a href="{{route('home')}}">Home</a>
	        </li>

	        <li class="sep">/
	        </li>

	        <li>Signup
	        </li>
	      </ul>
	    </div>
	  </div>
	</div>
</section>

<section class="whitepage">
	<div class="container">
		<!-- Row  -->
		<div class="row justify-content-center ">
			<div class="col-lg-7 col-lg-offset-3">
				<div class="contact-box p-r-40">
					<h3 class="title">Sign Up</h3>
					<form class="form-horizontal" role="form" method="POST" action="{{ route('create_user') }}">
						{!! csrf_field() !!}

						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<label class="col-md-4 control-label">{{ trans('backpack::base.name') }}</label>

							<div class="col-md-10">
								<input type="text" class="form-control" name="name" value="{{ old('name') }}" required>

								@if ($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label class="col-md-4 control-label">{{ trans('backpack::base.email_address') }}</label>

							<div class="col-md-10">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}" required>

								@if ($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
								@endif
							</div>
						</div>
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Phone</label>

                            <div class="col-md-10">
                                <input type="tel" class="form-control" name="phone" value="{{ old('phone') }}" required>

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							<label class="col-md-4 control-label">{{ trans('backpack::base.password') }}</label>

							<div class="col-md-10">
								<input type="password" class="form-control" name="password" required>

								@if ($errors->has('password'))
								<span class="help-block">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
							<label class="col-md-4 control-label">{{ trans('backpack::base.confirm_password') }}</label>

							<div class="col-md-10">
								<input type="password" class="form-control" name="password_confirmation" required>

								@if ($errors->has('password_confirmation'))
								<span class="help-block">
									<strong>{{ $errors->first('password_confirmation') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-10">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-btn fa-user"></i> {{ trans('backpack::base.register') }}
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
jQuery(document).ready(function($) {
  var alterClass = function() {
    var ww = document.body.clientWidth;
    if (ww < 400) {
      $('#subheader').remove();
    } 
  };
  $(window).resize(function(){
    alterClass();
  });
  //Fire it when the page first loads:
  alterClass();
});

</script>
@endpush