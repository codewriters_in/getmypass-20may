@extends('frontend.layouts.master')
@section('content')
<section id="subheader" style="background-image: url({{asset('assets/images/static-slider/slider10/img1.jpg')}});">
	<div class="container">
	  <div class="row">
	    <div class="col-md-12">
	      <h1>
	        Add Event
	      </h1>
	    </div>
	    
	    <!-- devider -->
	    <div class="col-md-12">
	      <div class="devider-page">
	        <div class="devider-img-right">
	        </div>
	      </div>
	    </div>

	    <div class="col-md-12">
	      <ul class="subdetail">
	        <li>
	          <a href="{{route('home')}}">Home</a>
	        </li>

	        <li class="sep">/
	        </li>

	        <li>Register
	        </li>
	      </ul>
	    </div>
	  </div>
	</div>
</section>

<section class="whitepage">
	<div class="container">
		<!-- Row  -->
		<div class="row justify-content-center ">
			<!-- Column -->
			<div class="col-md-12 align-self-center text-center" data-aos="fade-down" data-aos-duration="1200">
				<span class="label label-rounded label-inverse">getmypass</span>
				<h1 class="title">List Your Event</h1>
				<h6 class="subtitle op-8">GetMyPass.in
					: an online free effortless and concise platform for the event organizers.
					Want to create awareness about your event, design passes and keep complete record of passes sold? 
					<br/>
					<a class="btn btn-outline-light btn-rounded btn-md btn-arrow m-t-20" data-toggle="collapse" href="">YESSS, GetMyPass is the best platform</a>
				</h6>
			</div>
				<!-- Column -->

		</div>
	</div>
	<div class="container">

		<div class="row">
			<div class="row">
				<div class="col-lg-7">
					@if (session()->has('success'))
					<div class="alert alert-success">
						{{ session()->get('success') }}
					</div>
					@endif
					@if ($errors->any())
						<div class="alert alert-danger">
					        {{ implode('', $errors->all(':message')) }}
					    </div>
					@endif        	

					@if (session()->has('error'))
					<div class="alert alert-danger">
						{{ session()->get('error') }}
					</div>
					@endif
					<br/>
					<div class="col-md-12">
						<h3 class="title">Register With Us</h3>
						<form class="form-horizontal" data-aos="fade-left" data-aos-duration="1200" method="POST" action="organiser-signup">
							{!! csrf_field() !!}
							<div class="col-lg-12">
								<div class="form-group">
	                  				
										<label for="org">Entity/Organisation/Club</label>
										<input class="form-control" type="text" name="org" placeholder="Enter Organisation or entity name" required>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="form-group">
	                  				
										<label for="name">Select City</label>
									  	<select class="form-control" name="city">
									  		@foreach($cities as $city)
										    	<option value="{{$city->id}}">{{$city->cname}}</option>
										    @endforeach
									  	</select>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="form-group">
	                  				
										<label for="name">Contact Name</label>
										<input class="form-control" type="text" name="name" placeholder="Enter Name" required>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="form-group">
	                  				
										<label for="email">Email ID</label>
										<input class="form-control" type="text" name="email" placeholder="Enter Email Address" required>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="form-group">
	                  				
										<label for="phone">Phone Number</label>
										<input class="form-control" type="text" name="phone" placeholder="Enter Phone Number" required>
								</div>
							</div>
							<div class="col-lg-12">

								<label for="facebook">Facebook Account<small> (Optional)</small></label>
								<div class="input-group">
									<span style="background-color: #eee;" class="input-group-addon">facebook.com/</span>
									<input class="form-control " placeholder="Username" name="facebook" type="text" id="facebook" >
								</div>
							</div>
							<div class="col-lg-12">
								
								<label for="facebook">Twitter Account<small> (Optional)</small></label>
								<div class="input-group">
									<span style="background-color: #eee;" class="input-group-addon">twitter.com/</span>
									<input class="form-control " placeholder="Username" name="tw_username" type="text" id="tw_username">
								</div>
							</div>
							
							<div class="col-lg-12">
								<div class="form-group">
									
									<label for="password">Password</label>
									<input class="form-control" type="password" name="password" placeholder="Enter Password" required>
								</div>
							</div>
							<div class="col-lg-12">
								<button type="submit" class="btn-content"><span> Register <i class="ti-arrow-right"></i></span></button>
							</div>
						</form>
					</div>
				</div>
				<div class="col-lg-5">
					<div class="detail-box p-40 bg-info" data-aos="fade-right" data-aos-duration="1200">
						<h3 class="text-white">Features</h3>
						<br/>
						<p class="text-white">1) Dashboard with complete integrated records of events,revenues and passes.</p>
						<p class="text-white">2) Create and customize events and variety of passes of every event in a single click.</p>
						<p class="text-white">3) Exclusive check-in facility with ticket generation and bar code generation.</p>
						<p class="text-white">4) Receive payment instantly from the customer directly in your bank account.</p>
						<p class="text-white">5) Secure and simple!! Takes down some pain from your shoulders so host a event from getmypass - your ticket managing partner..</p>
						<a class="btn btn-outline-light btn-rounded btn-md btn-arrow m-t-20" data-toggle="collapse" href=""><span>Contact Us</span></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection
@push('scripts')
<script type="text/javascript">
jQuery(document).ready(function($) {
  var alterClass = function() {
    var ww = document.body.clientWidth;
    if (ww < 400) {
      $('#subheader').remove();
    } 
  };
  $(window).resize(function(){
    alterClass();
  });
  //Fire it when the page first loads:
  alterClass();
});

</script>
@endpush