<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- Tell the browser to be responsive to screen width -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<!-- Favicon icon -->
		<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/favicon.png') }}">
		<title>GetMyPass - Book & Buy Tickets</title>
    	<link href="{{ asset('frontend/assets/css/bootstrap.min.css') }}" rel="stylesheet">
		<link rel="stylesheet" href="{{asset('frontend/assets/css/jquery.dateselect.css') }}">  
	    <!-- font themify CSS -->
	    <link rel="stylesheet" href="{{ asset('frontend/assets/css/themify-icons.css') }}">
	    <!-- font awesome CSS -->
	    <link href="{{ asset('frontend/assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
	    <!-- on3step CSS -->
	    <link href="{{ asset('frontend/assets/css/animated-on3step.css') }}" rel="stylesheet">
	    <link href="{{ asset('frontend/assets/css/owl.carousel.css') }}" rel="stylesheet">
	    <link href="{{ asset('frontend/assets/css/owl.theme.css') }}" rel="stylesheet">
	    <link href="{{ asset('frontend/assets/css/owl.transitions.css') }}" rel="stylesheet">
	    <link href="{{ asset('frontend/assets/css/on3step-style.css') }}" rel="stylesheet">
	    <link href="{{ asset('frontend/assets/css/queries-on3step.css') }}" media="all" rel="stylesheet" type="text/css">
			<link href="{{ asset('assets/css/design.css') }}" rel="stylesheet">

			@stack('styles')
	</head>
  <body>
    
    <!-- preloader -->
    <div class="bg-preloader-white"></div>
    <div class="preloader-white">
      <div class="mainpreloader">
        <span></span>
      </div>
    </div>
    <!-- preloader end -->
    
    <!-- content wraper -->
    <div class="content-wrapper">

    	@php
    	$class = \Request::route()->getName() == 'page.event' ? 'event' : '';
    	@endphp
	  	<header class="init {{$class}}">
					
			<!--------- NEW TABS ----------->
			<div class="tabs-nav org" style="display:none">
				<ul>
					<li>
						<a href="{{route('home')}}"><i class="fa fa-home" aria-hidden="true"></i></a>
					</li>
					<li>
						<a href="{{ route('page.event') }}"><i class="fa fa-calendar" aria-hidden="true"></i></a>
					</li>
					<li class="Account"> 
						<a href="#"><i class="fa fa-user" aria-hidden="true"></i></a>
						<div class="menu">
								@if(Auth::User())
									<a href="#">{{Auth::User()->name}}<i class="fa fa-angle-down"></i></a>
									<a href="{{ url('my-account') }}">Profile</a>
									<a href="{{ url('my-orders') }}">Orders</a>
									<a href="{{ route('logout_user') }}">Logout</a>
								@else
			                        <a href="{{ url('login') }}">Login</a>
			                        <a href="{{ url('register') }}">Register</a>
			                    @endif
							{{-- <a href="{{ url('login') }}">Login</a><hr>
							<a href="{{ url('register') }}">Register</a> --}}
						</div>
					</li>
					<li>
						<a href="{{ route('cart.index') }}"><i class="fa fa-shopping-cart " aria-hidden="true"></i>{{ Cart::instance('default')->count(false) }}</a>
						<span class="animated fadeIn slow"></span>
					</li>
					<li>
						<a href="{{ route('wishlist.index') }}"><i class="fa fa-heart" aria-hidden="true"></i>{{ Cart::instance('wishlist')->count(false) }}</a>
						<span class="animated fadeIn slow"></span>
					</li>
					<li>
						<a href="{{ route('create_organiser') }}"><i class="fa fa-list-alt" aria-hidden="true"></i></a>
					</li>
				</ul>
			</div>
			<!--------- ./NEW TABS ----------->

	        <!-- nav -->
	        <div class="navbar-default-white navbar-fixed-top navbar_">
	          <div class="container">
	            <div class="row">
	              <!-- menu mobile display -->
	              <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse">
	              <span class="icon icon-bar"></span>
	              <span class="icon icon-bar"></span>
	              <span class="icon icon-bar"></span></button>
	              
	              <!-- logo -->
	              <a class="navbar-brand white" href="{{route('home')}}">
	                <img class="white" alt="logo" src="{{ asset('assets/images/logos/black-logo.png')}}">
	                <img class="black" alt="logo" src="{{ asset('assets/images/logos/black-logo.png')}}">
	              </a>
	              <!-- logo end -->
	              @if(\Request::route()->getName() != "page.event")
	              <!-- mainmenu start -->
	              	<div class="white menu-init" id="main-menu">
		                <nav id="menu-center">
		                	<ul>
			                    <li><a class="actived" href="{{route('home')}}">Home</a> </li>
			                    <li><a href="{{ route('page.event') }}">Events</a></li>
								@if(Auth::User())
									<li>
										<a href="#">{{Auth::User()->name}}<i class="fa fa-angle-down"></i></a>
										<ul>
											<li><a href="{{ url('my-account') }}">Profile</a></li>
											<li><a href="{{ url('my-orders') }}">Orders</a></li>
											<li><a href="{{ route('logout_user') }}">Logout</a></li>
										</ul>
									</li>
								@else
				                    <li><a href="#">Account <i class="fa fa-angle-down"></i></a>
				                      <ul>
				                        <li><a href="{{ url('login') }}">Login</a></li>
				                        <li><a href="{{ url('register') }}">Register</a></li>
				                      </ul>
				                    </li>
			                    @endif
								<li>
									<a href="{{ route('cart.index') }}">Cart |<i class="fa fa-ticket"></i>{{ Cart::instance('default')->count(false) }}</a>
								</li>
								<li>
									<a href="{{ route('wishlist.index') }}">Wishlist | <i class="fa fa-heart"></i>{{ Cart::instance('wishlist')->count(false) }}</a>
								</li>
								<li>
									<a href="{{ route('create_organiser') }}"> List Your Event<i class="fa fa-plus"></i></a>
								</li>                  		
							</ul>
		                </nav>
	              	</div>
	              @elseif(\Request::route()->getName() == "page.event")
	              	<div class="white menu-init event" id="main-menu">
		            <div class="col-md-7 top-nav event"> 
		          		{{Form::open(['route' => 'page.event', 'method' => 'GET'])}}
			              <div class="row">
			                <div class="col-md-3 form-group">
			                  <select class="form-control" id="selectroom" name="city_id">
			                    <optgroup label="City">
			                    	<option value="">Select City</option>
			                   	@foreach($cities as $city)
			                      	<option value="{{$city->id}}">{{$city->cname}}</option>
			                    @endforeach
			                    </optgroup>
			                  </select>
			                </div>

			                <div class="col-md-3 form-group">
			                  <div class="input-group">
			                    <input type="text" name="date1" id="date" class="form-control fox1" placeholder="START DATE">
			                    <div class="input-group-btn">
			                      <span class="icon"><i class="fa fa-calendar"></i></span>
			                    </div>
			                  </div>
			                </div>

			                <div class="col-sm-3 form-group">
			                  <select class="form-control" id="selectroom" name="ticket_type">
			                    <optgroup label="TICKET TYPE">
		                    	  <option value="">Select Ticket</option>
			                      <option value="stag">STAG</option>
			                      <option value="couple">COUPLE</option>
			                      <option value="ladies">LADIES</option>
			                      <option value="guest">GUEST</option>
			                    </optgroup>
			                  </select>
			                </div>
			                <div class="col-sm-3 form-group">
			                	<button type="submit" class="btn-primary btn">Search</button>
			                </div>
			              </div>
			            {{Form::close()}}
		        	</div>
		                <nav id="menu-center">
		                	<ul>
			                    <li><a href="{{route('home')}}">Home</a> </li>
								@if(Auth::User())
									<li>
										<a href="#">{{Auth::User()->name}}<i class="fa fa-angle-down"></i></a>
										<ul>
											<li><a href="{{ url('my-account') }}">Profile</a></li>
											<li><a href="{{ url('my-orders') }}">Orders</a></li>
											<li><a href="{{ route('logout_user') }}">Logout</a></li>
										</ul>
									</li>
								@else
				                    <li><a href="#">Account <i class="fa fa-angle-down"></i></a>
				                      <ul>
				                        <li><a href="{{ url('login') }}">Login</a></li>
				                        <li><a href="{{ url('register') }}">Register</a></li>
				                      </ul>
				                    </li>
			                    @endif
								<li>
									<a href="{{ route('cart.index') }}"><i class="fa fa-ticket"></i>{{ Cart::instance('default')->count(false) }}</a>
								</li>
								<li>
									<a href="{{ route('wishlist.index') }}"><i class="fa fa-heart"></i>{{ Cart::instance('wishlist')->count(false) }}</a>
								</li>
							</ul>
		                </nav>
	              	</div>
		            <!-- mainmenu end -->
	        	  @endif
	            </div>
	          </div>
	        <!-- container -->
	        </div>
	      <!-- nav end -->
	  	</header>
	      <!-- home -->
	      <!-- background slider -->
	    @if(\Request::route()->getName() == "home")
		 	<div class="bgslider-owl" id="home">
		        <div id="owl-slider-home" class="owl-carousel">
		          	<div class="item imgbg" style="background-image:url({{asset('assets/images/static-slider/slider10/img1.jpg')}})">
			            <!-- intro -->
			            <div class="overlay-main v-align text-center ">
			              <div class="col-md-10 col-xs-11 onStep" data-animation="fadeInUp" data-time="300">
			                <h1>GETMYPASS</h1>
			                <h3 >Book & Buy Passes</h3>
			                <br>
			                
			              </div>
			            </div>
			            <!-- intro end -->
		          	</div>

		          	<div class="item imgbg" style="background-image:url({{asset('assets/images/static-slider/slider10/img1.jpg')}})">
			            <!-- intro -->
			            <div class="overlay-main v-align text-center ">
			              	<div class="col-md-10 col-xs-11">
				                
				                <h1>GETMYPASS</h1>
				                <h3 >Cause Standing in queue is old fashion</h3>
				                <br>
				                
				                
			              	</div>
			            </div>
			            <!-- intro end -->
		          	</div>
		        </div>
		 	</div>
		      <!-- background slider end -->
		      <!-- home end -->
		      <!-- booking home -->
		    <div class="bot-home-text onStep" data-animation="fadeInUp" data-time="600">
		        <div class="container">
					<div class="row hidden-sm hidden-md hidden-lg">
						<div class="col-xs-12 text-center">
							<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#ModalSearchForm"><i class="fa fa-2x fa-search"></i></button>
						</div>
					</div>
		          <div id="reservation" class="imgbgres hidden-xs" {{--style="background-image:url({{asset('assets/images/static-slider/slider10/img1.jpg')}})"--}}>
		          	{{Form::open(['route' => 'page.event', 'method' => 'GET', 'class' => 'form-horizontal'])}}
		              <div class="row">
		                <div class="col-sm-3 col-xs-4 form-group">
		                  <select class="form-control" id="selectroom" name="city_id">
		                    <optgroup label="City">
	                    	<option value="">Select City</option>
		                   	@foreach($cities as $city)
		                      	<option value="{{$city->id}}">{{$city->cname}}</option>
		                    @endforeach
		                    </optgroup>
		                  </select>
		                </div>
		                
		                <div class="col-md-3 col-xs-4 form-group">
		                  <div class="input-group">
		                    <input type="text" name="date1" id="date" class="form-control fox1" placeholder="START DATE">
		                    <div class="input-group-btn">
		                      <span class="icon"><i class="fa fa-calendar"></i></span>
		                    </div>
		                  </div>
		                </div>
		                
		                <div class="col-sm-3 col-xs-4 form-group">
		                  <select class="form-control" id="selectroom" name="ticket_type">
		                    <optgroup label="TICKET TYPE">
	                    	  <option value="">Select Ticket</option>
		                      <option value="stag">STAG</option>
		                      <option value="couple">COUPLE</option>
		                      <option value="ladies">LADIES</option>
		                      <option value="guest">GUEST</option>
		                    </optgroup>
		                  </select>
		                </div>
		                
		                <button type="submit" class="btn-content">Search</button>
					  </div>
		              </div>
		            {{Form::close()}}
		          </div>
		        </div>
		    </div>

			<div class="container">
				<div class="row">
					<div class="quick-search-container ">
						<div class="card-deck">
							<div class="card_bg">
								<div class="card quickBoxPanel">
									<div class="card-body text-center">
										<a href="{{route('page.event', ['date1' => date('Y-m-d', strtotime(today()))])}}">
											<p class="card-text">
												<img src="https://s3.ap-south-1.amazonaws.com/gpweb-angular4/today_sub_web.png">
												<span class="text_prop">
													Today
												</span>
											</p>
										</a>
									</div>
								</div>
							</div>
							<div class="card_bg">
								<div class="card quickBoxPanel">
									<div class="card-body text-center">
										<a href="{{route('page.event', ['date1' => date('Y-m-d', strtotime(today() . "+1 days"))])}}">
											<p class="card-text">
												<img src="https://s3.ap-south-1.amazonaws.com/gpweb-angular4/tomo_sub_web.png">
												<span class="text_prop">
													Tomorrow
												</span>
											</p>
										</a>
									</div>
								</div>
							</div>
							<div class="card_bg">
								<div class="card quickBoxPanel">
									<div class="card-body text-center">
										<a href="{{route('page.event', ['date' => "weekend"])}}">
											<p class="card-text">
												<img src="https://s3.ap-south-1.amazonaws.com/gpweb-angular4/week_sub_web.png">
												<span class="text_prop">
													Weekend
												</span>
											</p>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	    @endif

		@yield('content')
      	<!-- booking home end -->
		<section aria-label="footer" class="subfooter">
		  <div class="container">
		    <div class="row">
		      <div class="col-md-4 col-xs-12">
		        <div class="logo">
		          <img alt="logo" src="{{ asset('assets/images/logos/black-logo.png')}}">
		        </div>
		        <p>
		          Getmypass’ is an online ticket portal which serves as a bridge between a club and a user to connect and explore the nightlife of the city.

		        </p>
		      </div>
		      <div class="col-md-4 col-xs-12 col-md-offset-1">
		        <h3>
		        CONTACT INFO
		        </h3>
		        <address>
		          <span>5 JHA 35 Jawahar Nagar, Jaipur, Rajasthan, India 10903</span> <span>PHONE: +91 7976614292, 8112287886</span> <span>EMAIL : <a >rishabhmehta@getmypass.in , himanshudashwani@getmypass.in</a></span> <span>SITE : <a href="#">www.getmypass.in</a></span>
		        </address>
		      </div>
		      <div class="col-md-3 col-xs-12 pull-right">
		        <h3>
		        PHOTO STREAM
		        </h3>
		        <div id="flickr-photo-stream">
		          <script src="https://www.flickr.com/badge_code_v2.gne?count=8&amp;display=random&amp;size=s&amp;layout=x&amp;source=user&amp;user=52617155@N08" type="text/javascript">
		          </script>
		          <div class="clearfix">
		          </div>
		        </div>
		      </div>
		    </div>
		  </div>
		</section>
  		<!-- footer -->
	  <footer class="white">
	    <div class="container">
	      <div class="row">
	        <div class="col-md-6">
	          &copy; GetMyPass 2019.
	        </div>
	        <div class="col-md-6">
	          <div class="right">
	            <div class="social-icons">
	              <a href="#"><span class="ti-facebook"></span></a>
	              <a href="#"><span class="ti-dribbble"></span></a>
	              <a href="#"><span class="ti-twitter"></span></a>
	              <a href="https://instagram.com/getmypasss" target="_blank"><span class="ti-instagram"></span></a>
	              <a href="#"><span class="ti-linkedin"></span></a>
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
	  </footer>
	  <!-- footer end -->
	  <!-- ScrolltoTop -->
	  <div id="totop">
	    <span class="ti-angle-up"></span>
		</div>
		





	</div>	    
		<!-- plugin JS -->
	<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="{{ asset('frontend/assets/plugin/pluginson3step.js') }}" type="text/javascript"></script>
    <script src="{{ asset('frontend/assets/plugin/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/plugin/sticky.js') }}" type="text/javascript"></script> 
	
	<script type="text/javascript" src="{{ asset('frontend/assets/js/jquery.dateselect.js') }}"></script>
    <!-- slider revolution  -->
    <script type="text/javascript" src="{{ asset('frontend/assets/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
    <!-- on3step JS -->
    <script src="{{ asset('frontend/assets/js/on3step.js') }}" type="text/javascript"></script>
    <script src="{{ asset('frontend/assets/js/plugin-set.js') }}" type="text/javascript"></script>
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<script>
			$('.Account').click(function(){
				$('.menu').toggle()
			})
			</script>
    @include('sweet::alert')

    @stack('scripts')
	<!-- Modal HTML Markup -->
	<div id="ModalSearchForm" class="modal fade">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h1 class="modal-title">Search Events!</h1>
				</div>
				<div class="modal-body">
					{{Form::open(['route' => 'page.event', 'method' => 'GET', 'class' => 'form-horizontal'])}}
					<div class="row">
						<div class="col-sm-3 col-xs-12 form-group">
							<select class="form-control" id="selectroom" name="city_id">
								<optgroup label="City">
									<option value="">Select City</option>
									@foreach($cities as $city)
										<option value="{{$city->id}}">{{$city->cname}}</option>
									@endforeach
								</optgroup>
							</select>
						</div>

						<div class="col-md-3 col-xs-12 form-group">
							<div class="input-group">
								<input type="text" name="date1" id="date" class="form-control fox1" placeholder="START DATE">
								<div class="input-group-btn">
									<span class="icon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
						</div>

						<div class="col-sm-3 col-xs-12 form-group">
							<select class="form-control" id="selectroom" name="ticket_type">
								<optgroup label="TICKET TYPE">
									<option value="">Select Ticket</option>
									<option value="stag">STAG</option>
									<option value="couple">COUPLE</option>
									<option value="ladies">LADIES</option>
									<option value="guest">GUEST</option>
								</optgroup>
							</select>
						</div>

						<button type="submit" class="btn-content">Search</button>
					</div>
				</div>
				{{Form::close()}}


				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</body>
</html>



