@extends('frontend.layouts.master')
@section('content')
<section id="subheader" style="background-image: url({{asset('assets/images/static-slider/slider10/img1.jpg')}});">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>
            Wishlist
          </h1>
        </div>
        
        <!-- devider -->
        <div class="col-md-12">
          <div class="devider-page">
            <div class="devider-img-right">
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <ul class="subdetail">
            <li>
              <a href="{{route('home')}}">Home</a>
            </li>

            <li class="sep">/
            </li>

            <li>Wishlist
            </li>
          </ul>
        </div>
      </div>
    </div>
</section>
<section class="whitepage">

    <div class="container">
        @if (session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @endif

        @if (session()->has('error_message'))
            <div class="alert alert-danger">
                {{ session()->get('error_message') }}
            </div>
        @endif

        <h3>Items in your Wishlist</h3>
        <br>
        @if (sizeof(Cart::instance('wishlist')->content()) > 0)

            <table class="table">
                <thead>
                    <tr>
                        <th style="width: 40%">Ticket</th>

                        <th style="width: 20%">Price</th>
                        <th style="width: 20%">Event Status</th>
                        <th style="width: 20%"></th>
                    </tr>
                </thead>

                <tbody>
                    @foreach (Cart::instance('wishlist')->content() as $item)
                    <tr>
                        <td><a href="{{ url('events', [$item->model->slug]) }}">{{ $item->name }}</a></td>

                        <td>₹{{ $item->subtotal }}</td>
                        <td></td>
                        <td>
                        <div class="row">
                            <form action="{{ url('wishlist', [$item->rowId]) }}" method="POST" class="side-by-side">
                                {!! csrf_field() !!}
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="submit" class="btn btn-danger btn-sm" value="Remove">
                            </form>
                            &nbsp;
                            <form action="{{ url('switchToCart', [$item->rowId]) }}" method="POST" class="side-by-side">
                                {!! csrf_field() !!}
                                <input type="submit" class="btn btn-success btn-sm" value="Add To Cart">
                            </form>
                            </div>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>

            <div class="spacer"></div>

            <a href="/events" class="btn btn-primary">Continue Shopping</a> &nbsp;

            <div style="float:right">
                <form action="{{ url('/emptyWishlist') }}" method="POST">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" class="btn btn-danger" value="Empty Wishlist">
                </form>
            </div>

        @else

            <h5>You have no items in your Wishlist</h5>
            <br>
            <a href="/events" class="btn btn-success">Continue Shopping</a>

        @endif

        <div class="spacer"></div>

    </div> <!-- end container -->
</section>
@endsection
@push('scripts')
<script type="text/javascript">
jQuery(document).ready(function($) {
  var alterClass = function() {
    var ww = document.body.clientWidth;
    if (ww < 400) {
      $('#subheader').remove();
    } 
  };
  $(window).resize(function(){
    alterClass();
  });
  //Fire it when the page first loads:
  alterClass();
});

</script>
@endpush