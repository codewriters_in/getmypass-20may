@extends('f_layout')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/carousel.css')}}">
@section('content')

    <div class="panel panel-default panel-profile">
        @if(!empty($data->org_banner))
            <div class="panel-heading" style="background-image: url({!! asset($data->org_banner->banner_path . $data->org_banner->banner) !!})"></div>
            <div class="panel-body text-center">
                {{-- <span class="label label-rounded label-inverse"> --}}
                    <img class="panel-profile-img mb10" src="{{asset($data->org_banner->logo_path . $data->org_banner->logo)}}">
                {{-- </span> --}}
                {{-- <h3 class="panel-title"><strong>Hardy</strong></h3>
                <p>Senior Web Developer.</p>
                <a href="#" target="_blank" class="btn btn-primary btn-sm">
                    <span class="icon icon-add-user"></span> Read More
                </a> --}}
            </div>
        @endif
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-center">
                <h3 class="title">Upcoming Events</h3>
                <h6 class="subtitle">Choose from amazing events around you posted directly by organizers and be a part of fun  with friends & family.</h6>
            </div>
        </div>
        <div class="row m-t-40">
            @foreach($data->events as $event)
                <div class="movie-card org new">
                    <div class="movie-header manOfSteel" style="background-image: url({{asset($event->image)}});">
                        @if(!empty($event->event_highlight))
                            <div class="caption">
                                <a href="#" data-image-id="" data-toggle="modal" class="thumbnail btn btn-xs btn-danger " data-target="#image-gallery" data-title="" data-image="{{asset($event->event_highlight->collage_1)}}"> HIGHLIGHTS</a>

                                <a href="#" data-target="#image-gallery" data-title="" data-toggle="modal"  data-image-id="" data-image="{{asset($event->event_highlight->collage_2)}}" class="thumbnail">
                                    <img src="{{asset($event->event_highlight->collage_2)}}" style="display: none;">
                                </a>

                                <a href="#" data-target="#image-gallery" data-title="" data-toggle="modal"  data-image-id="" data-image="{{asset($event->event_highlight->collage_3)}}" class="thumbnail">
                                    <img src="{{asset($event->event_highlight->collage_3)}}" style="display: none;">
                                </a>
                                <a href="#" data-target="#image-gallery" data-toggle="modal"  data-image-id="" data-image="{{asset($event->event_highlight->collage_4)}}" class="thumbnail">
                                    <img src="{{asset($event->event_highlight->collage_4)}}" style="display: none;">
                                </a>
                                <a href="#" data-target="#image-gallery" data-toggle="modal"  data-image-id="" data-image="{{asset($event->event_highlight->collage_5)}}" class="thumbnail">
                                    <img src="{{asset($event->event_highlight->collage_5)}}" style="display: none;">
                                </a>
                            </div>
                        @endif
                        <div class="header-icon-container">
                            @php
                                $now = Carbon\Carbon::now();
                                $start_interval = $event->start_date->diffForHumans(null, true, true, 2);
                                $start_diff = $now->diff($event->start_date);
                                $end_interval = $event->end_date->diffForHumans(null, true, true, 2); 
                                $interval = $start_diff->format('%R%i');
                                // echo ($start_diff->format('%R%i')); die;
                            @endphp
                            <h6 class="card-label card-label-rounded">

                                {{$interval > 0 ? $start_interval . " To Go" : $end_interval . " To End" }}
                            </h6>
                        </div>
                    </div><!--movie-header-->
                    <div class="stats">
                        <div>
                            <strong>STAG</strong> {{isset($event->stag) ? ( $event->stag->price != 0 ? "₹ " . $event->stag->price : 'FREE') : "-"}} 
                        </div>
                        <div>
                            <strong>COUPLE</strong>  {{isset($event->couple) ? ($event->couple->price != 0 ? "₹ " . $event->couple->price: 'FREE') : "-"}} 
                        </div>
                        <div>
                            <strong>LADIES</strong> {{isset($event->ladies) ? ($event->ladies->price != 0 ? "₹ " . $event->ladies->price : 'FREE') : "-"}}
                        </div>
                    </div>

                    <div class="movie-content row">
                        <div class="date-panel col-2 ">
                            <p class="month-type">{{date('M', strtotime($event->start_date))}}</p>
                            <p class="date-type">{{date('d', strtotime($event->start_date))}}</p>
                            <p class="day-type">{{date('D', strtotime($event->start_date))}}</p>
                        </div>
                        <div class="detail-container col-10">
                            <a href="{{route('page.event.show', $event->id)}}"><h1 class="card-title"><span>{{ucfirst($event->title)}}</span></h1></a>
                            <div class="location">
                                <span>
                                    <font color="#0fb14a">{{ucfirst($event->venue_name)}} </font>, 
                                </span>
                                <p>{{$event->location_address_line_1}}</p>
                                        <a class="btn btn-default btn-xs">GUEST {{isset($event->guest_couple) || isset($event->guest_ladies) ? (isset($event->guest_couple->remaining_quantity) ? $event->guest_couple->remaining_quantity : 0 ) + (isset($event->guest_ladies->remaining_quantity) ? $event->guest_ladies->remaining_quantity :0) : "-"}}</a>

                                <a class="btn btn-{{isset($event->stag) && $event->stag->remaining_quantity > 0 ? 'success' : (isset($event->couple) && $event->couple->remaining_quantity > 0 ? 'success' : (isset($event->ladies) && $event->ladies->remaining_quantity > 0 ? 'success' : 'default'))}} btn-xs">TICKET</a>
                            </div>
                        </div>
                    </div><!--movie-content-->
                    <div class="row movie-content-footer">
                        

                    </div>
                </div><!--movie-card-->
            @endforeach
        </div>
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h3 class="title">Gallery</h3>
                <h6 class="subtitle">See our organization picture gallery.</h6>
            </div>
        </div>
        @if($data->org_gallery->count())
            <div class="row m-t-40">
                <div id="carouselExample" class="carousel slide" data-ride="carousel" data-interval="9000">
                    <div class="carousel-inner row w-100 mx-auto" role="listbox">
                        @foreach($data->org_gallery as $key => $gallery)

                            <div class="carousel-item col-md-3 ">
                                <div class="card ">
                                    <div class="card-body">
                                        <div class="myback-img ">
                                            <img src="{{asset($gallery->gallery_path . $gallery->gallery_image)}}" class="">
                                        </div>
                                        <div class="myoverlay"></div>
                                        {{-- <div class="profile-img">
                                            <div class="borders avatar-profile">
                                                <img src="https://images.pexels.com/photos/907267/pexels-photo-907267.jpeg?auto=compress&cs=tinysrgb&h=350">
                                            </div>
                                        </div> --}}
                                        <div class="profile-title">
                                            <a href="#">
                                                <h3>{{ucfirst($gallery->title)}}</h3>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next text-faded" href="#carouselExample" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h3 class="title">Past Events</h3>
                <h6 class="subtitle">See our past events.</h6>
            </div>
        </div>
        <div class="row m-t-40">
            @foreach($old_data->events as $old_event)
                <div class="movie-card org old">
                    <div class="movie-header manOfSteel" style="background-image: url({{asset($old_event->image)}});">
                        @if(!empty($old_event->event_highlight))
                            <div class="caption">
                                <a href="#" data-image-id="" data-toggle="modal" class="thumbnail btn btn-xs btn-danger " data-target="#image-gallery" data-title="" data-image="{{asset($old_event->event_highlight->collage_1)}}"> HIGHLIGHTS</a>

                                <a href="#" data-target="#image-gallery" data-title="" data-toggle="modal"  data-image-id="" data-image="{{asset($old_event->event_highlight->collage_2)}}" class="thumbnail">
                                </a>

                                <a href="#" data-target="#image-gallery" data-title="" data-toggle="modal"  data-image-id="" data-image="{{asset($old_event->event_highlight->collage_3)}}" class="thumbnail">
                                </a>
                                <a href="#" data-target="#image-gallery" data-toggle="modal"  data-image-id="" data-image="{{asset($old_event->event_highlight->collage_4)}}" class="thumbnail">
                                </a>
                                <a href="#" data-target="#image-gallery" data-toggle="modal"  data-image-id="" data-image="{{asset($old_event->event_highlight->collage_5)}}" class="thumbnail">
                                </a>
                            </div>
                        @endif
                        <div class="header-icon-container">
                            @php
                                $datetime1 = new DateTime();
                                $datetime2 = new DateTime($old_event->start_date);
                                $interval = $datetime1->diff($datetime2);
                                $days = $interval->format('%a');
                            @endphp
                            <h6 class="card-label card-label-rounded expired">Expired</h6>
                        </div>
                    </div><!--movie-header-->
                    <div class="stats">
                        <div>
                            <strong>STAG</strong> {{isset($old_event->stag) ? ( $old_event->stag->price != 0 ? "₹ " . $old_event->stag->price : 'FREE') : "-"}} 
                        </div>
                        <div>
                            <strong>COUPLE</strong>  {{isset($old_event->couple) ? ($old_event->couple->price != 0 ? "₹ " . $old_event->couple->price: 'FREE') : "-"}} 
                        </div>
                        <div>
                            <strong>LADIES</strong> {{isset($old_event->ladies) ? ($old_event->ladies->price != 0 ? "₹ " . $old_event->ladies->price : 'FREE') : "-"}}
                        </div>
                    </div>

                    <div class="movie-content row">
                        <div class="date-panel col-2 ">
                            <p class="month-type">{{date('M', strtotime($old_event->start_date))}}</p>
                            <p class="date-type">{{date('d', strtotime($old_event->start_date))}}</p>
                            <p class="day-type">{{date('D', strtotime($old_event->start_date))}}</p>
                        </div>
                        <div class="detail-container col-10">
                            <a href="{{route('page.event.show', $old_event->id)}}"><h1 class="card-title"><span>{{ucfirst($old_event->title)}}</span></h1></a>
                            <div class="location">
                                <span>
                                    <font color="#0fb14a">{{ucfirst($old_event->venue_name)}} </font>, 
                                </span>
                                <p>{{$old_event->location_address_line_1}}</p>
                                <a class="btn btn-default btn-xs">GUEST {{isset($old_event->guest_couple) || isset($old_event->guest_ladies) ? (isset($old_event->guest_couple->remaining_quantity) ? $old_event->guest_couple->remaining_quantity : 0 ) + (isset($old_event->guest_ladies->remaining_quantity) ? $old_event->guest_ladies->remaining_quantity :0) : "-"}}</a>

                                <a class="btn btn-default btn-xs">TICKET</a>
                            </div>
                        </div>
                    </div><!--movie-content-->
                    <div class="row movie-content-footer">
                        

                    </div>
                </div><!--movie-card-->
            @endforeach
        </div>

        <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="image-gallery-title"></h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                    </div>
                    <div class="modal-body" id="modal-body">
                        {{-- <img id="image-gallery-image" class="img-responsive col-md-12" src=""> --}}
                        {{-- <div class="embed-responsive embed-responsive-21by9" id="embed">
                            <iframe id="video-gallery-video" class="embed-responsive-item" src="" ></iframe>
                        </div> --}}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i class="fa fa-arrow-left"></i>
                        </button>

                        <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i class="fa fa-arrow-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>


    </div><!--container-->
{{-- </div> --}}


@endsection
@section('extra-js')
<script type="text/javascript">
    $(document).ready(function (){
        $('.carousel-inner').children('.carousel-item').first().addClass('active')

    });
    $('#carouselExample').on('slide.bs.carousel', function(e) {

        var $e = $(e.relatedTarget);
        var idx = $e.index();
        var itemsPerSlide = 4;
        var totalItems = $('.carousel-item').length;

        if (idx >= totalItems - (itemsPerSlide - 1)) {
            var it = itemsPerSlide - (totalItems - idx);
            for (var i = 0; i < it; i++) {
                // append slides to end
                if (e.direction == "left") {
                    $('.carousel-item').eq(i).appendTo('.carousel-inner');
                } else {
                    $('.carousel-item').eq(0).appendTo('.carousel-inner');
                }
            }
        }
    });

    $('#carouselExample').carousel({
        interval: 50000
    });


$( document ).ready(function() {
    $("[rel='tooltip']").tooltip();    
 
    $('.manOfSteel').hover(
        function(){
            $(this).find('.caption').slideDown(250); //.fadeIn(250)
        },
        function(){
            $(this).find('.caption').slideUp(250); //.fadeOut(205)
        }
    ); 
});


let modalId = $('#image-gallery');

$(document)
  .ready(function () {

    loadGallery(true, 'a.thumbnail');

    //This function disables buttons when needed
    function disableButtons(counter_max, counter_current) {
      $('#show-previous-image, #show-next-image')
        .show();
      if (counter_max === counter_current) {
        $('#show-next-image')
          .hide();
      } else if (counter_current === 1) {
        $('#show-previous-image')
          .hide();
      }
    }

    /**
     *
     * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
     * @param setClickAttr  Sets the attribute for the click handler.
     */

    function loadGallery(setIDs, setClickAttr) {
      let current_image,
        selector,
        counter = 0;

      $('#show-next-image, #show-previous-image')
        .click(function () {
          if ($(this)
            .attr('id') === 'show-previous-image') {
            current_image--;
          } else {
            current_image++;
          }

          selector = $('.active [data-image-id="' + current_image + '"]');
          updateGallery(selector);
        });

      function updateGallery(selector) {
        let $sel = selector;
        current_image = $sel.data('image-id');
        $('#image-gallery-title')
          .text($sel.data('title'));
          console.log(current_image);
          // console.log($sel[0].getAttribute('data-image'))
        if(current_image <= 5){
          // Check if its a video or not
            var check_type = $sel[0].getAttribute('data-image').match(/https:\/\/(:?www.)?(\w*)/);
            if (check_type == null || typeof check_type[1] === "undefined")
            { 
                if(document.getElementById('embed')) document.getElementById('embed').remove();
                if(document.getElementById('image-gallery-image')) document.getElementById('image-gallery-image').remove();
                if(current_image != 1 && document.getElementById("image-gallery-image")) document.getElementById('image-gallery-image').remove();
                var elem = document.createElement("img");
                elem.setAttribute("src", $sel.data('image'));
                elem.setAttribute("class", 'img-responsive');
                elem.setAttribute("id", 'image-gallery-image');
                document.getElementById("modal-body").appendChild(elem);
            }
            else if(check_type[2] == 'youtube' || check_type[2] == 'vimeo'){

                if(document.getElementById('image-gallery-image')) document.getElementById('image-gallery-image').remove();
                if(document.getElementById('embed')) document.getElementById('embed').remove();

                var velem = document.createElement('iframe');
                velem.setAttribute('src', $sel.data('image'));
                velem.setAttribute('class', 'embed-responsive-item');
                velem.setAttribute('id', 'video-gallery-video');

                var elem = document.createElement("div");
                elem.setAttribute("class", 'embed-responsive embed-responsive-21by9');
                elem.setAttribute("id", 'embed');
                elem.appendChild(velem);
                document.getElementById("modal-body").appendChild(elem);    
            }
        }
        disableButtons(counter, $sel.data('image-id'));
      }

      // if (setIDs == true) {
      //   $('[data-image-id]')
      //     .each(function () {
      //       counter++;
      //       console.log(counter);
      //       $(this)
      //         .attr('data-image-id', counter);
      //     });
      // }
      $(setClickAttr)
        .on('click', function () {
            var parent = $(this).closest('.movie-card').addClass('active');
            parent.siblings().removeClass('active');
            var old_class = $(this).parents('.old').attr('class')
            var new_class = $(this).parents('.new').attr('class')
            // console.log(current_class);
            if(typeof old_class === 'undefined'){

                $('.old').removeClass('active');
            }
            if(typeof new_class === 'undefined'){

                $('.new').removeClass('active');
            }

            if (setIDs == true) {
                $('.active .thumbnail[data-image-id]')
                  .each(function () {
                    if(counter < 5){
                        counter++;
                    }
                    else{
                        counter = 1;
                    }
                    console.log($(this));
                    console.log(counter);
                    $(this)
                      .attr('data-image-id', counter);
                  });
            }
          updateGallery($(this));
        });
    }
  });

// build key actions
$(document)
  .keydown(function (e) {
    switch (e.which) {
      case 37: // left
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
          $('#show-previous-image')
            .click();
        }
        break;

      case 39: // right
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
          $('#show-next-image')
            .click();
        }
        break;

      default:
        return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
  });

jQuery(document).ready(function($) {
  var alterClass = function() {
    var ww = document.body.clientWidth;
    if (ww < 400) {
      $('.subtitle').remove('h3');
    } 
  };
  $(window).resize(function(){
    alterClass();
  });
  //Fire it when the page first loads:
  alterClass();
});
</script>
@endsection
