@extends('frontend.layouts.master')
@section('content')
      <!-- subheader -->
  	<section id="subheader" style="background-image: url({{asset(!empty($data->image) ? ($data->image) : 'assets/images/static-slider/slider10/img1.jpg')}});">
        <div class="container">
          <div class="row">
			<div class="space-double"></div>
            <!-- devider -->
          </div>
        </div>
  	</section>
      <!-- subheader end -->

	<!-- services -->
	<section class="room-detail whitepage">
		<div class="onStep" data-animation="fadeInUp" data-time="300">
			<div class="container">
				<div class="row">
					
					<!-- text detail -->
					<div class="col-md-4">
						<div class="detaillist">
							<h2>{{ucfirst($data->title)}}</h2>
							<div class="devider-rooms-detail"></div>
							<p>{!! $data->description !!}</p>
							
							<div class="space-single"></div>
							
							<h2><i class="fa fa-map-marker" aria-hidden="true"></i>{!! $data->venue_name !!}</h2>
							<span>{{$data->location_address_line_1}}</span>,
							<span>{{$data->location_address_line_2}}</span>
							<div class="devider-rooms-detail"></div>
							<h3><i class="fa fa-calendar" aria-hidden="true"></i> Date & Time</h3>
							<span>
								{{date('d-M h:i A', strtotime($data->start_date))}}
								-
								{{date('d-M h:i A', strtotime($data->end_date))}}
							</span>
							{{-- <span><i class="ti-check-box"></i> Double Bed</span>
							<span><i class="ti-check-box"></i> Privacy Beach</span>
							<span><i class="ti-check-box"></i> Breakfast Include</span>
							<span><i class="ti-check-box"></i> Private Balcony</span>
							<span><i class="ti-check-box"></i> Free Wi-Fi</span>
							<span><i class="ti-check-box"></i> 4 Persons</span>
							<div class="price">$400<sub>/ night</sub></div> --}}
						</div>
					</div>
					<!-- text detail end -->
				<div class="col-md-2"></div>
				<div class="col-md-8 pricing-box align-self-center">
					@if($data->ticket->count())
						@foreach($data->ticket as $key => $ticket)
							<div class="card b-all m-15">
								<div class="card-body p-15">
									<form class="form-horizontal" role="form" action="{{ url('/cart') }}" method="POST">
										{!! csrf_field() !!}
										{{ Form::hidden('id', $ticket->id) }}
										{{ Form::hidden('name', $data->title) }}
										{{ Form::hidden('price', $ticket->price) }}
										{{ Form::hidden('event_id', $ticket->event_id) }}
										<div class="row">
											<div class="col-md-9">
												<div class="row">
													<div class="col-md-4">
														<h5>{{ucfirst($data->title)}}</h5>
														<span class="text-dark display-7">₹ {{$ticket->price}}</span>
													</div>
													<div class="col-md-4">
														<h5>Cover Amount</h5>
														<span class="text-dark display-7">₹ {{$ticket->cover_amount}}</span>
													</div>
													<div class="col-md-4">
														<h5>{{ucfirst($ticket->ticket_type)}}</h5>
														<h6>Entry for {{$ticket->no_of_person}}</h6>
													</div>
												</div>
											</div>
											<div class="col-md-3 m-t-40 pull-right cart">
											@if($ticket->for_sale == 'yes')
												@if($data->start_date > Carbon\Carbon::now())
													@if($ticket->ticket_type == 'guest_ladies' || $ticket->ticket_type == 'guest_couple')
														@php 
															$sale_end_date_time = new DateTime($ticket->sale_end_date_time);

															$current_time = new DateTime();
															$interval = $current_time->diff($sale_end_date_time);
															// dd($interval);
														@endphp

														@if($interval->format('%R%I') > 0)
															<button type="submit" class="btn btn-success-gradiant btn-arrow ">Free Booking</button>	
															<div id="entry_time_{{$key}}"></div>
														@else
															Expired!
														@endif
													@else
														<button type="submit" class="btn btn-success-gradiant btn-arrow ">Add To Cart</button>	
													@endif
												@else
													Expired!
												@endif
											@endif	
											</div>
										</div>
									</form>
								</div>

							</div>
						@endforeach
					@endif
				</div>
				<div class="space-single"></div>
			    <div id="accordion" class="panel-group">
			        <div class="panel panel-default">
			            <div class="panel-heading">
			                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
			                	<h4 class="panel-title">
			                    Venue Description
			                	</h4>
			                </a>
			            </div>

			            <div id="collapseOne" class="panel-collapse collapse in">
			                <div class="panel-body">
			                    <p>{!! $data->venue_description !!}</p>
			                </div>
			            </div>
			        </div>

			        <div class="panel panel-default">
			            <div class="panel-heading">
			            	<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
			                	<h4 class="panel-title">
			                    Terms & Condition
			                	</h4>
			                </a>
			            </div>

			            <div id="collapseTwo" class="panel-collapse collapse in">
			                <div class="panel-body">
			                    <p>{!! $data->terms !!}</p>
			                </div>
			            </div>
			        </div>

			        <div class="panel panel-default">
			            <div class="panel-heading">
			            	<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
			                	<h4 class="panel-title">
			                    For more personalized info
			                	</h4>
			                </a>	
			            </div>

			            <div id="collapseThree" class="panel-collapse collapse in">
			                <div class="panel-body">
			                    <p>{!! $data->personalized_info !!}</p>
			                </div>
			            </div>
			            
			        </div>
			    </div>
			</div>
		</div>
	</div>
</section>
@endsection
@push('scripts')
<script>
// Set the date we're counting down to
@foreach($data->ticket as $key => $ticket)
	@if($ticket->ticket_type == 'guest_ladies' || $ticket->ticket_type == 'guest_couple')
		@php 
			$sale_end_date_time = new DateTime($ticket->sale_end_date_time);

			$current_time = new DateTime();
			$interval = $current_time->diff($sale_end_date_time);
			// dd($interval);
		@endphp
		eventTimer("{{isset($sale_end_date_time) ? $sale_end_date_time->format('M d, Y H:i:s') : ""}}", "entry_time_{{$key}}");
	@endif
@endforeach
/**
 *  [eventTimer description]
 *
 *  @method eventTimer
 *
 *  @param  {[type]} sale_end_date_time [description]
 *  @param  {[type]} entry_time_id [description]
 *
 *  @return {[type]} [description]
 */

function eventTimer(sale_end_date_time, entry_time_id){

		var countDownDate = new Date(sale_end_date_time).getTime();
	// Update the count down every 1 second
	var x = setInterval(function() {

	  // Get todays date and time
	  var now = new Date().getTime();
	    
	  // Find the distance between now and the count down date
	  var distance = countDownDate - now;
	    
	  // Time calculations for days, hours, minutes and seconds
	  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
	  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
	  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
	    
	  // Output the result in an element with id="demo"
	  document.getElementById(entry_time_id).innerHTML = days + "d " + hours + "h "
	  + minutes + "m " + seconds + "s remaining";

	  
	}, 1000);

}

jQuery(document).ready(function($) {
  var alterClass = function() {
    var ww = document.body.clientWidth;
    if (ww < 400) {
      $('.cart').removeClass('pull-right');
    } 
  };
  $(window).resize(function(){
    alterClass();
  });
  //Fire it when the page first loads:
  alterClass();
});
</script>
@endpush