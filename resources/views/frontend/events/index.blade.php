@extends('frontend.layouts.master')
@section('content')
<section id="subheader" style="background-image: url({{asset('assets/images/static-slider/slider10/img1.jpg')}});">
	<div class="container">
	  <div class="row">
	    <div class="col-md-12">
	      <h1>
	        Events
	      </h1>
	    </div>
	    
	    <!-- devider -->
	    <div class="col-md-12">
	      <div class="devider-page">
	        <div class="devider-img-right">
	        </div>
	      </div>
	    </div>

	    <div class="col-md-12">
	      <ul class="subdetail">
	        <li>
	          <a href="{{route('home')}}">Home</a>
	        </li>

	        <li class="sep">/
	        </li>

	        <li>Events
	        </li>
	      </ul>
	    </div>
	  </div>
	</div>
</section>

<section class="whitepage">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				@if($data->count())
				    @foreach($data as $event)
				        <div class="movie-card">
				            <div class="movie-header manOfSteel" style="background-image: url({{asset($event->image)}});">
		                        @if(!empty($event->event_highlight))
		                        		{{-- {{dd($event->event_highlight)}} --}}
		                            <div class="caption">
		                                <a href="#" data-image-id="" data-toggle="modal" class="thumb btn btn-xs btn-danger " data-target="#image-gallery" data-title="" data-image="{{asset($event->event_highlight->collage_1)}}"> HIGHLIGHTS</a>

		                                <a href="#" data-target="#image-gallery" data-title="" data-toggle="modal"  data-image-id="" data-image="{{asset($event->event_highlight->collage_2)}}" class="thumb">
		                                </a>

		                                <a href="#" data-target="#image-gallery" data-title="" data-toggle="modal"  data-image-id="" data-image="{{asset($event->event_highlight->collage_3)}}" class="thumb">
		                                </a>
		                                <a href="#" data-target="#image-gallery" data-toggle="modal"  data-image-id="" data-image="{{asset($event->event_highlight->collage_4)}}" class="thumb">
		                                </a>
		                                <a href="#" data-target="#image-gallery" data-toggle="modal"  data-image-id="" data-image="{{asset($event->event_highlight->collage_5)}}" class="thumb">
		                                </a>
		                            </div>
		                        @endif
                        <div class="header-icon-container">
                            @php
                              $now = Carbon\Carbon::now();
                                $start_interval = $event->start_date->diffForHumans(null, true, true, 2);
                                $start_diff = $now->diff($event->start_date);
                                $end_interval = $event->end_date->diffForHumans(null, true, true, 2); 
                                $interval = $start_diff->format('%R%i');
                                // echo ($start_diff->format('%R%i')); die;
                            @endphp
                            <h6 class="card-label card-label-rounded">

                              {{$interval > 0 ? $start_interval . " To Go" : $end_interval . " To End" }}
                            </h6>
                        </div>
				            </div><!--movie-header-->
				            <div class="stats">
				                <div>
				                    <strong>STAG</strong> {{isset($event->stag) ? ( $event->stag->price != 0 ? "₹ " . $event->stag->price : 'FREE') : "-"}} 
				                </div>
				                <div>
				                    <strong>COUPLE</strong>  {{isset($event->couple) ? ($event->couple->price != 0 ? "₹ " . $event->couple->price : 'FREE') : "-"}} 
				                </div>
				                <div>
				                    <strong>LADIES</strong> {{isset($event->ladies) ? ($event->ladies->price != 0 ? "₹ " . $event->ladies->price : 'FREE') : "-"}}
				                </div>
				            </div>

				            <div class="movie-content row">
				                <div class="date-panel col-lg-2 col-md-2 col-sm-2 col-xs-2 ">
				                    <p class="month-type">{{date('M', strtotime($event->start_date))}}</p>
				                    <p class="date-type">{{date('d', strtotime($event->start_date))}}</p>
				                    <p class="day-type">{{date('D', strtotime($event->start_date))}}</p>
				                </div>
				                <div class="detail-container col-lg-10 col-md-10 col-sm-10 col-xs-10">
				                    <a href="{{route('page.event.show', $event->id)}}"><h1 class="card-title"><span>{{ucfirst($event->title)}}</span></h1></a>
				                    <div class="location">
				                        <span>
				                            <font color="#0fb14a">{{ucfirst($event->venue_name)}} </font>, 
				                        </span>
				                        <p>{{$event->location_address_line_1}}</p>
				                        <a class="btn btn-default btn-xs">GUEST {{isset($event->guest) ? $event->guest->remaining_quantity : "-"}}</a>

				                        <a class="btn btn-{{isset($event->stag) && $event->stag->remaining_quantity > 0 ? 'success' : (isset($event->couple) && $event->couple->remaining_quantity > 0 ? 'success' : (isset($event->ladies) && $event->ladies->remaining_quantity > 0 ? 'success' : 'default'))}} btn-xs">TICKET</a>
				                    </div>
				                </div>
				            </div><!--movie-content-->
				            <div class="row movie-content-footer">
				                

				            </div>
				        </div><!--movie-card-->
				    @endforeach
				@endif
			</div>
		</div>
        <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="image-gallery-title"></h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                    </div>
                    <div class="modal-body" id="modal-body">
                        {{-- <img id="image-gallery-image" class="img-responsive col-md-12" src=""> --}}
                        {{-- <div class="embed-responsive embed-responsive-21by9" id="embed">
                            <iframe id="video-gallery-video" class="embed-responsive-item" src="" ></iframe>
                        </div> --}}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i class="fa fa-arrow-left"></i>
                        </button>

                        <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i class="fa fa-arrow-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
	</div>
</section>

@endsection
@push('scripts')
<script type="text/javascript">
$( document ).ready(function() {
    $("[rel='tooltip']").tooltip();    
 
    $('.manOfSteel').hover(
        function(){
            $(this).find('.caption').slideDown(250); //.fadeIn(250)
        },
        function(){
            $(this).find('.caption').slideUp(250); //.fadeOut(205)
        }
    ); 
});
let modalId = $('#image-gallery');

$(document)
  .ready(function () {

    loadGallery(true, 'a.thumb');

    //This function disables buttons when needed
    function disableButtons(counter_max, counter_current) {
      $('#show-previous-image, #show-next-image')
        .show();
      if (counter_max === counter_current) {
        $('#show-next-image')
          .hide();
      } else if (counter_current === 1) {
        $('#show-previous-image')
          .hide();
      }
    }

    /**
     *
     * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
     * @param setClickAttr  Sets the attribute for the click handler.
     */

    function loadGallery(setIDs, setClickAttr) {
      let current_image,
        selector,
        counter = 0;

      $('#show-next-image, #show-previous-image')
        .click(function () {
          if ($(this)
            .attr('id') === 'show-previous-image') {
            current_image--;
          } else {
            current_image++;
          }

          selector = $('.active [data-image-id="' + current_image + '"]');
          updateGallery(selector);
        });

      function updateGallery(selector) {
        let $sel = selector;
        current_image = $sel.data('image-id');
        $('#image-gallery-title')
          .text($sel.data('title'));
          console.log(current_image);
          // console.log($sel[0].getAttribute('data-image'))
        if(current_image <= 5){
          // Check if its a video or not
            var check_type = $sel[0].getAttribute('data-image').match(/https:\/\/(:?www.)?(\w*)/);
            if (check_type == null || typeof check_type[1] === "undefined")
            { 
                if(document.getElementById('embed')) document.getElementById('embed').remove();
                if(document.getElementById('image-gallery-image')) document.getElementById('image-gallery-image').remove();
                if(current_image != 1 && document.getElementById("image-gallery-image")) document.getElementById('image-gallery-image').remove();
                var elem = document.createElement("img");
                elem.setAttribute("src", $sel.data('image'));
                elem.setAttribute("class", 'img-full');
                elem.setAttribute("id", 'image-gallery-image');
                document.getElementById("modal-body").appendChild(elem);
            }
            else if(check_type[2] == 'youtube' || check_type[2] == 'vimeo'){

                if(document.getElementById('image-gallery-image')) document.getElementById('image-gallery-image').remove();
                if(document.getElementById('embed')) document.getElementById('embed').remove();

                var velem = document.createElement('iframe');
                velem.setAttribute('src', $sel.data('image'));
                velem.setAttribute('class', 'embed-responsive-item');
                velem.setAttribute('id', 'video-gallery-video');

                var elem = document.createElement("div");
                elem.setAttribute("class", 'embed-responsive embed-responsive-16by9');
                elem.setAttribute("id", 'embed');
                elem.appendChild(velem);
                document.getElementById("modal-body").appendChild(elem);    
            }
        }
        disableButtons(counter, $sel.data('image-id'));
      }

      // if (setIDs == true) {
      //   $('[data-image-id]')
      //     .each(function () {
      //       counter++;
      //       console.log(counter);
      //       $(this)
      //         .attr('data-image-id', counter);
      //     });
      // }
      $(setClickAttr)
        .on('click', function () {
          var parent = $(this).closest('.movie-card').addClass('active');
          parent.siblings().removeClass('active')

          if (setIDs == true) {
            $('.active .thumb[data-image-id]')
              .each(function () {
                if(counter < 5){
                  counter++;
                }
                else{
                  counter = 1;
                }
                console.log(counter);
                $(this)
                  .attr('data-image-id', counter);
              });
          }
          updateGallery($(this));
        });
    }
  });

// build key actions
$(document)
  .keydown(function (e) {
    switch (e.which) {
      case 37: // left
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
          $('#show-previous-image')
            .click();
        }
        break;

      case 39: // right
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
          $('#show-next-image')
            .click();
        }
        break;

      default:
        return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
  });

jQuery(document).ready(function($) {
  var alterClass = function() {
    var ww = document.body.clientWidth;
    if (ww < 400) {
      $('#subheader').remove();
    } 
  };
  $(window).resize(function(){
    alterClass();
  });
  //Fire it when the page first loads:
  alterClass();
});

</script>
@endpush