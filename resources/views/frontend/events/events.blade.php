@extends('frontend.layouts.master')
@section('content')
<div class="space-double"></div>
<section class="whitepage">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				@if($data->count())
				    @foreach($data as $event)
				        <div class="movie-card">
				            <div class="movie-header manOfSteel" style="background-image: url({{asset($event->image)}});">
				                <div class="header-icon-container">
				                    @php
				                    	$now = Carbon\Carbon::now();
				                        $start_interval = $event->start_date->diffForHumans(null, true, true, 2);
				                        $start_diff = $now->diff($event->start_date);
				                        $end_interval = $event->end_date->diffForHumans(null, true, true, 2); 
				                        $interval = $start_diff->format('%R%i');
				                        // echo ($start_diff->format('%R%i')); die;
				                    @endphp
				                    <h6 class="card-label card-label-rounded">

				                    	{{$interval > 0 ? $start_interval . " To Go" : $end_interval . " To End" }}
				                    </h6>
				                </div>
				            </div><!--movie-header-->
				            <div class="stats">
				                <div>
				                    <strong>STAG</strong> {{isset($event->stag) ? ( $event->stag->price != 0 ? "₹ " . $event->stag->price : 'FREE') : "-"}} 
				                </div>
				                <div>
				                    <strong>COUPLE</strong>  {{isset($event->couple) ? ($event->couple->price != 0 ? "₹ " . $event->couple->price : 'FREE') : "-"}} 
				                </div>
				                <div>
				                    <strong>LADIES</strong> {{isset($event->ladies) ? ($event->ladies->price != 0 ? "₹ " . $event->ladies->price : 'FREE') : "-"}}
				                </div>
				            </div>

				            <div class="movie-content row">
				                <div class="date-panel col-lg-2 col-md-2 col-sm-2 col-xs-2 hidden-xs">
				                    <p class="month-type">{{date('M', strtotime($event->start_date))}}</p>
				                    <p class="date-type">{{date('d', strtotime($event->start_date))}}</p>
				                    <p class="day-type">{{date('D', strtotime($event->start_date))}}</p>
				                </div>
				                <div class="detail-container col-lg-10 col-md-10 col-sm-10 col-xs-12">
				                    <a href="{{route('page.event.show', $event->id)}}"><h1 class="card-title"><span>{{ucfirst($event->title)}}</span></h1></a>
				                    <div class="location">
				                        <span>
				                            <font color="#0fb14a">{{ucfirst($event->venue_name)}} </font>, 
				                        </span>
				                        <p>{{$event->location_address_line_1}}</p>
				                        <a class="btn btn-default btn-xs">GUEST {{isset($event->guest_couple) || isset($event->guest_ladies) ? (isset($event->guest_couple->remaining_quantity) ? $event->guest_couple->remaining_quantity : 0 ) + (isset($event->guest_ladies->remaining_quantity) ? $event->guest_ladies->remaining_quantity :0) : "-"}}</a>

				                        <a class="btn btn-{{isset($event->stag) && $event->stag->remaining_quantity > 0 ? 'success' : (isset($event->couple) && $event->couple->remaining_quantity > 0 ? 'success' : (isset($event->ladies) && $event->ladies->remaining_quantity > 0 ? 'success' : 'default'))}} btn-xs">TICKET</a>
				                    </div>
				                </div>
				            </div><!--movie-content-->
				            <div class="row movie-content-footer">
				                

				            </div>
				        </div><!--movie-card-->
				    @endforeach
				@endif
			</div>
		</div>
	</div>
</section>

@endsection