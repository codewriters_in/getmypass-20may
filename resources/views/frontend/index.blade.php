@extends('frontend.layouts.master')
@push('styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/carousel.css')}}">
@endpush
@section('content')
<section class="whitepage">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="text-center">
				  	<h2>Events & Tickets</h2>
				  	<span class="devider-center"></span>
				  	<h3  class="head desc">Book tickets for events around you in few clicks. Pay easily and receive tickets directly into your inbox through SMS & Email.<br/> Happy Booking :)</h3>
				</div>
				@if($data->count())
				    @foreach($data as $event)
				        <div class="movie-card new">
				            <div class="movie-header manOfSteel" style="background-image: url({{asset($event->image)}});">
		                        @if(!empty($event->event_highlight))

		                            <div class="caption">
		                                <a data-image-id="" data-toggle="modal" class="thumb btn btn-xs btn-danger " data-target="#image-gallery" data-title="" data-image="{{asset($event->event_highlight->collage_1)}}"> HIGHLIGHTS</a>

		                                <a data-target="#image-gallery" data-title="" data-toggle="modal"  data-image-id="" data-image="{{asset($event->event_highlight->collage_2)}}" class="thumb">
		                                </a>

		                                <a data-target="#image-gallery" data-title="" data-toggle="modal"  data-image-id="" data-image="{{asset($event->event_highlight->collage_3)}}" class="thumb">
		                                </a>
		                                <a data-target="#image-gallery" data-toggle="modal"  data-image-id="" data-image="{{asset($event->event_highlight->collage_4)}}" class="thumb">
		                                </a>
		                                <a data-target="#image-gallery" data-toggle="modal"  data-image-id="" data-image="{{asset($event->event_highlight->collage_5)}}" class="thumb">
		                                </a>
		                            </div>
		                        @endif
				                <div class="header-icon-container">
				                    @php
				                    	$now = Carbon\Carbon::now();
				                        $start_interval = $event->start_date->diffForHumans(null, true, true, 2);
				                        $start_diff = $now->diff($event->start_date);
				                        $end_interval = $event->end_date->diffForHumans(null, true, true, 2); 
				                        $interval = $start_diff->format('%R%i');
				                        // echo ($start_diff->format('%R%i')); die;
				                    @endphp
				                    <h6 class="card-label card-label-rounded">

				                    	{{$interval > 0 ? $start_interval . " To Go" : $end_interval . " To End" }}
				                    </h6>
				                </div>
				            </div><!--movie-header-->
				            <div class="stats">
				                <div>
				                    <strong>STAG</strong> {{isset($event->stag) ? ( $event->stag->price != 0 ? "₹ " . $event->stag->price : 'FREE') : "-"}} 
				                </div>
				                <div>
				                    <strong>COUPLE</strong>  {{isset($event->couple) ? ($event->couple->price != 0 ? "₹ " . $event->couple->price : 'FREE') : "-"}} 
				                </div>
				                <div>
				                    <strong>LADIES</strong> {{isset($event->ladies) ? ($event->ladies->price != 0 ? "₹ " . $event->ladies->price : 'FREE') : "-"}}
				                </div>
				            </div>

				            <div class="movie-content row">
				                <div class="date-panel col-lg-2 col-md-2 col-sm-2 col-xs-2 ">
				                    <p class="month-type">{{date('M', strtotime($event->start_date))}}</p>
				                    <p class="date-type">{{date('d', strtotime($event->start_date))}}</p>
				                    <p class="day-type">{{date('D', strtotime($event->start_date))}}</p>
				                </div>
				                <div class="detail-container col-lg-10 col-md-10 col-sm-10 col-xs-10">
				                    <a href="{{route('page.event.show', $event->id)}}"><h1 class="card-title"><span>{{ucfirst($event->title)}}</span></h1></a>
				                    <div class="location">
				                        <span>
				                            <font color="#0fb14a">{{ucfirst($event->venue_name)}} </font>, 
				                        </span>
				                        <p>{{$event->location_address_line_1}}</p>
				                        <a class="btn btn-default btn-xs">GUEST {{isset($event->guest_couple) || isset($event->guest_ladies) ? (isset($event->guest_couple->remaining_quantity) ? $event->guest_couple->remaining_quantity : 0 ) + (isset($event->guest_ladies->remaining_quantity) ? $event->guest_ladies->remaining_quantity :0) : "-"}}</a>

				                        <a class="btn btn-{{isset($event->stag) && $event->stag->remaining_quantity > 0 ? 'success' : (isset($event->couple) && $event->couple->remaining_quantity > 0 ? 'success' : (isset($event->ladies) && $event->ladies->remaining_quantity > 0 ? 'success' : 'default'))}} btn-xs">TICKET</a>
				                    </div>
				                </div>
				            </div><!--movie-content-->
				            <div class="row movie-content-footer">
				                

				            </div>
				        </div><!--movie-card-->
				    @endforeach
				@endif
			</div>
		</div>
	</div>
</section>
<!-- subscribe end -->
<section class="whitepage">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="text-center">
          <h2>Clubs</h2>
          <span class="devider-center"></span>
          <h3 class="head desc">Checkout the clubs around you.</h3>
        </div>
      </div>
      <!-- spacer -->
      <div class="space-double"></div>
      <!-- spacer -->
      <div class="onStep" data-animation="fadeInUp" data-time="300">
        <div class="owl-carousel" id="owl-post">
          @if($org_data->count())
          	@foreach($org_data as $org)
		        <div class="movie-card club">
		            <div class="movie-header manOfSteel" style="background-image: url({{asset(!empty($org->org_banner) ? $org->org_banner->banner_path . $org->org_banner->banner : "/frontend/assets/images/image.png" )}});">
						{{-- <div class="text-center pt-80">
							@if(!empty($org->org_banner->logo))
                            	<a href="{{route('frontend.organizer', $org->org_id)}}"> <img src="{{asset($org->org_banner->logo_path . $org->org_banner->logo)}}" class="img-thumb img-circle img-xs"></a>
                            @endif
                        </div> --}}		            
                    </div><!--movie-header-->
		            <div class="movie-content row">
		                {{-- <div class="date-panel col-lg-2 col-md-2 col-sm-2 col-xs-2 ">
		                    <p class="month-type">{{date('M', strtotime($org->start_date))}}</p>
		                    <p class="date-type">{{date('d', strtotime($org->start_date))}}</p>
		                    <p class="day-type">{{date('D', strtotime($org->start_date))}}</p>
		                </div> --}}
		                <div class="detail-container col-lg-10 col-md-10 col-sm-10 col-xs-10">
		                    <a href="{{route('frontend.organizer', $org->org_id)}}"><h1 class="card-title"><span>{{ucfirst($org->org)}}</span></h1></a>
		                    <div class="location">
		                        <span>
		                            <font color="#0fb14a">{{ucfirst($org->place)}} </font>, 
		                        </span>
		                        <p>{{ucfirst($org->city->cname)}}</p>
		                        
		                    </div>
		                </div>
		            </div><!--movie-content-->
		            <div class="row movie-content-footer">
		                

		            </div>
		        </div><!--movie-card-->
	        @endforeach
          @endif
        </div>
      </div>
    </div>
  </div>
</section>
<!-- section about end -->
<section class="whitepage">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="text-center">
				  	<h2>Past Events</h2>
				  	<span class="devider-center"></span>
				  	<h3 class="head desc">See our past events.</h3>
				</div>
				@if($old_data->count())
				    @foreach($old_data as $old_event)
		                <div class="movie-card old">
		                    <div class="movie-header manOfSteel" style="background-image: url({{asset($old_event->image)}});">

		                        @if(!empty($old_event->event_highlight))
		                            <div class="caption">
		                                <a data-image-id="" data-toggle="modal" class="thumb btn btn-xs btn-danger " data-target="#image-gallery" data-title="" data-image="{{asset($old_event->event_highlight->collage_1)}}"> HIGHLIGHTS</a>

		                                <a data-target="#image-gallery" data-title="" data-toggle="modal"  data-image-id="" data-image="{{asset($old_event->event_highlight->collage_2)}}" class="thumb">
		                                </a>

		                                <a data-target="#image-gallery" data-title="" data-toggle="modal"  data-image-id="" data-image="{{asset($old_event->event_highlight->collage_3)}}" class="thumb">
		                                </a>
		                                <a data-target="#image-gallery" data-toggle="modal"  data-image-id="" data-image="{{asset($old_event->event_highlight->collage_4)}}" class="thumb">
		                                </a>
		                                <a data-target="#image-gallery" data-toggle="modal"  data-image-id="" data-image="{{asset($old_event->event_highlight->collage_5)}}" class="thumb">
		                                </a>
		                            </div>
		                        @endif
		                        <div class="header-icon-container">
		                            <h6 class="card-label card-label-rounded expired">Expired</h6>
		                        </div>
		                    </div><!--movie-header-->
				            <div class="stats">
				                <div>
				                    <strong>STAG</strong> {{isset($old_event->stag) ? ( $old_event->stag->price != 0 ? "₹ " . $old_event->stag->price : 'FREE') : "-"}} 
				                </div>
				                <div>
				                    <strong>COUPLE</strong>  {{isset($old_event->couple) ? ($old_event->couple->price != 0 ? "₹ " . $old_event->couple->price: 'FREE') : "-"}} 
				                </div>
				                <div>
				                    <strong>LADIES</strong> {{isset($old_event->ladies) ? ($old_event->ladies->price != 0 ? "₹ " . $old_event->ladies->price : 'FREE') : "-"}}
				                </div>
				            </div>
		                    <div class="movie-content row">
		                        <div class="date-panel col-lg-2 col-md-2 col-sm-2 col-xs-2 ">
		                            <p class="month-type">{{date('M', strtotime($old_event->start_date))}}</p>
		                            <p class="date-type">{{date('d', strtotime($old_event->start_date))}}</p>
		                            <p class="day-type">{{date('D', strtotime($old_event->start_date))}}</p>
		                        </div>
		                        <div class="detail-container col-lg-10 col-md-10 col-sm-10 col-xs-10">
		                            <a href="{{route('page.event.show', $old_event->id)}}"><h1 class="card-title"><span>{{ucfirst($old_event->title)}}</span></h1></a>
		                            <div class="location">
		                                <span>
		                                    <font color="#0fb14a">{{ucfirst($old_event->venue_name)}} </font>, 
		                                </span>
		                                <p>{{$old_event->location_address_line_1}}</p>
                                		<a class="btn btn-default btn-xs">GUEST {{isset($old_event->guest_couple) || isset($old_event->guest_ladies) ? (isset($old_event->guest_couple->remaining_quantity) ? $old_event->guest_couple->remaining_quantity : 0 ) + (isset($old_event->guest_ladies->remaining_quantity) ? $old_event->guest_ladies->remaining_quantity :0) : "-"}}</a>

		                                <a class="btn btn-default btn-xs">TICKET</a>
		                            </div>
		                        </div>
		                    </div><!--movie-content-->
		                    <div class="row movie-content-footer">
		                        

		                    </div>
		                </div><!--movie-card-->
				    @endforeach
				@endif
			</div>
		</div>
        <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="image-gallery-title"></h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                    </div>
                    <div class="modal-body" id="modal-body">
                        {{-- <img id="image-gallery-image" class="img-responsive col-md-12" src=""> --}}
                        {{-- <div class="embed-responsive embed-responsive-21by9" id="embed">
                            <iframe id="video-gallery-video" class="embed-responsive-item" src="" ></iframe>
                        </div> --}}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i class="fa fa-arrow-left"></i>
                        </button>

                        <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i class="fa fa-arrow-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
	</div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
$( document ).ready(function() {
    $("[rel='tooltip']").tooltip();    
 
    $('.manOfSteel').hover(
        function(){
            $(this).find('.caption').slideDown(250); //.fadeIn(250)
        },
        function(){
            $(this).find('.caption').slideUp(250); //.fadeOut(205)
        }
    ); 
});
let modalId = $('#image-gallery');

$(document)
  .ready(function () {

    loadGallery(true, 'a.thumb');

    //This function disables buttons when needed
    function disableButtons(counter_max, counter_current) {
      $('#show-previous-image, #show-next-image')
        .show();
      if (counter_max === counter_current) {
        $('#show-next-image')
          .hide();
      } else if (counter_current === 1) {
        $('#show-previous-image')
          .hide();
      }
    }

    /**
     *
     * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
     * @param setClickAttr  Sets the attribute for the click handler.
     */

    function loadGallery(setIDs, setClickAttr) {
      let current_image,
        selector,
        counter = 0;

      $('#show-next-image, #show-previous-image')
        .click(function () {
          if ($(this)
            .attr('id') === 'show-previous-image') {
            current_image--;
          } else {
            current_image++;
          }

          selector = $('.movie-card.active [data-image-id="' + current_image + '"]');
          updateGallery(selector);
        });

      function updateGallery(selector) {
        let $sel = selector;
        current_image = $sel.data('image-id');
        $('#image-gallery-title')
          .text($sel.data('title'));
          console.log(current_image);
          // console.log($sel[0].getAttribute('data-image'))
        if(current_image <= 5){
          // Check if its a video or not
            var check_type = $sel[0].getAttribute('data-image').match(/https:\/\/(:?www.)?(\w*)/);
            console.log(check_type);
            if (check_type == null || typeof check_type[1] === "undefined")
            { 
                if(document.getElementById('embed')){ 
                	document.getElementById('embed').remove();
               	}
                if(document.getElementById('image-gallery-image')){ 
                	document.getElementById('image-gallery-image').remove();
                }
                if(current_image != 1 && document.getElementById("image-gallery-image")){document.getElementById('image-gallery-image').remove();
            	}
                var elem = document.createElement("img");
                elem.setAttribute("src", $sel.data('image'));
                elem.setAttribute("class", 'img-full');
                elem.setAttribute("id", 'image-gallery-image');
                document.getElementById("modal-body").appendChild(elem);
            }
            else if(check_type[2] == 'youtube' || check_type[2] == 'vimeo'){

                if(document.getElementById('image-gallery-image')){
                	document.getElementById('image-gallery-image').remove();
                }

                if(document.getElementById('embed')) {
                	document.getElementById('embed').remove();
                }

                var velem = document.createElement('iframe');
                velem.setAttribute('src', $sel.data('image'));
                velem.setAttribute('class', 'embed-responsive-item');
                velem.setAttribute('id', 'video-gallery-video');

                var elem = document.createElement("div");
                elem.setAttribute("class", 'embed-responsive embed-responsive-16by9');
                elem.setAttribute("id", 'embed');
                elem.appendChild(velem);
                document.getElementById("modal-body").appendChild(elem);    
            }
        }
        disableButtons(counter, $sel.data('image-id'));
      }

      $(setClickAttr)
        .on('click', function () {
        	var parent = $(this).closest('.movie-card').addClass('active');
        	parent.siblings().removeClass('active');
        	var old_class = $(this).parents('.old').attr('class')
        	var new_class = $(this).parents('.new').attr('class')
        	// console.log(current_class);
        	if(typeof old_class === 'undefined'){

        		$('.old').removeClass('active');
        	}
        	if(typeof new_class === 'undefined'){

        		$('.new').removeClass('active');
        	}

	      	if (setIDs == true) {
		        $('.movie-card.active .caption .thumb[data-image-id]')
		          .each(function () {
		          	if(counter < 5){
		            	counter++;
		          	}
		          	else{
		          		counter = 1;
		          	}
		            console.log(counter);
		            console.log($(this));
		            $(this)
		              .attr('data-image-id', counter);
		            // $(this).siblings().attr('data-image-id', '');
		          });
	      	}
          	updateGallery($(this));
        });
    }
  });

// build key actions
$(document)
  .keydown(function (e) {
    switch (e.which) {
      case 37: // left
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
          $('#show-previous-image')
            .click();
        }
        break;

      case 39: // right
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
          $('#show-next-image')
            .click();
        }
        break;

      default:
        return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
  });

jQuery(document).ready(function($) {
  var alterClass = function() {
    var ww = document.body.clientWidth;
    if (ww < 400) {
      $('.head').remove('h3');
    } 
  };
  $(window).resize(function(){
    alterClass();
  });
  //Fire it when the page first loads:
  alterClass();
});

</script>
@endpush