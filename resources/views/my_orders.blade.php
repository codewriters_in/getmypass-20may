@extends('frontend.layouts.master')
@section('content')
<section id="subheader" style="background-image: url({{asset('assets/images/static-slider/slider10/img1.jpg')}});">
	<div class="container">
	  <div class="row">
	    <div class="col-md-12">
	      <h1>
	        Orders
	      </h1>
	    </div>
	    
	    <!-- devider -->
	    <div class="col-md-12">
	      <div class="devider-page">
	        <div class="devider-img-right">
	        </div>
	      </div>
	    </div>

	    <div class="col-md-12">
	      <ul class="subdetail">
	        <li>
	          <a href="{{route('home')}}">Home</a>
	        </li>

	        <li class="sep">/
	        </li>

	        <li>Orders
	        </li>
	      </ul>
	    </div>
	  </div>
	</div>
</section>

<div class="contact1">
	<div class="row">
		<div class="container">
			<div class="spacer">
				<div class="row m-0">
					<div class="col-lg-12">
						<div class="contact-box p-r-40">
							<div class="card b-all">
								
								<div class="card-body p-15">
									<div class="row">
										<div class="col-md-2 m-t-40 text-center">
											<h6 class="small-head"><strong>Event</strong></h6>
										</div>
										<div class="text-center col-md-2 m-t-40">
											<h6 class="small-head"><strong>Start Date</strong></h6>
										</div>
										<div class="text-center col-md-2 m-t-40">
											<h6 class="small-head"><strong>Ticket</strong></h6>
										</div>
										<div class="text-center col-md-2 m-t-40">
											<h6 class="small-head"><strong>Price</strong></h6>
										</div>
										<div class="text-center col-md-2 m-t-40">
											<h6 class="small-head"><strong>Quantity</strong></h6>
										</div>
										<div class="col-md-2 m-t-40 text-center">
											<h6 class="small-head"><strong>Total Price</strong></h6>
										</div>
									</div>
								</div>
								@foreach($data as $key => $value)
									<div class="card-body p-15">
										<div class="row">
											<div class="col-md-2 m-t-40 text-center">
												<h6 class="small-head"><strong>{{$value->event->title}}</strong></h6>
											</div>
											<div class="text-center col-md-2 m-t-40">
												{{date('d-m-y', strtotime($value->event->start_date))}}
											</div>
											<div class="text-center col-md-2 m-t-40">
												{{ucfirst($value->ticket->ticket_type)}}
											</div>
											<div class="col-md-2 m-t-40 text-center">
												₹ <span class="text-dark ">	{{$value->price}}</span>
											</div>
											<div class="col-md-2 m-t-40 text-center">
												
												{{$value->qty}}
											</div>
											<div class="col-md-2 m-t-40 text-center">
												₹ <span class="text-dark ">	{{$value->user_payment->amount}}</span>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4 m-t-40 text-center">
												<strong>Purchase date</strong>: {{date('d-m-y', strtotime($value->user_payment->created_at))}}
											</div>
											<div class="col-md-2 m-t-40 m-b-15 text-center">
												<a href="{{route('download.get_ticket', [$value->user_payment->payment_order_id, 'download' => 'ticket'])}}" class="btn btn-primary">Preview Ticket</a>
											</div>
										</div>
									</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
jQuery(document).ready(function($) {
  var alterClass = function() {
    var ww = document.body.clientWidth;
    if (ww < 400) {
      $('#subheader').remove();
    } 
  };
  $(window).resize(function(){
    alterClass();
  });
  //Fire it when the page first loads:
  alterClass();
});

</script>
@endpush