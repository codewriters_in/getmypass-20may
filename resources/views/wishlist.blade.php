@extends('f_layout')

@section('content')
<div class="static-slider10" style="background-image:url(../assets/images/static-slider/slider10/img1.jpg)">
	<div class="container">
		<!-- Row  -->
		<div class="row justify-content-center ">
			<!-- Column -->
			<div class="col-md-6 align-self-center text-center" data-aos="fade-down" data-aos-duration="1200">
				<span class="label label-rounded label-inverse">getmypass</span>
				<h1 class="title">WishList</h1>
			</div>
			<!-- Column -->

		</div>
	</div>
</div>
<br>
<div class="container">
    @if (session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @endif

    @if (session()->has('error_message'))
        <div class="alert alert-danger">
            {{ session()->get('error_message') }}
        </div>
    @endif

    <h3>Items in your Wishlist</h3>
    <br>
    @if (sizeof(Cart::instance('wishlist')->content()) > 0)

        <table class="table">
            <thead>
                <tr>
                    <th style="width: 40%">Ticket</th>

                    <th style="width: 20%">Price</th>
                    <th style="width: 20%">Event Status</th>
                    <th style="width: 20%"></th>
                </tr>
            </thead>

            <tbody>
                @foreach (Cart::instance('wishlist')->content() as $item)
                <tr>
                    <td><a href="{{ url('events', [$item->model->slug]) }}">{{ $item->name }}</a></td>

                    <td>₹{{ $item->subtotal }}</td>
                    <td></td>
                    <td>
                    <div class="row">
                        <form action="{{ url('wishlist', [$item->rowId]) }}" method="POST" class="side-by-side">
                            {!! csrf_field() !!}
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="submit" class="btn btn-danger btn-sm" value="Remove">
                        </form>
                        &nbsp;
                        <form action="{{ url('switchToCart', [$item->rowId]) }}" method="POST" class="side-by-side">
                            {!! csrf_field() !!}
                            <input type="submit" class="btn btn-success btn-sm" value="Add To Cart">
                        </form>
                        </div>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>

        <div class="spacer"></div>

        <a href="/events" class="btn btn-primary">Continue Shopping</a> &nbsp;

        <div style="float:right">
            <form action="{{ url('/emptyWishlist') }}" method="POST">
                {!! csrf_field() !!}
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" class="btn btn-danger" value="Empty Wishlist">
            </form>
        </div>

    @else

        <h5>You have no items in your Wishlist</h5>
        <br>
        <a href="/events" class="btn btn-success">Continue Shopping</a>

    @endif

    <div class="spacer"></div>

</div> <!-- end container -->

@endsection