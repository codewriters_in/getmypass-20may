@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <h1 class="m-0 text-dark">Dashboard</h1>
        </div>
        <div class="row mb-2">
          <div class="col-md-8 col-md-offset-2">
            @if (session()->has('success'))
            <div class="alert alert-success">
              {{ session()->get('success') }}
            </div>
            @endif
            @if (session()->has('error'))
            <div class="alert alert-danger">
              {{ session()->get('error') }}
            </div>
            @endif
          </div>
        </div>
        <div class="row col-md-12 mt-2">
          <div class="col-md-12">
            <div class="card col-md-12">
              <div class="card-header">
                <h3 class="card-title">Payment Requests</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                  <tbody>
                    <tr>
                      <th>#</th>
                      <th>Organizer</th>
                      <th>Amount</th>
                      <th>Requested Date</th>
                      <th>Action</th>
                    </tr>
                    @foreach ($data as $key => $value)
                    <tr>
                      <td>{!!++ $key!!}</td>
                      <td>{!!ucfirst($value->organizer->name)!!}</td>
                      <td>{!!$value->beneficiary_amount!!}</td>
                      <td>{!!$value->created_at!!}</td>
                      <td>
                        @if($value->status == "pending")
                        <a href="{{route('payment.request_accept', $value->id)}}" class="btn btn-success">Accept</a>
                        @else
                        {{$value->status}}
                        @endif
                      </td>
                      <td>
                        
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection