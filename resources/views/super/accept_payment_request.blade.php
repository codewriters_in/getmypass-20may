@extends('layouts.master') 
@section('content')
<div class="content-wrapper">
  	<div class="content-header">
	    <div class="container-fluid">
	    	<div class="row">
		        <div class="col-sm-12">
		          <h1 class="m-0 text-dark">Accept Payment</h1>
		        </div>
	        </div>
		  	<div class="row mb-2">
				<div class="col-md-8 col-md-offset-2">
		        	@if (session()->has('success'))
						<div class="alert alert-success">
							{{ session()->get('success') }}
						</div>
					@endif

					@if (session()->has('error'))
						<div class="alert alert-danger">
							{{ session()->get('error') }}
						</div>
					@endif
		    	</div>

				<div class="card col-md-8 offset-2">
					<div class="box">
				    	<div class="box-body">
				    		{{Form::open(['route' => 'super.payment.create', 'method' => 'POST'])}}
				    				{{Form::hidden('id', $data->id)}}
							    <div class="form-group m-t-15">
							      <label for="beneficiary_amount">Amount</label>
							      <input class="form-control" type="text" name="beneficiary_amount"  value="{{$data->beneficiary_amount}}" readonly>
							    </div>
							    <div class="form-group m-t-15">
							      <label for="beneficiary_account_number">Account Number</label>
							      <input class="form-control" type="text" name="beneficiary_account_number"  value="{{$data->bank_detail->beneficiary_account_number}}" readonly>
							    </div>
							    <div class="form-group m-t-15">
							      <label for="beneficiary_name">Account Name</label>
							      <input class="form-control" type="text" name="beneficiary_name"  value="{{$data->bank_detail->beneficiary_name}}" readonly>
							    </div>
							    <div class="form-group m-t-15">
							      <label for="beneficiary_ifsc_code">IFSC Code</label>
							      <input class="form-control" type="text" name="beneficiary_ifsc_code"  value="{{$data->bank_detail->beneficiary_ifsc_code}}" readonly>
							    </div>
							    <div class="form-group m-t-15">
							    	{{Form::submit('Pay', ['class' => 'btn btn-primary'])}}
							    </div>
							{{Form::close()}}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection