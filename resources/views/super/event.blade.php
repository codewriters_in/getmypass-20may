@extends('layouts.master')
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          @if (session()->has('success_message'))
          <div class="alert alert-success">
            {{ session()->get('success_message') }}
          </div>
          @endif
          @if (session()->has('error_message'))
          <div class="alert alert-danger">
            {{ session()->get('error_message') }}
          </div>
          @endif
          <h1 class="m-0 text-dark">Dashboard</h1>
        </div>
        <div class="row col-md-12 mt-2">
          <div class="col-md-12">
            <div class="card col-md-12">
              <div class="card-header">
                
                <h3 class="card-title">Event Information</h3>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group m-t-15">
                    <label for="organisation">Event Title: </label>
                    {!! $event->title!!}
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="form-group m-t-15">
                    <label for="name">Location: </label>
                    {!! $event->location!!}
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="form-group m-t-15">
                    <label for="email">Description: </label>
                    {!! $event->description!!}
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="form-group m-t-15">
                    <label for="phone">start date: </label>
                    {!! $event->start_date!!}
                  </div>
                </div>
                <div class="col-lg-12">
                  <label for="facebook">End Date: <small> </small></label>
                  {!! $event->end_date!!}
                </div>
                
                
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endsection