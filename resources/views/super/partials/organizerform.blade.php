
<div class="row">
  <div class="col-lg-12">
    <div class="form-group m-t-15">
      <label for="org">Entity/Organisation</label>
      <input class="form-control org1" type="text" name="org" placeholder="Enter Organisation or entity name" required>
    </div>
  </div>
  <div class="col-lg-12">
    <div class="form-group m-t-15">
      <label for="name">Contact Name</label>
      <input class="form-control name1" type="text" name="name" placeholder="Enter Name" required>
    </div>
  </div>
  <div class="col-lg-12">
    <div class="form-group m-t-15">
      <label for="email">Email ID</label>
      <input class="form-control email1" type="text" name="email" placeholder="Enter Email Address" required>
    </div>
  </div>
  <div class="col-lg-12">
    <div class="form-group m-t-15">
      <label for="phone">Phone Number</label>
      <input class="form-control phone1" type="text" name="phone" placeholder="Enter Phone Number" required>
    </div>
  </div>
  <div class="col-lg-12">
    <div class="form-group m-t-15">
      <label for="phone">City</label>
      <select name="cityid" class="form-control"> 
        @foreach($cities as $p)
          <option value="{{$p->id}}">{{$p->cname}}</option>
        @endforeach
      </select>
    
    </div>
  </div>
  <div class="col-lg-12">
    <div class="form-group m-t-15">
      <label for="commission">Basic Commission %</label>
      <input class="form-control basic_commission1" type="number" name="basic_commission" placeholder="Enter Commission %" required step="0.01">
    </div>
  </div>
  <div class="col-lg-12">
    <div class="form-group m-t-15">
      <label for="commission">Standard Commission %</label>
      <input class="form-control standard_commission1" type="number" name="standard_commission" placeholder="Enter Commission %" required step="0.01">
    </div>
  </div>
  <div class="col-lg-12">
    <label for="facebook">Facebook Account<small> (Optional)</small></label>
    <div class="input-group">
      <span style="background-color: #eee;" class="input-group-addon">facebook.com/</span>
      <input class="form-control fb1" placeholder="Username" name="facebook" type="text"  >
    </div>
  </div>
  <div class="col-lg-12">
    <label for="facebook">Twitter Account<small> (Optional)</small></label>
    <div class="input-group">
      <span style="background-color: #eee;" class="input-group-addon">twitter.com/</span>
      <input class="form-control tw1" placeholder="Username" name="tw_username" type="text" >
    </div>
  </div>
  
  <div class="col-lg-12">
    <div class="form-group m-t-15">
      <label for="password">Password</label>
      <input class="form-control" type="password" name="password" placeholder="Enter Password" required>
    </div>
  </div>

</div>
                  