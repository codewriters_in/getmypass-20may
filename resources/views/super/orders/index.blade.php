@extends('layouts.master') 
@section('content')
<!-- Content Wrapper. Contains page content -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <h1 class="m-0 text-dark">Dashboard</h1>
        </div>

        <div class="row col-md-12 mt-2">
          <div class="col-md-12">
            <div class="card col-md-12">
              <div class="card-header">
                <h3 class="card-title">Orders</h3>

               </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                  <tbody><tr>
                    <th>#</th>
                    <th>Payment Odrder#</th>
                    <th>Event</th>
                    <th>User</th>
                    <th>Ticket Price</th>
                    <th>Ticket Quantity</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th>Order Date</th>
                    {{-- <th>Action</th> --}}
                  </tr>
                  @if($data->count())
                    @foreach ($data as $key =>  $order)
                      <tr>
                        <td>{{++ $key}}</td>
                        <td>{{$order->user_payment->payment_order_id}}</td>
                        <td>{{ucfirst($order->event->title)}}</td>
                        <td>{{ucfirst($order->user->name)}}</td>
                        <td>₹ {{ucfirst($order->price)}}</td>
                        <td>{{ucfirst($order->qty)}}</td>
                        <td>₹ {{ucfirst($order->total_price)}}</td>
                        <td>{{ucfirst($order->status)}}</td>
                        <td>{{date('D-M-Y', strtotime($order->created_at))}}</td>
                        {{-- <td><a href=""><i class="fa fa-eye" alt="Preview" data-toggle="tooltip" title="Preview"></i></a></td> --}}
                      </tr>
                    @endforeach
                  @else

                    No records found
                  @endif
                </tbody>
              </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Edit Modal -->
@endsection
 