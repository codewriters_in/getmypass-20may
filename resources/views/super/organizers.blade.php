@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <h1 class="m-0 text-dark">Dashboard</h1>
        </div>
        <div class="row col-md-12 mt-2">
          <div class="col-md-12">
            <div class="card col-md-12">
              <div class="card-header">
                <h3 class="card-title">Organizers</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addorganizer">Add Organizer <i class="fa fa-organizer-plus fa-fw"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                  <tbody><tr>
                    <th>ID</th>
                    <th>Organizer</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  @foreach ($organizers as $organizer)
                  <tr>
                    <td>{{$organizer->id}}</td>
                    <td>{{$organizer->name}}</td>
                    <td>{{$organizer->email}}</td>
                    <td>
                      @if($organizer->email_verified_at)
                      <span class="badge bg-success">Verfied</span>
                      @else
                      <span class="badge bg-danger">Not Verfied</span>
                      @endif
                    </td>
                    <td>
                      <a class="btn btn-success" href="{{route('admin.organizer.show',$organizer->id)}}"><i class="fa fa-eye"></i></a>
                      <button class="btn btn-warning" data-uid="{{$organizer->id}}" data-name="{{$organizer->name}}"
                      data-email="{{$organizer->email}}" data-toggle="modal" data-target="#editorganizer"><i class="fa fa-pencil "></i></button>
                      <button class="btn btn-danger" data-uid="{{$organizer->id}}"
                      data-email="{{$organizer->email}}" data-toggle="modal" data-target="#delorganizer"><i class="fa fa-trash "></i></button>
                      
                      
                    </td>
                  </tr>
                  @endforeach
                </tbody></table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Add Modal -->
<div class="modal fade" id="addorganizer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add Organizer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('admin.organizer.add')}}" method="post">
        {{csrf_field()}}
        <div class="modal-body">
          @include('admin.partials.organizerform')
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Add</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Edit Modal -->
<div class="modal fade" id="editorganizer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Edit Organizer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('admin.organizer.edit')}}" method="post">
        {{csrf_field()}}
        <input type="hidden"  name="id" value="9" class="uid">
        <div class="modal-body">
          @include('admin.partials.organizerform')
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save Changes</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Delete Modal -->
<div class="modal fade" id="delorganizer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Delete Organizer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('admin.organizer.delete')}}" method="post">
        {{csrf_field()}}
        <input type="hidden"  name="id" value="9" class="uid">
        <div class="modal-body">
          Are You Sure ?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger">Delete</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
$('#editorganizer').on('show.bs.modal', function (event) {
var button = $(event.relatedTarget);
var name = button.data('name');
var email = button.data('email');
var uid = button.data('uid');
var modal = $(this);
modal.find('.modal-body .name1').val(name);
modal.find('.modal-body .email1').val(email);
$('.uid').val(uid);

})
$('#delorganizer').on('show.bs.modal', function (event) {
var button = $(event.relatedTarget);
var uid = button.data('uid');
var modal = $(this);
$('.uid').val(uid);

})
</script>
@stop