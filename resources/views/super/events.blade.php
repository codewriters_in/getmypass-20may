@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          @if (session()->has('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}
          </div>
          @endif
          @if (session()->has('error'))
          <div class="alert alert-danger">
            {{ session()->get('error') }}
          </div>
          @endif
          <h1 class="m-0 text-dark">Dashboard</h1>
        </div>
        <div class="row col-md-12 mt-2">
          <div class="col-md-12">
            <div class="card col-md-12">
              <div class="card-header">
                <h3 class="card-title">Upcoming Events</h3>
                
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                  <tbody><tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Organizer</th>
                    <th>Venue</th>
                    <th>Start Date</th>
                    <th>Action</th>
                  </tr>
                  @foreach ($events as $event)
                  <tr class="{{($event->start_date < Carbon\Carbon::now()) ? 'bg-dim' : 'bg-highlight'}}">
                    <td>{!!$event->id!!}</td>
                    <td>{!!$event->title!!}</td>
                    <td>{!!$event->user->name!!}</td>
                    <td>{!!$event->venue_name!!}</td>
                    <td>{!!$event->start_date!!}</td>
                    <td>
                      <a class="btn btn-primary" href="{!!route('super.event.show',$event->id)!!}"><i class="fa fa-eye"></i></a>
                      
                      @if($event->status=='0')
                      <a href="<?php echo url('/').'/super/eventstatus/' ?>{{$event->id}}/1">
                        <button class="btn btn-warning" data-toggle="modal" data-status="1" data-target="#activeuser"><i class="fa fa-thumbs-down "></i></button>
                      </a>
                      @else
                      <a href="<?php echo url('/').'/super/eventstatus/' ?>{{$event->id}}/0">
                        <button class="btn btn-success" data-toggle="modal" data-status="0" data-target="#activeuser"><i class="fa fa-thumbs-up "></i></button>
                      </a>
                      @endif
                      @if($event->is_approved == "no")
                      <a href="{{route('super.event.approve', $event->id)}}" class="btn btn-success" onclick="return confirm('Are you sure you want to approve this event?')"  title="Approve" alt="Approve"><i class="fa fa-check"></i></a>
                      @else
                      Approved
                      @endif
                      
                    </td>
                  </tr>
                  @endforeach
                </tbody></table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Add Modal -->
<div class="modal fade" id="adduser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{!!route('super.org.add')!!}" method="post">
        {!!csrf_field()!!}
        <div class="modal-body">
          @include('super.partials.userform')
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Add</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Edit Modal -->
<div class="modal fade" id="edituser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Edit User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{!!route('super.org.edit')!!}" method="post">
        {!!csrf_field()!!}
        <input type="hidden"  name="id" value="9" class="uid">
        <div class="modal-body">
          @include('super.partials.userform')
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save Changes</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Delete Modal -->
<div class="modal fade" id="deluser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Delete Organizer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{!!route('super.org.delete')!!}" method="post">
        {!!csrf_field()!!}
        <input type="hidden"  name="id" value="9" class="uid">
        <div class="modal-body">
          Are You Sure ?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger">Delete</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
$('#edituser').on('show.bs.modal', function (event) {
var button = $(event.relatedTarget);
var name = button.data('name');
var email = button.data('email');
var uid = button.data('uid');
var modal = $(this);
modal.find('.modal-body .name1').val(name);
modal.find('.modal-body .email1').val(email);
$('.uid').val(uid);

})
$('#deluser').on('show.bs.modal', function (event) {
var button = $(event.relatedTarget);
var uid = button.data('uid');
var modal = $(this);
$('.uid').html(uid);

})
</script>
@stop