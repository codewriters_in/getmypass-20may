@extends('layouts.master')
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          @if (session()->has('success_message'))
          <div class="alert alert-success">
            {{ session()->get('success_message') }}
          </div>
          @endif
          @if (session()->has('error_message'))
          <div class="alert alert-danger">
            {{ session()->get('error_message') }}
          </div>
          @endif
          <h1 class="m-0 text-dark">Dashboard</h1>
        </div>
        <div class="row col-md-12 mt-2">
          <div class="col-md-12">
            <div class="card col-md-12">
              <div class="card-header">
                <h3 class="card-title">User Basic Information</h3>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group m-t-15">
                    <label for="organisation">Entity: </label>
                    <label class="form-group org1">{!! $user->org!!}</label>
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="form-group m-t-15">
                    <label for="name">Contact Name: </label>
                    <label class="form-group name1">{!! $user->name!!}</label>
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="form-group m-t-15">
                    <label for="email">Email ID: </label>
                    <label class="form-group email1">{!! $user->email!!}</label>
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="form-group m-t-15">
                    <label for="phone">Phone Number: </label>
                    <label class="form-group phone1">{!! $user->phone!!}</label>
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="form-group m-t-15">
                    <label for="commission">Commission: </label>
                    <label class="form-group commission1">{!! $user->commission!!} %</label>
                  </div>
                </div>
                <div class="col-lg-12">
                  <label for="facebook">Facebook Account: <small> </small></label>
                  <div class="label-group">
                    <span style="background-color: #eee;" class="label-group-addon">facebook.com/</span>
                    <label class="form-group fb1">{!! $user->fb_username!!}</label>
                  </div>
                </div>
                <div class="col-lg-12">
                  <label for="facebook">Twitter Account: <small> </small></label>
                  <div class="label-group">
                    <span style="background-color: #eee;" class="label-group-addon">twitter.com/</span>
                    <label class="form-group tw1">{!! $user->tw_username!!}</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection