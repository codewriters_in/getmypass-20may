@extends('f_layout')

@section('content')
<div class="static-slider10" style="background-image:url({!! asset('assets/images/static-slider/slider10/img1.jpg') !!})">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center ">
            <!-- Column -->
            <div class="col-md-6 align-self-center text-center" data-aos="fade-down" data-aos-duration="1200">
                <span class="label label-rounded label-inverse">Home</span>
                <h1 class="title">Events & Tickets</h1>
                <h6 class="subtitle op-8">Book tickets for events around you in few clicks. Pay easily and receive tickets directly into your inbox through SMS & Email.<br/> Happy Booking :)</h6>
                <a class="btn btn-outline-light btn-rounded btn-md btn-arrow m-t-20" data-toggle="collapse" href=""><span>Browse Events<i class="ti-arrow-right"></i></span></a>
            </div>
            <!-- Column -->

        </div>
    </div>
</div>

<div class="blog-home2 spacer">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center">
            <!-- Column -->
            <div class="col-md-8 text-center">
                <h3 class="title">Upcoming Events</h3>
                <h6 class="subtitle">Choose from amazing events around you posted directly by organizers and be a part of fun  with friends & family.</h6>
            </div>
            <!-- Column -->
        </div>
        <div class="row m-t-40">
            <!-- Column -->
            @foreach($eventData as $eventData)
                <div class="col-md-4">
                    <div class="card" data-aos="flip-left" data-aos-duration="1200">
                        <a href="#"><img class="card-img-top" src="{!! asset('assets/images/blog/blog-home/img3.jpg') !!}" alt="wrappixel kit"></a>
                        <div class="date-pos bg-info-gradiant">{{ $eventData->start_date->format('M') }}<span>{{ $eventData->start_date->format('d') }}</span></div>
                        <h5 class="font-medium m-t-30"><a href="#" class="link">{{ $eventData->title }}</a></h5>
                        {{ $eventData->venue_name}}<br><b>Starting from Rs. {{ $eventData->getTicket->min('price') }}</b>
                        <p class="m-t-20">{!! $eventData->description !!}</p>
                        <a href="{{ route('page.event.show', $eventData->id) }}"><button class="btn btn-sm btn-secondary">View Details <i class="fa fa-chevron-right"></i></button></a>
                    </div>
                </div>
            @endforeach
            
        </div>
    </div>
</div>

<div class="spacer">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center">
            <div class="col-md-7 text-center">
                <h2 class="title">Our Recent work with three column</h2>
                <h6 class="subtitle">You can relay on our amazing features list and also our customer services will be great experience for you without doubt and in no-time</h6>
            </div>
        </div>
        <!-- Row  -->
        <div class="row m-t-40">
            <!-- Column -->
            <div class="col-md-4">
                <div class="card card-shadow" data-aos="flip-left" data-aos-duration="1200">
                    <a href="#" class="img-ho"><img class="card-img-top" src="{!! asset('assets/images/portfolio/img1.jpg') !!}" alt="wrappixel kit" /></a>
                    <div class="card-body">
                        <h5 class="font-medium m-b-0">Branding for Theme Designer</h5>
                        <p class="m-b-0 font-14">Digital Marketing</p>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-4">
                <div class="card card-shadow" data-aos="flip-up" data-aos-duration="1200">
                    <a href="#" class="img-ho"><img class="card-img-top" src="{!! asset('assets/images/portfolio/img2.jpg') !!}" alt="wrappixel kit" /></a>
                    <div class="card-body">
                        <h5 class="font-medium m-b-0">Button Designs Free</h5>
                        <p class="m-b-0 font-14">Search Engine</p>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-4">
                <div class="card card-shadow" data-aos="flip-right" data-aos-duration="1200">
                    <a href="#" class="img-ho"><img class="card-img-top" src="{!! asset('assets/images/portfolio/img3.jpg') !!}" alt="wrappixel kit" /></a>
                    <div class="card-body">
                        <h5 class="font-medium m-b-0">Branding & Co Agency</h5>
                        <p class="m-b-0 font-14">Admin templates</p>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-4">
                <div class="card card-shadow" data-aos="flip-left" data-aos-duration="1200">
                    <a href="#" class="img-ho"><img class="card-img-top" src="{!! asset('assets/images/portfolio/img4.jpg') !!}" alt="wrappixel kit" /></a>
                    <div class="card-body">
                        <h5 class="font-medium m-b-0">Zukandre Phoniex</h5>
                        <p class="m-b-0 font-14">Branding</p>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-4">
                <div class="card card-shadow" data-aos="flip-up" data-aos-duration="1200">
                    <a href="#" class="img-ho"><img class="card-img-top" src="{!! asset('assets/images/portfolio/img5.jpg') !!}" alt="wrappixel kit" /></a>
                    <div class="card-body">
                        <h5 class="font-medium m-b-0">Sionage Mokcup</h5>
                        <p class="m-b-0 font-14">Wll Mockup</p>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-4">
                <div class="card card-shadow" data-aos="flip-right" data-aos-duration="1200">
                    <a href="#" class="img-ho"><img class="card-img-top" src="{!! asset('assets/images/portfolio/img6.jpg') !!}" alt="wrappixel kit" /></a>
                    <div class="card-body">
                        <h5 class="font-medium m-b-0">Hard Cover Book Mock</h5>
                        <p class="m-b-0 font-14">Book Covers</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="static-slider3">
    <div class="container">
        <!-- Row  -->
        <div class="row justify-content-center ">
            <!-- Column -->
            <div class="col-md-8 align-self-center text-center" data-aos="fade-right" data-aos-duration="1200">
                <h1 class="title">I’m Johanthan Doe, an <b class="font-bold">Entreprenuer, Designer & Front-end Developer</b>, Making <span class="text-success-gradiant font-bold typewrite" data-period="2000" data-type='[ "Photoshop", "Web Application", "Web Designing", "Web Development" ]'></span></h1>
                <a class="btn btn-success-gradiant btn-md btn-arrow m-t-20" data-toggle="collapse" href=""><span>Checkout My Work <i class="ti-arrow-right"></i></span></a>
            </div>
            <!-- Column -->

        </div>
    </div>
</div>

@endsection