@extends('f_layout')

@section('content')
<div class="static-slider10" style="background-image:url({!! asset('assets/images/static-slider/slider10/img1.jpg') !!})">
	<div class="container">
		<!-- Row  -->
		<div class="row justify-content-center ">
			<!-- Column -->
			<div class="col-md-6 align-self-center text-center" data-aos="fade-down" data-aos-duration="1200">
				<span class="label label-rounded label-inverse">getmypass</span>
				<h1 class="title">List Your Event</h1>
				<h6 class="subtitle op-8">GetMyPass.in
					: an online free effortless and concise platform for the event organizers.
					Want to create awareness about your event, design passes and keep complete record of passes sold? 
					<br/>
					<a class="btn btn-outline-light btn-rounded btn-md btn-arrow m-t-20" data-toggle="collapse" href="">YESSS, GetMyPass is the best platform</a>
					</h6>
				</div>
				<!-- Column -->

			</div>
		</div>
	</div>

	<div class="contact1">
		<div class="row">
			<div class="container">
				<div class="spacer">
					<div class="row m-0">
						<div class="col-lg-7">
							<div class="contact-box p-r-40">
								@if (session()->has('success_message'))
								<div class="alert alert-success">
									{{ session()->get('success_message') }}
								</div>
								@endif

								@if (session()->has('error_message'))
								<div class="alert alert-danger">
									{{ session()->get('error_message') }}
								</div>
								@endif
								<br/>
								<h3 class="title">Register With Us</h3>
								<form class="form-horizontal" data-aos="fade-left" data-aos-duration="1200" method="POST" action="organiser-signup">
									{!! csrf_field() !!}
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group m-t-15">
												<label for="organisation">Entity/Organisation/Club</label>
												<input class="form-control" type="text" name="organisation" placeholder="Enter Organisation or entity name" required>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="form-group m-t-15">
												<label for="name">Select City</label>
											  	<select class="form-control" name="cityid">
											  		@foreach($cities as $city)
												    	<option value="{{$city->id}}">{{$city->cname}}</option>
												    @endforeach
											  	</select>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="form-group m-t-15">
												<label for="name">Contact Name</label>
												<input class="form-control" type="text" name="name" placeholder="Enter Name" required>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="form-group m-t-15">
												<label for="email">Email ID</label>
												<input class="form-control" type="text" name="email" placeholder="Enter Email Address" required>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="form-group m-t-15">
												<label for="phone">Phone Number</label>
												<input class="form-control" type="text" name="phone" placeholder="Enter Phone Number" required>
											</div>
										</div>
										<div class="col-lg-12">
											<label for="facebook">Facebook Account<small> (Optional)</small></label>
											<div class="input-group">
												<span style="background-color: #eee;" class="input-group-addon">facebook.com/</span>
												<input class="form-control " placeholder="Username" name="facebook" type="text" id="facebook" >
											</div>
										</div>
										<div class="col-lg-12">
											<label for="facebook">Twitter Account<small> (Optional)</small></label>
											<div class="input-group">
												<span style="background-color: #eee;" class="input-group-addon">twitter.com/</span>
												<input class="form-control " placeholder="Username" name="tw_username" type="text" id="tw_username">
											</div>
										</div>
										
										<div class="col-lg-12">
											<div class="form-group m-t-15">
												<label for="password">Password</label>
												<input class="form-control" type="password" name="password" placeholder="Enter Password" required>
											</div>
										</div>
										<div class="col-lg-12">
											<button type="submit" class="btn btn-danger-gradiant m-t-20 btn-arrow"><span>Register<i class="ti-arrow-right"></i></span></button>
										</div>
									</div>
								</form>
							</div>
						</div>
						<div class="col-lg-5">
							<div class="detail-box p-40 bg-info" data-aos="fade-right" data-aos-duration="1200">
								<h3 class="text-white">Features</h3>
								<br/>
								<p class="text-white">1) Dashboard with complete integrated records of events,revenues and passes.</p>
								<p class="text-white">2) Create and customize events and variety of passes of every event in a single click.</p>
								<p class="text-white">3) Exclusive check-in facility with ticket generation and bar code generation.</p>
								<p class="text-white">4) Receive payment instantly from the customer directly in your bank account.</p>
								<p class="text-white">5) Secure and simple!! Takes down some pain from your shoulders so host a event from getmypass - your ticket managing partner..</p>
								<a class="btn btn-outline-light btn-rounded btn-md btn-arrow m-t-20" data-toggle="collapse" href=""><span>Contact Us</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@endsection