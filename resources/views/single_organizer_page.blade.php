@extends('f_layout')

@section('content')
<div class="new-slider" style="background-image:url({!! asset('assets/images/static-slider/slider10/img1.jpg') !!})">
	<div class="container">
		<!-- Row  -->
		<div class="row justify-content-center ">
			<!-- Column -->
			<div class="col-md-6 align-self-center text-center" data-aos="fade-down" data-aos-duration="1200">
				<span class="label label-rounded label-inverse">getmypass</span>
				<h1 class="title">Events & Tickets</h1>
			</div>
			<!-- Column -->

		</div>
	</div>
</div>
<br>
<div>
	<div class="container">
		<div class="row justify-content-center">
			<!-- Column -->
			<div class="col-md-8 text-center">
				<h2 class="title">Upcoming Events</h2>
				<h6 class="subtitle">Choose from amazing events around you posted directly by organizers and be a part of fun  with friends & family.</h6>
			</div>
			<!-- Column -->
		</div>
		<br/>
		<div class="row justify-content-center">
			<button type="button" class="btn btn-sm waves-effect waves-light btn-rounded btn-secondary">All Events</button>
			&nbsp;
			<button type="button" class="btn btn-sm waves-effect waves-light btn-rounded btn-secondary">This Week</button>
			&nbsp;
			<button type="button" class="btn btn-sm waves-effect waves-light btn-rounded btn-secondary">This Month</button>
		</div>
		<div class="row m-t-40 blog-home2">
			<!-- Column -->
			@foreach($eventData as $eventData)
			<div class="col-md-4">
				<div class="card" data-aos="flip-left" data-aos-duration="1200">
					<a href="#"><img class="card-img-top" src="{!! URL::to('assets/images/blog/blog-home/img3.jpg') !!}" alt="wrappixel kit"></a>
					<div class="date-pos bg-info-gradiant">{{ $eventData->start_date->format('M') }}<span>{{ $eventData->start_date->format('d') }}</span></div>
					<h5 class="font-medium m-t-30"><a href="#" class="link">{{ $eventData->title }}</a></h5>
					{{ $eventData->venue_name}}<br><b>Starting from Rs. {{ $eventData->getTicket->min('price') }}</b>
					<p class="m-t-20">{!! $eventData->description !!}</p>
					<a href="/event/{{ $eventData->id }}"><button class="btn btn-sm btn-secondary">View Details <i class="fa fa-chevron-right"></i></button></a>
				</div>
			</div>
			@endforeach
			
		</div>
		<nav aria-label="...">
				<ul class="pagination">
					<li class="page-item disabled">
						<a class="page-link" href="#" tabindex="-1">Previous</a>
					</li>
					<li class="page-item"><a class="page-link" href="#">1</a></li>
					<li class="page-item"><a class="page-link" href="#">2</a></li>
					<li class="page-item"><a class="page-link" href="#">3</a></li>
					<li class="page-item">
						<a class="page-link" href="#">Next</a>
					</li>
				</ul>
			</nav>
	</div>

</div>

@endsection