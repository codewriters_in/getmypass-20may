<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="{{asset('css/download_ticket.css')}}">

</head>
<body id="ticket">

<div class="box">
  <div class="ticket">
    <div class="container">
        <div class="row">
            <span class="airline">{{ucfirst($data->order->event->title)}}</span>
            
            <span class="boarding">{{ucfirst($data->order->ticket->ticket_type)}} Ticket</span>
            <button class="btn" id="print" onclick="createPDF()">PRIINT TICKET</button>
            <div class="content-left">
                <div class="content">
                    <p>Name: {{$data->order->user->name}}</p>
                    <p>Price: ₹ {{$data->order->price}}</p>
                    <p>Number of tickets: {{$data->order->qty}}</p>
                    <p>Subtotal: ₹ {{$data->order->total_price}}</p>
                </div>
              <span class="watermark">Getmypass</span>
            </div>

            <div class="content-right">
                <div class="barcode">
                    {!! \QrCode::size(150)->generate($data->payment_order_id)!!}
                </div>
            </div>
            <address class="content-footer">
                {{date('D-d-M h:i A', strtotime($data->order->event->start_date))}},
                {{$data->order->event->venue_name}},
                {{$data->order->event->location_address_line_1}}
            </address>
        </div>
    </div>
    
  </div>
</div>


<script type="text/javascript">
    function createPDF() {
        document.getElementById('print').style.display = 'none';          

        var ticket = document.getElementById('ticket').innerHTML;
        var style = "{!!asset('css/download_ticket.css')!!}";
        var win = window.open('', '', 'height=700,width=700');
        win.document.write('<!doctype html><html><head><meta charset="utf-8">');
        // win.document.write('<title>Ticket</title>');   // <title> FOR PDF HEADER.
        win.document.write('<link rel="stylesheet" type="text/css" href="' + style + '">');          // ADD STYLE INSIDE THE HEAD TAG.
        win.document.write('</head>');
        win.document.write('<body>');
        win.document.write(ticket);
        win.document.write('</body></html>');

        // win.close();
        win.print();    // PRINT THE CONTENTS.
        document.getElementById('print').style.display = 'block';          
    }
</script>

</body>
</html>