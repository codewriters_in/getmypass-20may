@extends('f_layout')

@section('content')
<div class="new-slider" style="background-image:url({!! asset('assets/images/static-slider/slider10/img1.jpg') !!})">
  <div class="container">
    <!-- Row  -->
    <div class="row justify-content-center">
      <!-- Column -->
      <div class="col-md-6 align-self-center text-center" data-aos="fade-down" data-aos-duration="1200">
        <span class="label label-rounded label-inverse">getmypass</span>
        <h1 class="title">Order Failed</h1>
      </div>
      <!-- Column -->

    </div>
  </div>
</div>
<br>
<div class="container">
<div class="text-center">
  <img class="img img-responsive" style="max-width: 100px; max-height: 100px;" src="{!! URL::to('assets/images/failure-cross.png') !!}">
</div>
<br>
<h4 class="text-center">
Oops!
Error returned from payment gateway. You can give it a try again.
</h4>
<br>
 <div class="text-center">
    <a href="/events"><button class="btn btn-success">Browse Events</button></a>
  </div>
  <br>
</div>

@endsection