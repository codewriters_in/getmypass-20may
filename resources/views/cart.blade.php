@extends('f_layout')
@section('content')
<style type="text/css">
	.table>tbody>tr>td, .table>tfoot>tr>td{
		vertical-align: middle;
	}
	@media screen and (max-width: 600px) {
		table#cart tbody td .form-control{
			width:20%;
			display: inline !important;
		}
		.actions .btn{
			width:25%;
			margin:1.5em 0;
		}
		.actions .btn-info{
			float:left;
		}
		.actions .btn-danger{
			float:right;
		}
		table#cart thead { display: none; }
		table#cart tbody td { display: block; padding: .6rem; min-width:320px;}
		table#cart tbody tr td:first-child { background: #fff; color: #333; }
		table#cart tbody td:before {
			content: attr(data-th); font-weight: bold;
			display: inline-block; width: 8rem;
		}
		table#cart tfoot td{display:block; }
		table#cart tfoot td .btn{display:block;}
	}
</style>
<div class="new-slider" style="background-image:url({!! asset('assets/images/static-slider/slider10/img1.jpg') !!})">
	<div class="container">
		<!-- Row  -->
		<div class="row justify-content-center ">
			<!-- Column -->
			<div class="col-md-6 align-self-center text-center" data-aos="fade-down" data-aos-duration="1200">
				<span class="label label-rounded label-inverse">getmypass</span>
				<h1 class="title">Ticket Bag</h1>
			</div>
			<!-- Column -->
		</div>
	</div>
</div>
<br>
<div class="container">
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6 text-center">
			@if (session()->has('success_message'))
			<div class="alert alert-success">
				{{ session()->get('success_message') }}
			</div>
			@endif
			@if (session()->has('error_message'))
			<div class="alert alert-danger">
				{{ session()->get('error_message') }}
			</div>
			@endif
			<br/>
		</div>
		<div class="col-md-3"></div>
	</div>
	
	@if (sizeof(Cart::content()) > 0)
		<h4 class="text-center">Your Ticket Details<br><small>All tickets in your bag can be found here</small></h4>
		<br/>
		<div class="row m-t-40">
		<!-- column  -->
			<div class="col-md-2"></div>
			<div class="col-md-8 pricing-box align-self-center">
				@foreach (Cart::content() as $item)
					<div class="card b-all">
						<div class="card-body p-15">
							<div class="row">
								<div class="col-md-4 m-t-40 text-center">
									<h5><strong>{{ $item->name }}</strong></h5>
									{{-- <h6>Entry for 1</h6> --}}
								</div>
								<div class="text-center col-md-1 m-t-40">
									<select class="quantity" data-id="{{ $item->rowId }}">
										<option {{ $item->qty == 1 ? 'selected' : '' }}>1</option>
										<option {{ $item->qty == 2 ? 'selected' : '' }}>2</option>
										<option {{ $item->qty == 3 ? 'selected' : '' }}>3</option>
										<option {{ $item->qty == 4 ? 'selected' : '' }}>4</option>
										<option {{ $item->qty == 5 ? 'selected' : '' }}>5</option>
									</select>
								</div>
								<div class="text-center col-md-2 m-t-40">
									₹ <span class="text-dark ">{{ $item->subtotal }}</span>
								</div>
								<div class="col-md-2 m-t-40 text-center">
									<form action="{{ url('cart', [$item->rowId]) }}" method="POST" class="side-by-side">
										{!! csrf_field() !!}
										<input type="hidden" name="_method" value="DELETE">
										<input type="submit" class="btn btn-danger btn-sm" value="Remove">
									</form>
								</div>
								<div class="col-md-2 m-t-40 text-center">
									<form action="{{ url('switchToWishlist', [$item->rowId]) }}" method="POST" class="side-by-side">
										{!! csrf_field() !!}
										<input type="submit" class="btn btn-success btn-sm" value="To Wishlist">
									</form>
								</div>
							</div>
						</div>
					</div>
				@endforeach
				<div class="card b-all">
					<div class="card-body p-15">
						<div class="row">
							<div class="col-md-12">
								<b style="float: right;">Subtotal:   ₹ {{ Cart::instance('default')->subtotal() }}</b>
								<br><br>
								{{-- <b style="float: right">GST:   ₹ {{ Cart::instance('default')->tax() }}</b> --}}
								<br><br>
								<b style="float: right">
								<h6>Your Total:   ₹ {{ Cart::total() }}</h6>
								</b>
								<form action="{{ url('/emptyCart') }}" method="POST">
									{!! csrf_field() !!}
									<input type="hidden" name="_method" value="DELETE">
									<input type="submit" class="btn btn-danger btn-sm" value="Empty Cart">
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 text-center">
						<a href="{{ url('/events') }}"><button style="margin: 5px;" class="btn btn-secondary">Browse Events</button></a>&nbsp;
						<a href="{{route('order-details')}}"><button style="margin: 5px;" class="btn btn-success-gradiant">Proceed to Checkout</button></a>
					</div>
				</div>
			</div>
		</div>
	@else
		<h4 class="text-center">Your ticket bag is empty.<br><small>Find your event & book tickets clicking below</small></h4>
		<br>
		<div class="text-center">
			<a href="{{ url('/events') }}" class="btn btn-success">Browse Events</a>
		</div>
	@endif
	<div class="spacer"></div>
</div>
@section('extra-js')
<script>
(function(){
$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
$('.quantity').on('change', function() {
var id = $(this).attr('data-id')
$.ajax({
type: "PATCH",
url: '{{ url("/cart") }}' + '/' + id,
data: {
'quantity': this.value,
},
success: function(data) {
window.location.href = '{{ url('/cart') }}';
}
});
});
})();
</script>
@endsection
@endsection