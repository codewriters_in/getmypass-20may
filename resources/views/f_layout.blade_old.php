<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Favicon icon -->
  <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
  <title>GetMyPass - Book & Buy Tickets</title>
  <!-- Bootstrap Core CSS -->
  <link href="../assets/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- This page CSS -->
  <!-- Custom CSS -->
  <link href="../assets/css/style.css" rel="stylesheet">
  <link href="../assets/css/index-landingpage/landing-page.css" rel="stylesheet">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>

    <body class="">

    	<!-- ============================================================== -->
    	<!-- Main wrapper - style you can find in pages.scss -->
    	<!-- ============================================================== -->
    	<div id="main-wrapper">
    		<!-- ============================================================== -->
    		<!-- Top header  -->
    		<!-- ============================================================== -->
    		<div class="header1 po-relative">
          <div class="container">
            <!-- Header 1 code -->
            <nav class="navbar navbar-expand-lg h1-nav">
              <a class="navbar-brand" href="#"><img style="max-width: 170px;" src="../assets/images/logos/black-logo.png"></a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header1" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="ti-menu"></span>
              </button>

              <div class="collapse navbar-collapse" id="header1">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                  <li class="nav-item active"><a class="nav-link" href="/">Home</a></li>
                  <li class="nav-item"><a class="nav-link" href="/events">Events</a></li>

                  @if(Auth::User())
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="h1-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{Auth::User()->name}}
                      <i class="fa fa-angle-down m-l-5"></i>
                    </a>
                    <ul class="b-none dropdown-menu animated fadeInUp">
                      <li><a class="dropdown-item" href="/my-account">Profile</a></li>
                      <li><a class="dropdown-item" href="/my-orders">Orders</a></li>

                      <li><a class="dropdown-item" href="{{ route('logout_user') }}">Logout</a></li>
                    </ul>
                  </li>
                  @else
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="h1-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">My Account
                      <i class="fa fa-angle-down m-l-5"></i>
                    </a>
                    <ul class="b-none dropdown-menu animated fadeInUp">
                      <li><a class="dropdown-item" href="/login">Login</a></li>
                      <li><a class="dropdown-item" href="/register">Register</a></li>
                    </ul>
                  </li>
                  @endif

                  <li class="nav-item"><a class="btn btn-success btn-sm" href="/cart">Cart |&nbsp;<i class="fa fa-ticket"></i>&nbsp;{{ Cart::instance('default')->count(false) }}</a>
                    &nbsp;

                    <a class="btn btn-outline-danger btn-sm" href="/wishlist">Wishlist | <i class="fa fa-heart"></i>&nbsp;{{ Cart::instance('wishlist')->count(false) }}</a>
                    &nbsp;
                    <a href="/organiser-signup" class="btn btn-secondary btn-rounded" href="sell-your-ticket">List Your Event</a></li>
                  </ul>
                </div>
              </nav>
              <!-- End Header 1 code -->
            </div>
          </div>

          <div class="page-wrapper">
           <!-- ============================================================== -->
           <!-- Container fluid  -->
           <!-- ============================================================== -->
           <div class="container-fluid">

            @yield('content')
            <!-- ============================================================== -->
          </div>
          <!-- ============================================================== -->
          <!-- End Container fluid  -->
          <!-- ============================================================== -->
          <!-- ============================================================== -->
          <!-- Back to top -->
          <!-- ============================================================== -->
          <a class="bt-top btn btn-circle btn-lg btn-info" href="#top"><i class="ti-arrow-up"></i></a>
        </div>
        <div class="footer4 b-t spacer">
         <div class="container">
          <div class="row">
           <div class="col-lg-3 col-md-6 m-b-30">
            <h5 class="m-b-20">About Us</h5>
            <p>Brief Introduction</p>
          </div>
          <div class="col-lg-3 col-md-6 m-b-30">
            <h5 class="m-b-20">Phone</h5>
            <p>Reception :  +205 123 4567 <br/>Office :  +207 235 7890</p>
          </div>
          <div class="col-lg-3 col-md-6 m-b-30">
            <h5 class="m-b-20">Email</h5>
            <p>Office :  <a href="#" class="link">info@wrappixel.com</a> <br/>Site :  <a href="#" class="link">wrapkit@wrappixel.com</a></p>
          </div>
          <div class="col-lg-3 col-md-6">
            <h5 class="m-b-20">Social</h5>
            <div class="round-social light">
             <a href="#" class="link"><i class="fa fa-facebook"></i></a>
             <a href="#" class="link"><i class="fa fa-instagram"></i></a>
           </div>
         </div>
       </div>
       <div class="f4-bottom-bar">
         <div class="row">
          <div class="col-md-12">
           <div class="d-flex font-14">
            <div class="m-t-10 m-b-10 copyright">All Rights Reserved by GetMyPass.</div>
            <div class="links ml-auto m-t-10 m-b-10">
             <a href="#" class="p-10 p-l-0">Terms of Use</a>
             <a href="#" class="p-10">Legal Disclaimer</a>
             <a href="#" class="p-10">Privacy Policy</a>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
<!-- ============================================================== -->
<!-- End footer -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="../assets/node_modules/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap popper Core JavaScript -->
<script src="../assets/node_modules/popper/dist/popper.min.js"></script>
<script src="../assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<!--Custom JavaScript -->
<script src="../assets/js/custom.min.js"></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->


<script type="text/javascript">

  $('a').on('click', function (event) {
   var $anchor = $(this);
   $('html, body').stop().animate({
    scrollTop: $($anchor.attr('href')).offset().top - 90
  }, 1000);
   event.preventDefault();
            // code 
            
          });
        </script>

        @yield('extra-js')
      </body>

      </html>