@extends('f_layout')

@section('content')
<div class="new-slider" style="background-image:url({!! asset('assets/images/static-slider/slider10/img1.jpg') !!})">
  <div class="container">
    <!-- Row  -->
    <div class="row justify-content-center">
      <!-- Column -->
      <div class="col-md-6 align-self-center text-center" data-aos="fade-down" data-aos-duration="1200">
        <span class="label label-rounded label-inverse">getmypass</span>
        <h1 class="title">Order Success</h1>
      </div>
      <!-- Column -->

    </div>
  </div>
</div>
<style type="text/css">

  .float-left {
    float:left;
    width:300px; // or 33% for equal width independent of parent width
  }

.contenido {
    margin: 30px auto;
    max-height: 430px;
    max-width: 245px;
    overflow: hidden;
    box-shadow: 0 0 10px rgb(202, 202, 204);
    background-color:;
    border-radius: 2px;
  }
  .details {
    padding: 26px;
    background:white;
    border-top: 1px dashed #c3c3c3;
  }
  .tinfo {
    font-size: 0.65em;
    font-weight: 300;
    color: #c3c3c3;
    text-transform: uppercase;
    letter-spacing: 1px;
    margin:7px 0;
  }

  .tdata {
    font-size: 0.7em;
    font-weight: 400;
    font-family: 'Roboto', sans-serif;
    letter-spacing: 0.5px;
    margin:7px 0;
  }

  .name {
    font-size: 1.3em;
    font-weight: 300;
  }

  .link {
    text-align: center;
    margin-top:10px;
  }

  .hqr{
    display: table;
    width: 100%;
    table-layout: fixed;
    margin: 0px auto;
  }
    .left-one{
    background-repeat: no-repeat;
    background-image: radial-gradient(circle at 0 96% , rgba(0,0,0,0) .2em, gray .3em, white .1em);
  }
    .right-one{
    background-repeat: no-repeat;
    background-image: radial-gradient(circle at 100% 96% , rgba(0,0,0,0) .2em, gray .3em, white .1em);
  }
  .column
  {
      display: table-cell;
      padding: 0px 0px;
  }
  .center{
    background:white;
    text-align: center;
  }

  #qrcode img{
    height:70px;
    width:70px;
    margin: 0 auto;
  }
  .masinfo{
    display:block;
  }
  .left,.right{
    width:49%;
    display:inline-table;
  }

  .nesp{
    letter-spacing: 0px;
  }
</style>

<br>
<div class="container">
<div class="text-center">
  <img class="img img-responsive" style="max-width: 100px; max-height: 100px;" src="{!!asset('assets/images/success-tick.png') !!}">
</div>
<br>
<h4 class="text-center">
Congrats!
Your payment was successful. You can preview and download your ticket below.
</h4>
<div class="contenido">
  <div class="ticket">
    <div class="hqr">
      {{-- <div class="column left-one"></div> --}}
      <div class="column center">
        <div id="qrcode">{!! \QrCode::size(150)->generate($data->user_payment->payment_order_id)!!}</div>
      </div>
      {{-- <div class="column right-one"></div> --}}
    </div>
    </div>
    <div class="details">
      <div class="tinfo">
        attendee
      </div>
      <div class="tdata name">
        {{ucfirst($data->user->name)}}
      </div>
      <div class="tinfo">
        ticket
      </div>
      <div class="tdata">
        {{ucfirst($data->ticket->ticket_type)}}
      </div>      
      <div class="tinfo">
        event
      </div>
      <div class="tdata">
        {{ucfirst($data->event->title)}}
      </div>  
      <div class="masinfo">
        <div class="left">
         <div class="tinfo">
        event date
      </div>
      <div class="tdata nesp">
        {{date('D-M-y', strtotime($data->event->start_date)) . "-" . date('D-M-y', strtotime($data->event->endt_date)) }}
      </div>  
        </div>
        <div class="right">
        <div class="tinfo">
        location
      </div>
      <div class="tdata nesp">
        {{$data->event->location_address_line_1}}, {{$data->event->location_address_line_2}}
      </div> 
        </div>
      </div>
      
      <div class="link">
        
      </div>
    </div>
  </div>
</div>

<!-- <div class='event'>
      <div class='event-side'>
          <div class='dotted-line-separator'><span class='line'></span></div>
          <div class='event-date'>Jan 17</div>
          <div class='event-time'>11 PM <span class='to'>to </span>12 PM</div>
          <div class='cut-out'></div>
      </div>
      <div class='event-body'>
        <div class='event-image'><a href='#'><span class='image'></a></div>
        <div class="event-location"><h5>Club Disco</h5></div>
        <div class="event-title"><h5>Spinning<br><small>as Resident DJ</small></h5></div>
        <div class="event-details"><h5>Entry for 2</h5></div>  
      </div>
  </div>
  <br> -->
  <div class="text-center">
    <a href="{!! route('download.get_ticket', $data->user_payment->payment_order_id) !!}"><button class="btn btn-success">Download Ticket</button></a>
  </div>
  <br>
  <p class="text-center">You can always download ticket from orders section before event gets expired.</p>
</div>
<script type="text/javascript">
var qrcode = new QRCode("qrcode");
function makeCode () {    
  var elText ="v";
  qrcode.makeCode(elText);
}
makeCode();
</script>

@endsection