@extends('backpack::eventmenulayout')
<link href="{{ asset('custom_css/ticket_widget.css') }}" rel="stylesheet">
@section('header')

<section class="content-header">
  <h1>
  <span class="text-capitalize">Tickets</span>
  <small> <span class="text-lowercase">Create a new ticket </span></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
    <li><a href="{{route('orgevent')}}">event</a></li>
    <li><a href="{{route('ticket.index', $event_id)}}" class="text-capitalize">Ticket</a></li>
    <li class="active">Create</li>
  </ol>
</section>
@endsection

@section('content')
  	<div class="row mb-2">
		<div class="col-md-8 col-md-offset-2">
			@if ($errors->any())
				<div class="alert alert-danger">
			        {{ implode('', $errors->all(':message')) }}
			    </div>
			@endif        	
			@if (session()->has('success'))
				<div class="alert alert-success">
					{{ session()->get('success') }}
				</div>
			@endif

			@if (session()->has('error'))
				<div class="alert alert-danger">
					{{ session()->get('error') }}
				</div>
			@endif
    	</div>

		<div class="col-md-8 col-md-offset-2">
			<div class="box">
		    	<div class="box-body">
		    		{{Form::open(['route' => ['ticket.store', $event_id], 'method' => 'POST'])}}

					    <div class="form-group m-t-15">
					      	<label for="ticket_type">Choose ticket type*</label>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('ticket_type', 'stag' , true) }}Stag
			                    </label>
		                  	</div>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('ticket_type', 'couple' , false) }}Couple
			                    </label>
		                  	</div>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('ticket_type', 'ladies' , false) }}Ladies
			                    </label>
		                  	</div>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('ticket_type', 'guest_ladies' , false) }}Guest Ladies
			                    </label>
		                  	</div>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('ticket_type', 'guest_couple' , false) }}Guest Couple
			                    </label>
		                  	</div>
					    </div>
					    <div class="form-group m-t-15">
					    	<label for="entry_time">Entry Time</label>
					    	{{Form::time('entry_time', '', ['class' => 'form-control'])}}
					    </div>
					    <div class="form-group m-t-15">
					    	<label for="sale_end_date_time">Sale End Time</label>
					    	<input type="datetime-local" name="sale_end_date_time" class="form-control">
					    </div>
					    <div class="form-group m-t-15">
					    	<label for="no_of_person">Number of person*</label>
					    	{{Form::number('no_of_person', '', ['min' => 1, 'class' => 'form-control'])}}
					    </div>
					    <div class="form-group m-t-15">
					    	<label for="price">Ticket Price*</label>
					    	{{Form::number('price', '', ['min' => 0, 'class' => 'form-control', 'step' => '.01', 'required'])}}
					    </div>
					    <div class="form-group m-t-15">
					    	<label for="cover_amount">Cover Amount*</label>
					    	{{Form::number('cover_amount', '', ['min' => 0, 'class' => 'form-control', 'step' => '.01', 'required'])}}
					    </div>
					    <div class="form-group m-t-15">
					    	<label for="quantity">Number of tickets*</label>
					    	{{Form::number('quantity', '', ['min' => 1, 'class' => 'form-control', 'required'])}}
					    </div>
					    {{-- <div class="form-group m-t-15">
					    	<label for="offer_discount">Offer discount</label>
					    	{{Form::number('offer_discount', '', ['min' => 0, 'class' => 'form-control'])}}
					    </div>
					    <div class="form-group m-t-15">
					    	<label for="coupon_discount">Coupon discount</label>
					    	{{Form::number('coupon_discount', '', ['min' => 0, 'class' => 'form-control'])}}
					    </div> --}}
					    <div class="form-group m-t-15">
					    	<label for="for_sale">Ticket available for sale*</label>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('for_sale', 'yes' , true) }}Yes
			                    </label>
		                  	</div>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('for_sale', 'no' , false) }}No
			                    </label>
		                  	</div>
					    </div>
					    <div class="form-group m-t-15">
					    	{{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
					    </div>
					{{Form::close()}}
				</div>
			</div>
		</div>
	</div>
@endsection