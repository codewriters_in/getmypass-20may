@extends('backpack::eventmenulayout')
<link href="{{ asset('custom_css/ticket_widget.css') }}" rel="stylesheet">
@section('header')

<section class="content-header">
  <h1>
  <span class="text-capitalize">Tickets</span>
  <small> <span class="text-lowercase">Create a new ticket </span></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
    <li><a href="{{route('orgevent')}}">event</a></li>
    <li><a href="{{route('ticket.index', $event_id)}}" class="text-capitalize">Ticket</a></li>
    <li class="active">Create</li>
  </ol>
</section>
@endsection

@section('content')
  	<div class="row mb-2">
		<div class="col-md-8 col-md-offset-2">
			@if ($errors->any())
				<div class="alert alert-danger">
			        {{ implode('', $errors->all('<div>:message</div>')) }}
			    </div>
			@endif        	
			@if (session()->has('success'))
				<div class="alert alert-success">
					{{ session()->get('success') }}
				</div>
			@endif

			@if (session()->has('error'))
				<div class="alert alert-danger">
					{{ session()->get('error') }}
				</div>
			@endif
    	</div>

		<div class="col-md-8 col-md-offset-2">
			<div class="box">
		    	<div class="box-body">
		    		{{Form::model($data, ['route' => ['ticket.update', $event_id, $data->id], 'method' => 'PUT'])}}

					    <div class="form-group m-t-15">
					      	<label for="ticket_type">Choose ticket type*</label>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('ticket_type', 'stag' , ($data->ticket_type == 'stag')) }}Stag
			                    </label>
		                  	</div>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('ticket_type', 'couple' , ($data->ticket_type == 'couple')) }}Couple
			                    </label>
		                  	</div>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('ticket_type', 'ladies' , ($data->ticket_type == 'ladies')) }}Ladies
			                    </label>
		                  	</div>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('ticket_type', 'guest_ladies' , ($data->ticket_type == 'guest_ladies')) }}Guest Ladies
			                    </label>
		                  	</div>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('ticket_type', 'guest_couple' , ($data->ticket_type == 'guest_couple')) }}Guest Couple
			                    </label>
		                  	</div>
					    </div>
					    <div class="form-group m-t-15">
					    	<label for="entry_time">Entry Time</label>
					    	{{Form::time('entry_time', $data->entry_time, ['class' => 'form-control'])}}
					    </div>
					    <div class="form-group m-t-15">
					    	<label for="sale_end_date_time">Sale End Time</label>
					    	@php
						    	$date_time = date('Y-m-d\TH:i', strtotime($data->sale_end_date_time));
					    	@endphp
					    	<input type="datetime-local" value="{{$date_time}}" name="sale_end_date_time" class="form-control">
					    </div>
					    <div class="form-group m-t-15">
					    	<label for="no_of_person">Number of person*</label>
					    	{{Form::number('no_of_person', $data->no_of_person, ['min' => 1, 'class' => 'form-control'])}}
					    </div>
					    <div class="form-group m-t-15">
					    	<label for="price">Ticket Price*</label>
					    	{{Form::number('price', $data->price, ['min' => 0, 'class' => 'form-control', 'step' => '.01', 'required'])}}
					    </div>
					    <div class="form-group m-t-15">
					    	<label for="cover_amount">Cover Amount*</label>
					    	{{Form::number('cover_amount', $data->cover_amount, ['min' => 0, 'class' => 'form-control', 'step' => '.01', 'required'])}}
					    </div>
					    <div class="form-group m-t-15">
					    	<label for="quantity">Number of tickets*</label>
					    	{{Form::number('quantity', $data->quantity, ['min' => 1, 'class' => 'form-control', 'required'])}}
					    </div>
					    {{-- <div class="form-group m-t-15">
					    	<label for="offer_discount">Offer discount</label>
					    	{{Form::number('offer_discount', $data->offer_discount, ['min' => 0, 'class' => 'form-control'])}}
					    </div>
					    <div class="form-group m-t-15">
					    	<label for="coupon_discount">Coupon discount</label>
					    	{{Form::number('coupon_discount', $data->coupon_discount, ['min' => 0, 'class' => 'form-control'])}}
					    </div> --}}
					    <div class="form-group m-t-15">
					    	<label for="for_sale">Ticket available for sale*</label>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('for_sale', 'yes' , ($data->for_sale == 'yes')) }}Yes
			                    </label>
		                  	</div>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('for_sale', 'no' , ($data->for_sale == 'no')) }}No
			                    </label>
		                  	</div>
					    </div>
					    <div class="form-group m-t-15">
					    	{{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
					    </div>
					{{Form::close()}}
				</div>
			</div>
		</div>
	</div>
@endsection