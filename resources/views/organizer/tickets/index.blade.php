@extends('backpack::eventmenulayout')
<link href="{{ asset('custom_css/ticket_widget.css') }}" rel="stylesheet">
@section('header')

<section class="content-header">
  <h1>
  <span class="text-capitalize">Tickets</span>
  <small> <span class="text-lowercase">All tickets </span></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
    <li><a href="{{route('orgevent')}}">event</a></li>
    <li class="active">Tickets</li>
  </ol>
</section>
@endsection

@section('content')
<div class="row">
  	<div class="row mb-2">
		<div class="col-md-8 col-md-offset-2">
        	@if (session()->has('success'))
				<div class="alert alert-success">
					{{ session()->get('success') }}
				</div>
			@endif

			@if (session()->has('error'))
				<div class="alert alert-danger">
					{{ session()->get('error') }}
				</div>
			@endif
    	</div>
		<!-- THE ACTUAL CONTENT -->
		<div class="col-md-10 col-md-offset-1">
			<div class="box">
				<div class="box-header">
					<a href="{{route('ticket.create', $event_id)}}" class="btn btn-primary">Add Ticket</a>
					<div id="datatable_button_stack" class="pull-right text-right"></div>
				</div>
				<div class="box-body table-responsive">
					<table id="crudTable" class="table table-striped table-hover display dataTable" role="grid" aria-describedby="crudTable_info" style="width: 854px;">
						<thead>
							<tr role="row"><th data-orderable="false" class="sorting_disabled" rowspan="1" colspan="1" style="width: 816px;" aria-label="
								List Of All Tickets
								">
								List Of All Tickets
							</th></tr>
						</thead>
						<tbody>
							@if($data->count())
								@foreach($data as $key => $value)
								<tr role="row" class="odd">
									<td>
										<div class="col-xs-12 col-sm-10">
											<ul class="event-list">
												<li>
													<div class="info col-md-4">
														<h2 class="title">
															{{$value->event->title}}
															| {{ucfirst($value->ticket_type)}} ticket
															<span class="badge">
																<i class="fa fa-dot-circle-o"></i> @if($value->is_available == "yes" && $value->remaining_quantity > 0 && strtotime($value->event->start_date) > strtotime(today()))ON SALE @else OUT OF SALE @endif
															</span>
														</h2>
														<p class="desc">
															<span><b><small>PRICE</small></b> | <span><b>₹ {{$value->price}}</b></span></span>
														</p>
													</div>
													<div class="info col-md-8 text-center">
														<br>
														<p class="desc">
															<span><b><small>Number of people</small></b> | <span class="label label-default">{{$value->no_of_person}}</span></span>
															&nbsp;
															<span><b><small>SOLD</small></b> | <span class="label label-default">{{$value->quantity - $value->remaining_quantity}}</span></span>
															&nbsp;
															<span><b><small>REMAINING</small></b> | <span class="label label-default">{{$value->remaining_quantity}}</span></span>
														</p>
													</div>
												</li>
											</ul>
										</div>
										<div class="col-xs-12 col-sm-2">
											<div class="info">
												<p class="desc">
													<a href="{{route('ticket.edit', [$event_id, $value->id])}}" class="btn btn-sm btn-flat btn-default"><i class="fa fa-edit"></i> | <b>EDIT</b></a>
												</p>
												{{-- <p class="desc">
													<a href="{{route('ticket.customize.create', $value->id)}}" class="btn btn-sm btn-flat btn-default"><i class="fa fa-code"></i> | <b>CUSTOMIZE</b></a>
												</p> --}}
												<p>
													{{  Form::open(array('route' => ['ticket.destroy', $event_id, $value['id']], 'class' => 'delete', 'method' => 'DELETE')) }}
                                                		{{ Form::button('<i class=" fa fa-trash" ></i> DELETE', array('class' => 'btn btn-sm btn-flat btn-danger', 'onClick' => " return confirm('Are you sure, you want to delete this record?')", 'title' => "Delete", 'type' => 'submit')) }}
                                            		{{Form::close()}}
												</p>
											</div>
										</div>
									</td>
								</tr>
								@endforeach
							@else
								No record found
							@endif
						</tbody>
						<tfoot>
							<tr><th rowspan="1" colspan="1">List Of All Tickets</th></tr>
						</tfoot>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>
</div>
@endsection