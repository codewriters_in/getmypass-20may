@extends('backpack::eventmenulayout')
<link href="{{ asset('custom_css/ticket_widget.css') }}" rel="stylesheet">
@section('header')
<section class="content-header">
	<h1>
	<span class="text-capitalize">Check in</span>
	<small> <span class="text-lowercase">Check in details </span></small>
	</h1>
	<ol class="breadcrumb">
    	<li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
		<li class="active">Check in</li>
	</ol>
</section>
@endsection
@section('content')
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<!-- Profile Image -->
		<div class="box box-primary">
			<div class="box-body box-profile">
				{{-- @if(!empty($data->chec_in)) --}}
				{{-- <img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture"> --}}
				{{Form::open(['route' => ['check-in.store', $event_id]])}}
				<div class="profile-user-img img-responsive ">
					{!! \QrCode::size(150)->generate($data->payment_order_id)!!}
				</div>
				<h3 class="profile-username text-center">{{ucfirst($data->user->name)}}</h3>
				{{-- <p class="text-muted text-center">Software Engineer</p> --}}
				<ul class="list-group list-group-unbordered">
					<li class="list-group-item">
						<b>Payment Order#</b> <a class="pull-right">{{($data->payment_order_id)}}</a>
					</li>
					<li class="list-group-item">
						<b>Event</b> <a class="pull-right">{{ucfirst($data->order->event->title)}}</a>
					</li>
					<li class="list-group-item">
						<b>Ticket</b> <a class="pull-right">{{ucfirst($data->order->ticket->ticket_type)}}</a>
					</li>
					<li class="list-group-item">
						<b>Price</b> <a class="pull-right">₹ {{$data->order->price}}</a>
					</li>
					<li class="list-group-item">
						<b>Number of tickets</b> <a class="pull-right">{{$data->order->qty}}</a>
					</li>
					<li class="list-group-item">
						<b>Total price</b> <a class="pull-right">₹ {{$data->order->total_price}}</a>
					</li>
					<li class="list-group-item">
						<b>Booking date</b> <a class="pull-right">{{date('D-M-y', strtotime($data->order->created_at))}}</a>
					</li>
				</ul>
				{{Form::hidden('payment_order_id', $data->payment_order_id)}}
				{{Form::hidden('status', '')}}
				<div class="row">
					<div class="col-md-6">
						{{Form::button('Accept', ['class' => 'btn btn-primary btn-block', 'id' => 'accepted', 'data-id' => 'accepted', 'type' => 'submit'])}}
					</div>
					<div class="col-md-6">
						{{Form::button('Reject', ['class' => 'btn btn-danger btn-block', 'id' => 'rejected', 'data-id' => 'rejected', 'type' => 'submit'])}}
					</div>
				</div>
				{{Form::close()}}
				{{-- @else
					<div class="callout callout-danger">
			          <h4>Warning!</h4>

			          <p>This ticket has been used!</p>
			        </div>				
				@endif --}}
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>
@endsection
@section('after_scripts')
<script type="text/javascript">
	$(function(){

		$('.btn').on('click', function(){
			// alert($(this).attr('data-id'));
			$('input[name=status]').val($(this).attr('data-id'));
		})
	});
</script>
@endsection