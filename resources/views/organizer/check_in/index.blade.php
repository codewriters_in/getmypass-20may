@extends('backpack::eventmenulayout')
<link href="{{ asset('custom_css/ticket_widget.css') }}" rel="stylesheet">
@section('header')

<section class="content-header">
  <h1>
  <span class="text-capitalize">Check in</span>
  <small> <span class="text-lowercase">Check in list </span></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
    <li class="active">Check in</li>
  </ol>
</section>
@endsection

@section('content')
<div class="row">
  	<div class="row mb-2">
		<div class="col-md-8 col-md-offset-2">
        	@if (session()->has('success'))
				<div class="alert alert-success">
					{{ session()->get('success') }}
				</div>
			@endif

			@if (session()->has('error'))
				<div class="alert alert-danger">
					{{ session()->get('error') }}
				</div>
			@endif
    	</div>
		<!-- THE ACTUAL CONTENT -->
		<div class="col-md-10 col-md-offset-1">
			<div class="box">
				<div class="box-header">
					<a href="{{route('check-in.create', $event_id)}}" class="btn btn-primary">Check in</a>
					<div id="datatable_button_stack" class="pull-right text-right"></div>
				</div>
				<div class="box-body table-responsive">
			        <table id="crudTable" class="table table-striped table-hover display">
			           <thead>
			              <tr>
			                {{-- Table columns --}}  
			                    <th data-orderable="true"># </th>
			                    <th data-orderable="true">Payment Order#</th>
			                    <th data-orderable="true">Event</th>
			                    <th data-orderable="true">Check in date</th>
			              </tr>
			            </thead>
			            <tbody>
			              	@if($data->count())
				              	@foreach($data as $key => $value)
					              	<tr>
						                <td>{{ ++ $key}}</td>
						                <td>{{$value->user_payment->payment_order_id}}</td>
						                <td>{{ucfirst($value->user_payment->order->event->title)}}</td>
						                <td>{{date('D-M-y', strtotime($value->created_at))}}</td>
					              	</tr>
				            	@endforeach
				            @else
				            	No record found
			           		@endif
			            </tbody>
			      	</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>
</div>
@endsection