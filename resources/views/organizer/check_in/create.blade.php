<!DOCTYPE html>
<html lang="en">
<html>
<head>
  <title>Check In | getmypass</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
  <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/bootstrap/css/bootstrap.min.css">
  <div class="row">
    <div class="col-md-offset-3 col-md-6 col-xs-12 text-center">
      <h3>getmypass | Check-In Tool</h3>
      <video style="width: 100%;" autoplay id="video"></video>
      <br>
      {{-- <div class="text-center">
        <div id="verification"></div>
        <button class="btn btn-success btn-lg" id="restart">Restart</button>
        <button class="btn btn-primary btn-lg" id="reset">Reset</button>
        <button class="btn btn-danger btn-lg" id="stop" >Stop</button>
      </div> --}}
    </div>
    

  </div>
  
  <!-- <script src="{{ asset('vendor/adminlte') }}/plugins/jQuery/jQuery-2.2.3.min.js"></script> -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="{{asset('js/instascan.min.js')}}"></script>
  <script type="text/javascript">

      let scanner = new Instascan.Scanner({ 
                          continuous: true,
                          video: document.getElementById('video'),
                          mirror: true,
                          captureImage: false,
                          backgroundScan: true,
                          refractoryPeriod: 5000,
                          scanPeriod: 1
                        });

      scanner.addListener('scan', function (content) {
        console.log(content);
        window.location.replace("detail" + "/" + content);
        scanner.close();
                
      });
      Instascan.Camera.getCameras().then(function (cameras) {
        if (cameras.length > 0) {
          scanner.start(cameras[0]);
          // console.log(cameras);
        } else {
          console.error('No cameras found.');
        }
      }).catch(function (e) {
        console.error(e);
      });
    </script>
</body>
</html>
