@extends('backpack::eventmenulayout')
<link href="{{ asset('custom_css/ticket_widget.css') }}" rel="stylesheet">
@section('header')

<section class="content-header">
  <h1>
  <span class="text-capitalize">Organizer</span>
  <small> <span class="text-lowercase">Event </span></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
    <li><a href="{{route('orgevent')}}">event</a></li>
    <li class="active">Highlight</li>
  </ol>
</section>
@endsection

@section('content')
<div class="row">
  	<div class="row mb-2">
		<div class="col-md-8 col-md-offset-2">
        	@if (session()->has('success'))
				<div class="alert alert-success">
					{{ session()->get('success') }}
				</div>
			@endif

			@if (session()->has('error'))
				<div class="alert alert-danger">
					{{ session()->get('error') }}
				</div>
			@endif
    	</div>
		<!-- THE ACTUAL CONTENT -->
		<div class="col-md-10 col-md-offset-1">
			<div class="box">
				<div class="box-header">
					<a href="{{route('highlight.create', $event_id)}}" class="btn btn-primary">Add highlight</a>
					<div id="datatable_button_stack" class="pull-right text-right"></div>
				</div>
				<div class="box-body table-responsive">
			        <table id="crudTable" class="table table-striped table-hover display">
			           <thead>
			              <tr>
			                {{-- Table columns --}}  
			                    <th data-orderable="true">Event Name</th>
			                    <th data-orderable="true">Event Start Day</th>
			                    <th data-orderable="true">Location</th>
			                    <th data-orderable="true">Action</th>
			              </tr>
			            </thead>
			            <tbody>
			            	@if(!empty($data))
				              	<tr>
					                <td>
				                		{{$data->event->title}}
					                </td>
					                <td>
					                	{{date('D-M-y', strtotime($data->event->start_date))}}
					                </td>
					                <td>{{$data->event->location_address_line_1}}</td>
					                <td>

					                  <a href="{{route('highlight.edit', [$event_id, $data->id])}}"><button type="button" class="btn btn-sm btn-flat btn-default" readonly><i class="fa fa-edit"></i> | <b>EDIT</b></span></button></a>

					                  {{  Form::open(array('route' => ['highlight.destroy', $event_id, $data['id']], 'class' => 'delete', 'method' => 'DELETE')) }}
	                                    {{ Form::button('<i class=" fa fa-trash" ></i> DELETE', array('class' => 'btn btn-sm btn-flat btn-danger', 'onClick' => " return confirm('Are you sure, you want to delete this record?')", 'title' => "Delete", 'type' => 'submit')) }}
	                                    {{Form::close()}}

					                </td>
				              	</tr>
				            @else
				            	No banner found
				            @endif
			            </tbody>
			      	</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>
</div>
@endsection