@extends('backpack::eventmenulayout')
@section('header')

<section class="content-header">
  <h1>
  <span class="text-capitalize">Event</span>
  <small> <span class="text-lowercase"> Highlight</span></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
    <li><a href="{{route('orgevent')}}">event</a></li>
    <li class="active">Create Highlight</li>
  </ol>
</section>
@endsection

@section('content')
<div class="row">
  	<div class="row mb-2">
		<div class="col-md-8 col-md-offset-2">
			@if ($errors->any())
				<div class="alert alert-danger">
			        {{ implode('', $errors->all(':message')) }}
			    </div>
			@endif        	
        	@if (session()->has('success'))
				<div class="alert alert-success">
					{{ session()->get('success') }}
				</div>
			@endif

			@if (session()->has('error'))
				<div class="alert alert-danger">
					{{ session()->get('error') }}
				</div>
			@endif
    	</div>
		<!-- THE ACTUAL CONTENT -->
		<div class="col-md-10 col-md-offset-1">
			<div class="box">
		    	<div class="box-body">
		    		{{Form::open(['route' => ['highlight.store', $event_id], 'method' => 'POST', 'enctype' => 'multipart/form-data'])}}

					    <div class="form-group m-t-15">
		                  	<label for="collage_1">Collage 1</label>

		                  	<div class="radio">
			                    <label>
			                      <input type="radio" name="r_collage_1" id="r_image_1" value="image" onclick="onCheckToggle('r_image_1', 'image_collage_1', 'video_collage_1')">
			                      Image (Image size should be 850 x 500 for better preview)
			                    </label>
		                  	</div>
		                  	<div class="radio">
			                    <label>
			                      <input type="radio" name="r_collage_1" id="r_video_1" value="video" onclick="onCheckToggle('r_video_1', 'video_collage_1', 'image_collage_1')">
			                      Video (Enter the video embed url)
			                    </label>
                  			</div>
		                  	<input type="file" name="collage_1" id="image_collage_1">
		                  	<input type="url" name="collage_1" placeholder="Enter the video url in embed url format" class="form-control" id="video_collage_1">
		                </div>

					    <div class="form-group m-t-15">
		                  	<label for="collage_2">Collage 2</label>

		                  	<div class="radio">
			                    <label>
			                      <input type="radio" name="r_collage_2" id="r_image_2" value="image" onclick="onCheckToggle('r_image_2', 'image_collage_2', 'video_collage_2')">
			                      Image (Image size should be 850 x 500 for better preview)
			                    </label>
		                  	</div>
		                  	<div class="radio">
			                    <label>
			                      <input type="radio" name="r_collage_2" id="r_video_2" value="video" onclick="onCheckToggle('r_video_2', 'video_collage_2', 'image_collage_2')">
			                      Video (Enter the video embed url)
			                    </label>
                  			</div>
		                  	<input type="file" name="collage_2" id="image_collage_2">
		                  	<input type="url" name="collage_2" placeholder="Enter the video url" class="form-control" id="video_collage_2">
		                </div>

					    <div class="form-group m-t-15">
		                  	<label for="collage_3">Collage 3</label>

		                  	<div class="radio">
			                    <label>
			                      <input type="radio" name="r_collage_3" id="r_image_3" value="image" onclick="onCheckToggle('r_image_3', 'image_collage_3', 'video_collage_3')">
			                      Image (Image size should be 850 x 500 for better preview)
			                    </label>
		                  	</div>
		                  	<div class="radio">
			                    <label>
			                      <input type="radio" name="r_collage_3" id="r_video_3" value="video" onclick="onCheckToggle('r_video_3', 'video_collage_3', 'image_collage_3')">
			                      Video (Enter the video embed url)
			                    </label>
                  			</div>
		                  	<input type="file" name="collage_3" id="image_collage_3">
		                  	<input type="url" name="collage_3" placeholder="Enter the video url" class="form-control" id="video_collage_3">
		                </div>

					    <div class="form-group m-t-15">
		                  	<label for="collage_4">Collage 4</label>

		                  	<div class="radio">
			                    <label>
			                      <input type="radio" name="r_collage_4" id="r_image_4" value="image" onclick="onCheckToggle('r_image_4', 'image_collage_4', 'video_collage_4')">
			                      Image (Image size should be 850 x 500 for better preview)
			                    </label>
		                  	</div>
		                  	<div class="radio">
			                    <label>
			                      <input type="radio" name="r_collage_4" id="r_video_4" value="video" onclick="onCheckToggle('r_video_4', 'video_collage_4', 'image_collage_4')">
			                      Video (Enter the video embed url)
			                    </label>
                  			</div>
		                  	<input type="file" name="collage_4" id="image_collage_4">
		                  	<input type="url" name="collage_4" placeholder="Enter the video url" class="form-control" id="video_collage_4">
		                </div>

					    <div class="form-group m-t-15">
		                  	<label for="collage_5">Collage 5</label>

		                  	<div class="radio">
			                    <label>
			                      <input type="radio" name="r_collage_5" id="r_image_5" value="image" onclick="onCheckToggle('r_image_5', 'image_collage_5', 'video_collage_5')">
			                      Image (Image size should be 850 x 500 for better preview)
			                    </label>
		                  	</div>
		                  	<div class="radio">
			                    <label>
			                      <input type="radio" name="r_collage_5" id="r_video_5" value="video" onclick="onCheckToggle('r_video_5', 'video_collage_5', 'image_collage_5')">
			                      Video (Enter the video embed url)
			                    </label>
                  			</div>
		                  	<input type="file" name="collage_5" id="image_collage_5">
		                  	<input type="url" name="collage_5" placeholder="Enter the video url" class="form-control" id="video_collage_5">
		                </div>



					    <div class="form-group m-t-15">
					    	<label for="status">Status*</label>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('status', 'active', true) }}Active
			                    </label>
		                  	</div>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('status', 'inactive', false) }}Inactive
			                    </label>
		                  	</div>
					    </div>
					    <div class="form-group m-t-15">
					    	{{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
					    </div>
					{{Form::close()}}
				</div>
			</div><!-- /.box -->
		</div>
	</div>
</div>
@endsection
@section('after_scripts')
	<script type="text/javascript">
 	window.onload = function() {
	    document.querySelectorAll('[name^="collage_"]')[0].style.display = 'none';
	    document.querySelectorAll('[name^="collage_"]')[1].style.display = 'none';
	    document.querySelectorAll('[name^="collage_"]')[2].style.display = 'none';
	    document.querySelectorAll('[name^="collage_"]')[3].style.display = 'none';
	    document.querySelectorAll('[name^="collage_"]')[4].style.display = 'none';
	    document.querySelectorAll('[name^="collage_"]')[5].style.display = 'none';
	    document.querySelectorAll('[name^="collage_"]')[6].style.display = 'none';
	    document.querySelectorAll('[name^="collage_"]')[7].style.display = 'none';
	    document.querySelectorAll('[name^="collage_"]')[8].style.display = 'none';
	    document.querySelectorAll('[name^="collage_"]')[9].style.display = 'none';
	}	
	function onCheckToggle(radio_id, active_id, inactive_id) {
		// console.log(radio_id);

	    if (document.getElementById(radio_id).checked) {
	    
	        document.getElementById(active_id).style.display = 'block';
	        document.getElementById(inactive_id).style.display = 'none';
	        // document.getElementById(inactive_id).value = '';
	    } 
	}
	</script>
@endsection