@extends('backpack::eventmenulayout')
<link href="{{ asset('custom_css/ticket_widget.css') }}" rel="stylesheet">
@section('header')

<section class="content-header">
  <h1>
  <span class="text-capitalize">Team Members</span>
  <small> <span class="text-lowercase">All team members </span></small>
  </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
    <li class="active">Team members</li>
    </ol>
</section>
@endsection

@section('content')
<div class="row">
  	<div class="row mb-2">
		<div class="col-md-8 col-md-offset-2">
        	@if (session()->has('success'))
				<div class="alert alert-success">
					{{ session()->get('success') }}
				</div>
			@endif

			@if (session()->has('error'))
				<div class="alert alert-danger">
					{{ session()->get('error') }}
				</div>
			@endif
    	</div>
		<!-- THE ACTUAL CONTENT -->
		<div class="col-md-10 col-md-offset-1">
			<div class="box">
				<div class="box-header">
					<a href="{{route('team-member.create')}}" class="btn btn-primary">Add member</a>
					<div id="datatable_button_stack" class="pull-right text-right"></div>
				</div>
				<div class="box-body table-responsive">
			        <table id="crudTable" class="table table-striped table-hover display">
			           <thead>
			              <tr>
			                {{-- Table columns --}}  
			                    <th data-orderable="true"># </th>
			                    <th data-orderable="true">Name</th>
			                    <th data-orderable="true">Email</th>
			                    <th data-orderable="true">Role</th>
			                    <th data-orderable="true">Created at</th>
			                    <th data-orderable="true">Action</th>
			              </tr>
			            </thead>
			            <tbody>
			              	@if($data->count())
				              	@foreach($data as $key => $value)
					              	<tr>
						                <td>{{ ++ $key}}</td>
						                <td>{{$value->name}}</td>
						                <td>{{$value->email}}</td>
						                <td>{{$value->role == '2' ? 'Sub Admin' : ($value->role == '3' ? 'Other Admin' : ($value->role == '4' ? 'Guard' : '') )}}</td>
						                <td>{{date('d-m-y', strtotime($value->created_at))}}</td>
						                <td>

						                  <a href="{{route('team-member.edit', $value->id)}}"><button type="button" class="btn btn-sm btn-flat btn-default" readonly><i class="fa fa-edit"></i> | <b>EDIT</b></span></button></a>

						                  {{  Form::open(array('route' => ['team-member.destroy', $value['id']], 'class' => 'delete', 'method' => 'DELETE')) }}
                                            {{ Form::button('<i class=" fa fa-trash" ></i> DELETE', array('class' => 'btn btn-sm btn-flat btn-danger', 'onClick' => " return confirm('Are you sure, you want to delete this record?')", 'title' => "Delete", 'type' => 'submit')) }}
                                            {{Form::close()}}

						                </td>
					              	</tr>
				            	@endforeach
				            @else
				            	No record found
			           		@endif
			            </tbody>
			      	</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>
</div>
@endsection