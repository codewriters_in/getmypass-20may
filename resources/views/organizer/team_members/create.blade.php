@extends('backpack::eventmenulayout')
<link href="{{ asset('custom_css/ticket_widget.css') }}" rel="stylesheet">
@section('header')

<section class="content-header">
  <h1>
  <span class="text-capitalize">Team Members</span>
  <small> <span class="text-lowercase">All team members </span></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
    <li class="active">Team members</li>
  </ol>
</section>
@endsection

@section('content')
<div class="row">
  	<div class="row mb-2">
		<div class="col-md-8 col-md-offset-2">
			@if ($errors->any())
				<div class="alert alert-danger">
			        {{ implode('', $errors->all(':message')) }}
			    </div>
			@endif        	
        	@if (session()->has('success'))
				<div class="alert alert-success">
					{{ session()->get('success') }}
				</div>
			@endif

			@if (session()->has('error'))
				<div class="alert alert-danger">
					{{ session()->get('error') }}
				</div>
			@endif
    	</div>
		<!-- THE ACTUAL CONTENT -->
		<div class="col-md-10 col-md-offset-1">
			<div class="box">
		    	<div class="box-body">
		    		{{Form::open(['route' => ['team-member.store'], 'method' => 'POST'])}}

					    <div class="form-group m-t-15">
					    	<label for="name">Name*</label>
					    	{{Form::text('name', '', ['min' => 4, 'max' => 20, 'class' => 'form-control', 'required'])}}
					    </div>
					    <div class="form-group m-t-15">
					    	<label for="email">Email*</label>
					    	{{Form::email('email', '', ['class' => 'form-control', 'required'])}}
					    </div>

					    <div class="form-group m-t-15">
					      	<label for="role">Choose role*</label>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('role', "2", true) }}Sub Admin
			                    </label>
		                  	</div>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('role', "3", false) }}Other Admin
			                    </label>
		                  	</div>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('role', "4", false) }}Guard
			                    </label>
		                  	</div>
		                </div>
					    <div class="form-group m-t-15">
					    	<label for="password">Password*</label>
					    	{{Form::password('password', ['min' => 5, 'max' => 20, 'class' => 'form-control', 'required'])}}
					    </div>
					    <div class="form-group m-t-15">
					    	<label for="password_confirmation">Confirm Password*</label>
					    	{{Form::password('password_confirmation', ['min' => 5, 'max' => 20, 'class' => 'form-control', 'required'])}}
					    </div>
					    <div class="form-group m-t-15">
					    	<label for="is_available">User active*</label>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('is_active', 1, true) }}Yes
			                    </label>
		                  	</div>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('is_active', 0, false) }}No
			                    </label>
		                  	</div>
					    </div>
					    <div class="form-group m-t-15">
					    	{{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
					    </div>
					{{Form::close()}}
				</div>
			</div><!-- /.box -->
		</div>
	</div>
</div>
@endsection