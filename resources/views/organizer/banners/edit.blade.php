@extends('backpack::eventmenulayout')
@section('header')

<section class="content-header">
  <h1>
  <span class="text-capitalize">Organizer</span>
  <small> <span class="text-lowercase">Banner</span></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="">admin</a></li>
    <li class="active">Edit Banner</li>
  </ol>
</section>
@endsection

@section('content')
<div class="row">
  	<div class="row mb-2">
		<div class="col-md-8 col-md-offset-2">
			@if ($errors->any())
				<div class="alert alert-danger">
			        {{ implode('', $errors->all(':message')) }}
			    </div>
			@endif        	
        	@if (session()->has('success'))
				<div class="alert alert-success">
					{{ session()->get('success') }}
				</div>
			@endif

			@if (session()->has('error'))
				<div class="alert alert-danger">
					{{ session()->get('error') }}
				</div>
			@endif
    	</div>
		<!-- THE ACTUAL CONTENT -->
		<div class="col-md-10 col-md-offset-1">
			<div class="box">
		    	<div class="box-body">
		    		{{Form::model($data, ['route' => ['banner.update', $data->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data'])}}
					    <div class="form-group m-t-15">
					    	<label for="banner">Banner Image* (Ideal image size for the banner should be 1320 x 450)</label>

						    <div class="imageupload panel panel-default">
								<img class="thumbnail" src="{{asset($data->banner_path . $data->banner)}}" style="max-width: 250px; max-height: 250px;">						      	
						      <div class="file-tab panel-body">
						        <div>
						          <button type="button" class="btn btn-primary btn-file">
						          <span>Browse file</span>
						          <input type="file" name="banner">
						          </button>
						          <button type="button" class="btn btn-danger">Remove</button>
						        </div>
						      </div>
						    </div>
						</div>
					    <div class="form-group m-t-15">
					    	<label for="banner_status">Banner Status*</label>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('banner_status', 'active', ($data->banner_status == 'active')?:false) }}Active
			                    </label>
		                  	</div>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('banner_status', 'inactive', ($data->banner_status == 'inactive')?:false) }}Inactive
			                    </label>
		                  	</div>
					    </div>


					    <div class="form-group m-t-15">
					    	<label for="banner">Logo Image* (Ideal image size for the logo should be 150 x 75)</label>

						    <div class="imageupload panel panel-default">
								<img class="thumbnail" src="{{asset($data->logo_path . $data->logo)}}" style="max-width: 250px; max-height: 250px;">						      	
						      <div class="file-tab panel-body">
						        <div>
						          <button type="button" class="btn btn-primary btn-file">
						          <span>Browse file</span>
						          <input type="file" name="logo">
						          </button>
						          <button type="button" class="btn btn-danger">Remove</button>
						        </div>
						      </div>
						    </div>
						</div>

					    <div class="form-group m-t-15">
					    	<label for="logo_status">Logo Status*</label>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('logo_status', 'active', ($data->logo_status == 'active')?:false) }}Active
			                    </label>
		                  	</div>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('logo_status', 'inactive', ($data->logo_status == 'inactive')?:false) }}Inactive
			                    </label>
		                  	</div>
					    </div>
					    <div class="form-group m-t-15">
					    	{{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
					    </div>
					{{Form::close()}}
				</div>
			</div><!-- /.box -->
		</div>
	</div>
</div>
@endsection
@section('after_scripts')
<script type="text/javascript" src="{{asset('js/img-upload.js')}}"></script>
<script>
    var $imageupload = $('.imageupload');
    $imageupload.imageupload();

</script>
@endsection