@extends('backpack::eventmenulayout')
@section('header')

<section class="content-header">
  <h1>
  <span class="text-capitalize">Organizer</span>
  <small> <span class="text-lowercase">Gallery</span></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
    <li class="active">Create Gallery</li>
  </ol>
</section>
@endsection

@section('content')
<div class="row">
  	<div class="row mb-2">
		<div class="col-md-8 col-md-offset-2">
			@if ($errors->any())
				<div class="alert alert-danger">
			        {{ implode('', $errors->all(':message')) }}
			    </div>
			@endif        	
        	@if (session()->has('success'))
				<div class="alert alert-success">
					{{ session()->get('success') }}
				</div>
			@endif

			@if (session()->has('error'))
				<div class="alert alert-danger">
					{{ session()->get('error') }}
				</div>
			@endif
    	</div>
		<!-- THE ACTUAL CONTENT -->
		<div class="col-md-10 col-md-offset-1">
			<div class="box">
		    	<div class="box-body">
		    		{{Form::open(['route' => ['gallery.store'], 'method' => 'POST', 'enctype' => 'multipart/form-data'])}}
					    <div class="form-group m-t-15">
					    	<label for="name">Title*</label>
					    	{{Form::text('title', '', ['min' => 3, 'max' => 50, 'class' => 'form-control', 'required'])}}
					    </div>
					    <div class="form-group m-t-15">
					    	<label for="banner">Gallery Image* (Ideal image size should be 850 x 400)</label>

						    <div class="imageupload panel panel-default">
						      <div class="file-tab panel-body">
						        <div>
						          <button type="button" class="btn btn-primary btn-file">
						          <span>Browse file</span>
						          <input type="file" name="gallery_image">
						          </button>
						          <button type="button" class="btn btn-danger">Remove</button>
						        </div>
						      </div>
						    </div>
						</div>
					    <div class="form-group m-t-15">
					    	<label for="banner_status">Status*</label>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('status', 'active', true) }}Active
			                    </label>
		                  	</div>
					      	<div class="radio">
			                    <label>
	  								{{ Form::radio('status', 'inactive', false) }}Inactive
			                    </label>
		                  	</div>
					    </div>


					    <div class="form-group m-t-15">
					    	{{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
					    </div>
					{{Form::close()}}
				</div>
			</div><!-- /.box -->
		</div>
	</div>
</div>
@endsection
@section('after_scripts')
<script type="text/javascript" src="{{asset('js/img-upload.js')}}"></script>
<script>
            var $imageupload = $('.imageupload');
            $imageupload.imageupload();

        </script>
@endsection