@extends('backpack::eventmenulayout')
<link href="{{ asset('custom_css/ticket_widget.css') }}" rel="stylesheet">
@section('header')

<section class="content-header">
  <h1>
  <span class="text-capitalize">Organizer</span>
  <small> <span class="text-lowercase">Galleries </span></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
    <li class="active">Gallery</li>
  </ol>
</section>
@endsection

@section('content')
<div class="row">
  	<div class="row mb-2">
		<div class="col-md-8 col-md-offset-2">
        	@if (session()->has('success'))
				<div class="alert alert-success">
					{{ session()->get('success') }}
				</div>
			@endif

			@if (session()->has('error'))
				<div class="alert alert-danger">
					{{ session()->get('error') }}
				</div>
			@endif
    	</div>
		<!-- THE ACTUAL CONTENT -->
		<div class="col-md-10 col-md-offset-1">
			<div class="box">
				<div class="box-header">
					<a href="{{route('gallery.create')}}" class="btn btn-primary">Add gallery</a>
					<div id="datatable_button_stack" class="pull-right text-right"><strong>Note:</strong> Atleast add 5 gallery images</div>
				</div>
				<div class="box-body table-responsive">
			        <table id="crudTable" class="table table-striped table-hover display">
			           <thead>
			              <tr>
			                {{-- Table columns --}}  
			                    <th data-orderable="true">#</th>
			                    <th data-orderable="true">Title</th>
			                    <th data-orderable="true">Gallery Image</th>
			                    <th data-orderable="true">Status</th>
			                    <th data-orderable="true">Action</th>
			              </tr>
			            </thead>
			            <tbody>
			            	@if($data->count())
			            		@foreach($data as $key => $value)
					              	<tr>
					              		<td>{{++$key}}</td>
					              		<td>{{ucfirst($value->title)}}</td>
						                <td>
						                	<div class="attachment-block">
						                		<img class="attachment-img" src="{{asset($value->gallery_path . $value->gallery_image)}}">
						                	</div>
						                </td>
						                <td>{{ucfirst($value->status)}}</td>
						                <td>

						                  <a href="{{route('gallery.edit', $value->id)}}"><button type="button" class="btn btn-sm btn-flat btn-default" readonly><i class="fa fa-edit"></i> | <b>EDIT</b></span></button></a>

						                  {{  Form::open(array('route' => ['gallery.destroy', $value['id']], 'class' => 'delete', 'method' => 'DELETE')) }}
		                                    {{ Form::button('<i class=" fa fa-trash" ></i> DELETE', array('class' => 'btn btn-sm btn-flat btn-danger', 'onClick' => " return confirm('Are you sure, you want to delete this record?')", 'title' => "Delete", 'type' => 'submit')) }}
		                                    {{Form::close()}}

						                </td>
					              	</tr>
					            @endforeach
				            @else
				            	No gallery found
				            @endif
			            </tbody>
			      	</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>
</div>
@endsection