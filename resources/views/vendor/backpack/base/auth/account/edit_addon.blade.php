@extends('backpack::layout')

@section('after_styles')
<style media="screen">
    .backpack-profile-form .required::after {
        content: ' *';
        color: red;
    }
</style>
@endsection

@section('header')
<section class="content-header">

    <h1>
        {{ trans('backpack::base.my_account') }}
    </h1>

    <ol class="breadcrumb">

        <li>
            <a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a>
        </li>

        <li>
            <a href="{{ route('backpack.account.info') }}">{{ trans('backpack::base.my_account') }}</a>
        </li>

        <li class="active">
            Add Add-On User
        </li>

    </ol>

</section>
@endsection

@section('content')
<div class="row">
    <div class="col-md-3">
        @include('backpack::auth.account.sidemenu')
    </div>
    <div class="col-md-6">
        <div class="box">
        <div class="box-header with-border">
                <div class="box-title">Linked Add-On Users</div>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped table-hover display">
                    
                    <tbody>
                    @if($addons->count())
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Actions</th>
                        </tr>
                        @foreach($addons as $addon)
                        
                        <tr>
                            <td>{{$addon->name}}</td>
                            <td>{{$addon->email}}</td>
                            @if($addon->id != $id)
                            <td><a href="edit-addon/{{$addon->id}}"><button class="btn btn-sm btn-default" title="Edit"><i class="fa fa-edit"></i></button></a>
                            &nbsp;
                            @if($addon->is_active == 1)
                            <a href="../change-status/{{$addon->id}}"><button class="btn btn-sm btn-success" title="Click to Disable"><i class="fa fa-check"></i></button></a>
                            @else
                            <a href="../change-status/{{$addon->id}}"><button class="btn btn-sm btn-warning" title="Click to Enable"><i class="fa fa-close"></i></button></a>
                            @endif
                            &nbsp;
                            <a href="delete-addon/{{$addon->id}}"><button class="btn btn-sm btn-danger" title="Delete"><i class="fa fa-trash"></i></button></a></td>
                            @else
                            <td><label class="label label-default">Editing</label></td>
                            @endif
                        </tr>
                        
                        @endforeach
                        @else
                        No Addon Users
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box box-default">
            <div class="box-header with-border">
                <div class="box-title">Edit Add-On User</div>
            </div>

            <div class="box-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('backpack.account.update-addon', $edit_addons['id']) }}">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="{{ $edit_addons['id'] }}">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <div class="col-md-12">
                        <input type="text" class="form-control" name="name" value="{{ old('name') ? old('name') : $edit_addons['name'] }}">

                            @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="col-md-12">
                            <input type="email" class="form-control" name="email" value="{{ old('email') ? old('email') : $edit_addons['email'] }}">

                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="col-md-12">
                            <input type="password" class="form-control" name="password" placeholder="{{ trans('backpack::base.password') }}">

                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        
                        <div class="col-md-12">
                            <input type="password" class="form-control" name="password_confirmation" placeholder="{{ trans('backpack::base.confirm_password') }}">

                            @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <button type="submit" class="btn btn-warning btn-sm">
                        <i class="fa fa-btn fa-check"></i> <b>Update</b>
                    </button>
                    &nbsp;
                    <a href="/organiser/addon-user"><button type="button" class="btn btn-primary btn-sm">
                        <i class="fa fa-btn fa-chevron-left"></i> <b>Back</b>
                    </button></a>
                </form>


            </div>
        </div>
    </div>
@endsection
