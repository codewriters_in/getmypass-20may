@extends('backpack::layout')

@section('header')
<section class="content-header">

    <h1>
        {{ trans('backpack::base.my_account') }}
    </h1>

    <ol class="breadcrumb">

        <li>
            <a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a>
        </li>

        <li>
            <a href="{{ route('backpack.account.info') }}">{{ trans('backpack::base.my_account') }}</a>
        </li>

        <li>
            Add Add-On User
        </li>

        <li class="active">
            Bank Details
        </li>

    </ol>

</section>
@endsection

@section('content')
<div class="row">
    <div class="col-md-3">
        @include('backpack::auth.account.sidemenu')
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-header with-border">
                <div class="box-title">Bank Details</div>
            </div>
            <div class="box-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('backpack.account.bank_details') }}">
                    {!! csrf_field() !!}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <div class="col-md-12">
                        <input type="text" class="form-control" name="name" value="@if (!empty($bank_details->ac_name)){{ $bank_details->ac_name }} @endif" placeholder="{{ trans('backpack::base.name') }}">
                            
                            

                            @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('ac_number') ? ' has-error' : '' }}">
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="ac_number" value="@if (!empty($bank_details->ac_number)){{ $bank_details->ac_number }} @endif" placeholder="Account Number">

                            @if ($errors->has('ac_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('ac_number') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('ifsc_code') ? ' has-error' : '' }}">
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="ifsc_code" value="@if (!empty($bank_details->ifsc_code)){{ $bank_details->ifsc_code }} @endif" placeholder="IFSC Code">

                            @if ($errors->has('ifsc_code'))
                            <span class="help-block">
                                <strong>{{ $errors->first('ifsc_code') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success btn-sm">
                        <i class="fa fa-btn fa-plus"></i> Update
                    </button>
                </form>

            </div>
        </div>
    </div>
    @endsection
