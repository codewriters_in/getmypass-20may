@if (Auth::check())

    <!-- Left side column. contains the sidebar -->

    <aside class="main-sidebar">

      <!-- sidebar: style can be found in sidebar.less -->

      <section class="sidebar">

        <!-- Sidebar user panel -->

        @include('backpack::inc.sidebar_user_panel')



        <!-- sidebar menu: : style can be found in sidebar.less -->

        <ul class="sidebar-menu">

          <li class="header"><b>MY PANEL</b></li>

          <!-- ================================================ -->

          <!-- ==== Recommended place for admin menu items ==== -->

          <!-- ================================================ -->


          @if(Auth::user()->role == '1' || Auth::user()->role == '2' || Auth::user()->role == '4')
            <li><a href="{{ backpack_url('eventlist') }}"><i class="fa fa-fire"></i><span><b>Events</b></span></a></li>
            
            @if(Auth::user()->role == '1')
              <li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span><b>DASHBOARD</b></span></a></li>
              <li><a href="{{ route('team-member.index') }}"><i class="fa fa-users"></i> <span><b>Team Members</b></span></a></li>
            @endif
            
            @if(Auth::user()->role != '4')


              <li><a href="{{route('banner.index')}}"><i class="fa fa-shopping-cart"></i> <span><b>Banner</b></span></a></li>

              <li><a href="{{route('gallery.index')}}"><i class="fa fa-image"></i><span><b>Gallery</b></span></a></li>


              <li class="header"><b>ADMINISTRATION</b></li>

            @endif
          @endif
          {{-- @if(Auth::User()->hasRole('Admin')) --}}

          {{-- <li class="treeview">

          <a href="#"><i class="fa fa-male"></i> <span><b>MANAGE USERS & ROLES</b></span> <i class="fa fa-angle-left pull-right"></i></a>

          <ul class="treeview-menu">

          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/user') }}"><i class="fa fa-plus"></i> <span><b>USERS</b></span></a></li>

          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/role') }}"><i class="fa fa-bookmark"></i> <span><b>ROLES</b></span></a></li>

          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/permission') }}"><i class="fa fa-microchip"></i> <span>Permissions</span></a></li>

          </ul>

          </li>

          <li><a href="{{  backpack_url('registration') }}"><i class="fa fa-plus-square"></i> <span><b>REGISTRATIONS</b></span></a></li>

          <li><a href="{{  backpack_url('elfinder') }}"><i class="fa fa-archive"></i> <span><b>FILE MANAGER</b></span></a></li>

          <li><a href="{{ backpack_url('log') }}"><i class="fa fa-terminal"></i> <span><b>LOGS</b></span></a></li>
 --}}
          {{-- @endif --}}



          <!-- ======================================= -->

          {{-- <li class="header">Other menus</li> --}}

        </ul>

      </section>

      <!-- /.sidebar -->

    </aside>

@endif

