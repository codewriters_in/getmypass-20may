@extends('backpack::eventmenulayout')
@section('header')
	<section class="content-header">
	  <h1>
        <span class="text-capitalize">Withdrawal Request</span>
	  </h1>
	</section>

@endsection

@section('content')
  	<div class="row mb-2">
		<div class="col-md-8 col-md-offset-2">
			@if ($errors->any())
				<div class="alert alert-danger">
			        {{ implode('', $errors->all(':message')) }}
			    </div>
			@endif        	
        	@if (session()->has('success'))
				<div class="alert alert-success">
					{{ session()->get('success') }}
				</div>
			@endif

			@if (session()->has('error'))
				<div class="alert alert-danger">
					{{ session()->get('error') }}
				</div>
			@endif
    	</div>
    </div>
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="info-box">
                <span style="background-color: #f56954" class="info-box-icon"><i style="color: white" class="fa fa-inr"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Total Amount</span>
                  <span class="info-box-number"> {{round($subtotal_amount, 2)}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="info-box">
                <span style="background-color: #00a65a" class="info-box-icon"><i style="color: white" class="fa fa-inr"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Withdrawal Amount</span>
                  <span class="info-box-number">{{round($withdrawal_amount, 2)}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="info-box">
                <span style="background-color: #f39c12" class="info-box-icon"><i style="color: white" class="fa fa-inr"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Remaining Amount</span>
                  <span class="info-box-number">{{round($available_amount, 2)}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
          </div>
        </div>
    <div class="row mb-2">
		<div class="col-md-4 ">
			<div class="box">

				<div class="box-header with-border">
	              <div class="user-block">
	                <span class="username"><a href="#">Withdrawal Request</a></span>
	                <span class="description">Available amount : ₹ {{$payable_amount}}</span>
	              </div>
	              <!-- /.user-block -->
	              <div class="box-tools">
	                {{-- <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Mark as read">
	                  <i class="fa fa-circle-o"></i></button> --}}
	                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
	                </button>
	                {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> --}}
	              </div>
	              <!-- /.box-tools -->
	            </div>
		    	<div class="box-body">
		    		@if($payable_amount > 0)
			    		{{Form::open(['route' => ['order.payment.withdrawal_request', $event_id]])}}

			    			@if($bank_details->count())
							    <div class="form-group m-t-15">
							      	<label for="role">Select A bank Account*</label>
							      	@foreach($bank_details as $key => $bank_detail)
								      	<div class="radio">
						                    <label>
				  								{{ Form::radio('bank_detail_id', $bank_detail->id, false, ['id' => 'bank_detail_id_' . $key]) }}{{$bank_detail->beneficiary_account_number}} ({{$bank_detail->beneficiary_ifsc_code}})
						                    </label>
					                  	</div>
					                @endforeach
				                </div>
			    				<i class="fa fa-plus"></i><a class="btn btn-success btn-xs" id="add_acc">Add Account</a>
			    			@endif
		    				<div class="hidden" id="payment_withdraw">
							    <div class="form-group m-t-15">
							      <label for="beneficiary_amount">Amount</label>
							      <input class="form-control" type="text" name="beneficiary_amount"  value="{{$payable_amount}}" >
							    </div>
							    <div class="form-group m-t-15">
							    	{{Form::submit('Withdraw', ['class' => 'btn btn-primary'])}}
							    </div>
							</div>
						{{Form::close()}}
		    			<div class="{{($bank_details->count()) ? 'hidden' : 'show'}}" id="acc_detail_form">
		    				{{Form::open(['route' => 'bank_detail.store'])}}
							    <div class="form-group m-t-15">
							      <label for="beneficiary_account_number">Account Number</label>
							      <input class="form-control" type="text" name="beneficiary_account_number"  value="" required>
							    </div>
							    <div class="form-group m-t-15">
							      <label for="beneficiary_name">Account Name</label>
							      <input class="form-control" type="text" name="beneficiary_name"  value="" required>
							    </div>
							    <div class="form-group m-t-15">
							      <label for="beneficiary_ifsc_code">IFSC Code</label>
							      <input class="form-control" type="text" name="beneficiary_ifsc_code"  value="" required>
							    </div>
							    <div class="form-group m-t-15">
							    	{{Form::submit('Add', ['class' => 'btn btn-primary', 'onclick' => "confirm('Are you sure the account details is correct?')"])}}
							    </div>
							{{Form::close()}}
						</div>
					@else
						You have already withdrawal the balance
					@endif
				</div>
			</div>
		</div>

		<div class="col-md-8 ">
			<div class="box">

				<div class="box-header with-border">
	              <div class="user-block">
	                <span class="username"><a href="#">Withdrawal Logs</a></span>
	                <span class="description">Withdrawal amount : ₹ {{round($withdrawal_amount, 2)}}</span>
	              </div>
	              <!-- /.user-block -->
	              <div class="box-tools">
	                {{-- <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Mark as read">
	                  <i class="fa fa-circle-o"></i></button> --}}
	                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
	                </button>
	                {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> --}}
	              </div>
	              <!-- /.box-tools -->
	            </div>
		    	<div class="box-body">
		    		@if($withdrawal_amount > 0)
						<div class="box-body table-responsive">
					        <table id="crudTable" class="table table-striped table-hover display">
					           <thead>
					              <tr>
					                {{-- Table columns --}}  
				                    <th data-orderable="true">#</th>
				                    <th data-orderable="true">Amount</th>
				                    <th data-orderable="true">Account</th>
				                    <th data-orderable="true">Withdrawal Date</th>
				                    <th data-orderable="true">Status</th>
					              </tr>
					            </thead>
					            <tbody>
					    			@foreach($payment_logs as $key => $log)

					    				<tr>
					    					<td>{{++$key}}</td>
					    					<td>₹ {{round($log->beneficiary_amount, 2)}}</td>
					    					<td>{{$log->bank_detail->beneficiary_account_number}} ({{$log->bank_detail->beneficiary_ifsc_code}} )</td>
					    					<td>{{date('D-M-Y H:i', strtotime($log->created_at))}}</td>

					    					<td class="btn-{{$log->status == 'pending' ? 'warning' : ($log->status == 'accepted' ? 'success' : 'danger')}}">{{ucfirst($log->status)}}</td>
					    				</tr>
					    			@endforeach
					            </tbody>
					      	</table>
						</div><!-- /.box-body -->
		    		@else
		    			No withdrawal records found
		    		@endif
				</div>
			</div>
		</div>
	</div>
@endsection
@section('after_scripts')
<script type="text/javascript">
	document.getElementById("add_acc").addEventListener("click", showAddAccForm);

	function showAddAccForm(){

		document.getElementById("acc_detail_form").classList.add('show');
		document.getElementById("acc_detail_form").classList.remove('hidden');
		document.getElementById("payment_withdraw").classList.add('hidden');
	}
	var bank_detail = document.getElementsByName('bank_detail_id')
	for(var i = 0; i < bank_detail.length; i++){

		bank_detail[i].addEventListener("click", showPaymentWithdraw);
	}
	

	function showPaymentWithdraw(){
		// alert("hello")
		document.getElementById("payment_withdraw").classList.add('show');
		document.getElementById("payment_withdraw").classList.remove('hidden');
		document.getElementById("acc_detail_form").classList.add('hidden');

	}


</script>
@endsection