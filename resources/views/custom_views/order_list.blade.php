@extends('backpack::eventmenulayout')
@section('header')
@endsection

@section('content')

<!-- Default box -->

  <div class="row">

		<div class="col-md-8 col-md-offset-2">
        	@if (session()->has('success'))
				<div class="alert alert-success">
					{!! session()->get('success') !!}
				</div>
			@endif

			@if (session()->has('error'))
				<div class="alert alert-danger">
					{!! session()->get('error') !!}
				</div>
			@endif
    	</div>


    <!-- THE ACTUAL CONTENT -->

    <div class="col-md-12">

      <div class="box">

        <div class="box-header ">

        </div>

    	<div class="col-md-6">
    		<div class="box-body table-responsive">
	        	<table id="crudTable" class="table table-striped table-hover display">
		           	<thead>
		              <tr>
		                    <th data-orderable="true"># </th>
		                    <th data-orderable="true">Order ID</th>
		                    <th data-orderable="true">Event </th>
		                    <th data-orderable="true">Amount </th>
		                    <th data-orderable="true">Action </th>
		              </tr>
		            </thead>
		            <tbody>
		            	@if($data->count())
			            	@foreach($data as $key => $value)

			            		<tr>
			            			<td>{!! ++ $key!!}</td>
			            			<td>{!!$value['payment_order_id']!!}</td>
			            			<td>{!!$value['event_title']!!}</td>
			            			<td><i class="fa fa-rupee"></i> {!!$amount = $value['amount'] - ($value['amount'] * (Auth::user()->commission/100))!!}</td>
			            			<td>
			            				@if($value->payment_request == "no")
			            					<a href="{!!route('withdraw.payment', $value['payment_order_id'])!!}" class="btn btn-success">Withdraw</a>
			            				@else
			            					Request already sent
			            				@endif 
			            			</td>
			            		</tr>
			            	@endforeach
			            @endif
		            </tbody>
	          	</table>
          	</div>
        </div>
        <div class="col-md-6">
	        <div class="box-body table-responsive">
	        	<table id="crudTable" class="table table-striped table-hover display">
		           	<thead>
		              <tr>
		                    <th data-orderable="true"># </th>
		                    <th data-orderable="true">Order ID</th>
		                    <th data-orderable="true">Event</th>
		                    <th data-orderable="true">Amount</th>
		                    <th data-orderable="true">Withdrawal date</th>
		              </tr>
		              <tr>
		              	<td>-</td>
		              	<td>-</td>
		              	<td>-</td>
		              	<td>-</td>
		              	<td>-</td>
		              </tr>
		            </thead>
		            <tbody>
		            </tbody>
	          	</table>
        	</div>

        </div><!-- /.box-body -->

      </div><!-- /.box -->

    </div>

  </div>


@endsection