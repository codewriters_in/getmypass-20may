@extends('backpack::eventmenulayout')

@section('header')
  <section class="content-header">
    <h1>
    <span class="text-capitalize">Orders</span>
    <small> <span class="text-lowercase">All orders </span></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
      <li><a class="text-capitalize">Orders</a></li>
      {{-- <li class="active">List</li> --}}
    </ol>
  </section>
@endsection
@section('content')
<!-- Default box -->
<div class="row">
  <!-- THE ACTUAL CONTENT -->
  <div class="col-md-12">
    <div class="box">
      <div class="box-header ">
        <div id="datatable_button_stack" class="pull-right text-right">
          {{-- <a href="{{ url('')}}/organizer/event/create" class="btn btn-success">Add Event <i class="fa fa-plus"></i></a> --}}
        </div>
      </div>
      <div class="box-body table-responsive">
        <table id="crudTable" class="table table-striped table-hover display">
          <thead>
            <tr>
              {{-- Table columns --}}
              {{-- <th data-orderable="true"># </th> --}}
              <th data-orderable="true">Order#</th>
              <th data-orderable="true">Client</th>
              <th data-orderable="true">Event</th>
              <th data-orderable="true">Price</th>
              <th data-orderable="true">Quantity</th>
              <th data-orderable="true">Total</th>
              {{-- <th data-orderable="true">Tax @ 18%</th> --}}
              {{-- <th data-orderable="true">Subtotal</th> --}}
              <th data-orderable="true">Order Date</th>
              <th data-orderable="true">Status</th>
              <th data-orderable="true">Earning</th>
            </tr>
          </thead>
          <tbody>
            @if($data->count())
              @foreach($data as $key => $value)
              <tr>
                {{-- <td>{{++ $key}}</td> --}}
                <td>{{$value->user_payment->payment_order_id}}</td>
                <td>{{$value->user->name}}</td>
                <td>{{$value->event->title}}</td>
                <td>₹ {{$value->price}}</td>
                <td>{{$value->qty}}</td>
                <td>₹ {{$value->total_price}}</td>
                {{-- <td>{{$value->total_price * (18/100)}}</td> --}}
                {{-- <td>{{$value->user_payment->amount}}</td> --}}
                <td>{{date('d-m-y', strtotime($value->created_at))}}</td>
                <td>{{$value->status}}</td>
                <td>₹ {{$value->total_price - $value->commission}}</td>
                  {{-- @if($value->user_payment->payment_request == "no")
                    <a href="{!!route('withdraw.payment', $value->user_payment['payment_order_id'])!!}" class="btn btn-success">Withdraw</a>
                  @else
                    Request already sent
                  @endif --}} 
                
              </tr>
              @endforeach
              <tr class="bg-blue">
                <th colspan="6" class="text-right">Earning Subtotal</th><td colspan="2" class="text-right">₹ {{$subtotal_amount}}</td>
                <td colspan="3" class="text-right">
                  @if($subtotal_amount > 0)
                    <a href="{{route('order.payment.withdraw', $event_id)}}" class="btn btn-success">Withdraw</a>
                  @endif
                </td>
              </tr>
            @else
              No record found
            @endif
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div>
</div>
@endsection
