@extends('backpack::eventmenulayout')

<link href="{{ asset('custom_css/event_widget.css') }}" rel="stylesheet">

@section('header')

<section class="content-header">
  <h1>
  <span class="text-capitalize">Events</span>
  <small> <span class="text-lowercase">All events </span></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
    <li><a href="" class="text-capitalize">Events</a></li>
    <li class="active">List</li>
  </ol>
</section>
@endsection



@section('content')

<!-- Default box -->

  <div class="row">

    <div class="col-md-8 col-md-offset-2">
          @if (session()->has('success'))
        <div class="alert alert-success">
          {{ session()->get('success') }}
        </div>
      @endif

      @if (session()->has('error'))
        <div class="alert alert-danger">
          {{ session()->get('error') }}
        </div>
      @endif
    </div>


    <!-- THE ACTUAL CONTENT -->

    <div class="col-md-12">

      <div class="box">

        <div class="box-header ">

          <div id="datatable_button_stack" class="pull-right text-right">
            <a href="{{ url('')}}/organizer/event/create" class="btn btn-success">Add Event <i class="fa fa-plus"></i></a>
          </div>

        </div>

        <div class="box-body table-responsive">
        <table id="crudTable" class="table table-striped table-hover display">
           <thead>
              <tr>
                {{-- Table columns --}}  
                    <th data-orderable="true">ID </th>
                    <th data-orderable="true">Name</th>
                    <th data-orderable="true">Venue Name</th>
                    <th data-orderable="true">Start Date</th>
                    <th data-orderable="true">Status</th>
                    <th data-orderable="true">Actions</th>
              </tr>
            </thead>
            <tbody>
              @if(isset($eventData) && !empty($eventData))
              @foreach($eventData as $eventData)
              <tr class="{{($eventData->end_date < Carbon\Carbon::now()) ? 'bg-dim' : 'bg-highlight'}}">
                <td>{{$eventData->id}}</td>
                <td>{{$eventData->title}}</td>
                <td>{{$eventData->venue_name}}</td>
                <td>{{$eventData->start_date}}</td>
                <td>{{ucfirst(($eventData->is_approved == 'yes' ? 'approved' : 'not approved'))}}</td>
                <td>
                  @if(Auth::user()->role != 4)
                    <a href="event/{{$eventData->id}}/manage"><button type="button" class="btn btn-sm btn-flat btn-primary" readonly><i class="fa fa-paperclip"></i> | <b>MANAGE</b></button></a>

                    <a href="{{route('ticket.index', $eventData->id)}}"><button type="button" class="btn btn-sm btn-flat btn-warning" readonly><i class="fa fa-edit"></i> | <b>Manage Tickets</b></span></button></a>
                    @if($eventData->order->count() < 1)
                      <a href="event/{{$eventData->id}}/edit"><button type="button" class="btn btn-sm btn-flat btn-default" readonly><i class="fa fa-edit"></i> | <b>EDIT</b></span></button></a>
                      
                      <button class="btn btn-danger" data-uid="{!!$eventData->id!!}"  data-toggle="modal" data-target="#delevent"><i class="fa fa-trash "></i></button>
                    @endif
                  @else
                    @if($eventData->end_date >= \Carbon\Carbon::now())
                      <a href="{{(route('check-in.index', $eventData->id))}}"><button type="button" class="btn btn-sm btn-flat btn-default" readonly><i class="fa fa-user"></i> | <b>Check In</b></span></button></a>
                    @endif
                  @endif
                </td>
              </tr>
            @endforeach
           @endif
            </tbody>
          </table>

        </div><!-- /.box-body -->

      </div><!-- /.box -->

    </div>

  </div>

<!-- Delete Modal -->
<div class="modal fade" id="delevent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Delete Event</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{!!route('organizer.event.delete')!!}" method="post">
        {!!csrf_field()!!}
        <input type="hidden"  name="id" value="9" class="uid">
      <div class="modal-body">
        Are You Sure ?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-danger">Delete</button>
      </div>
      </form>
    </div>
  </div>
</div>

@endsection



@section('after_styles')

  <!-- DATA TABLES -->

  <link href="{{ asset('vendor/adminlte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/crud.css') }}">

  <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/form.css') }}">

  <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/list.css') }}">



  <!-- CRUD LIST CONTENT - crud_list_styles stack -->

  @stack('crud_list_styles')

@endsection



@section('after_scripts')
    <!-- DATA TABLES SCRIPT -->
    <script src="{{ asset('vendor/adminlte/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/backpack/crud/js/crud.js') }}"></script>
    <script src="{{ asset('vendor/backpack/crud/js/form.js') }}"></script>
    <script src="{{ asset('vendor/backpack/crud/js/list.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>
  <!-- CRUD LIST CONTENT - crud_list_scripts stack -->
  @stack('crud_list_scripts')
<script type="text/javascript">
  $('#delevent').on('show.bs.modal', function (event) {

  var button = $(event.relatedTarget);
  var uid = button.data('uid');  
  var modal = $(this);
  $('.uid').val(uid);
  
})
</script>
@endsection

