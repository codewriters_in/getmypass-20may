@extends('backpack::eventmenulayout')
@section('header')

	<section class="content-header">

	  <h1>

        <span class="text-capitalize">Event Dashboard</span>

        <small>Ticket Customize</small>

	  </h1>

	  <ol class="breadcrumb">
        <li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>

	    <li><a href="#" class="text-capitalize">Events</a></li>

	    <li class="active">Customize Tiket</li>

	  </ol>

	</section>

@endsection



@section('content')

<div class="row" style="padding: 10px !important">

        <div class="col-md-12">

            <div class="box box-default">
              <div class="tab-pane active" id="ticket_design">
                    <form method="POST" action="{{route('ticket.customize.store', $event->id)}}" accept-charset="UTF-8" class="ajax "><input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                    <h4>Ticket Design</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="ticket_border_color" class="control-label required ">Ticket Border Color</label>
                                <div class="minicolors minicolors-theme-default minicolors-position-bottom minicolors-position-left"><input class="form-control colorpicker minicolors-input" placeholder="#000000" name="ticket_border_color" type="text" value="{!! $event->ticket_border_color!!}" id="ticket_border_color" size="7"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="background-color: rgb(0, 0, 0);"></span></span><div class="minicolors-panel minicolors-slider-hue"><div class="minicolors-slider minicolors-sprite"><div class="minicolors-picker" style="top: 0px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker"></div></div><div class="minicolors-grid minicolors-sprite" style="background-color: rgb(255, 0, 0);"><div class="minicolors-grid-inner"></div><div class="minicolors-picker" style="top: 150px; left: 0px;"><div></div></div></div></div></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="ticket_bg_color" class="control-label required ">Ticket Background Color</label>
                                <div class="minicolors minicolors-theme-default minicolors-position-bottom minicolors-position-left"><input class="form-control colorpicker minicolors-input" placeholder="#FFFFFF" name="ticket_bg_color" type="text" value="{!! $event->ticket_bg_color!!}" id="ticket_bg_color" size="7"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="background-color: rgb(204, 71, 71); opacity: 1;"></span></span><div class="minicolors-panel minicolors-slider-hue" style="display: none;"><div class="minicolors-slider minicolors-sprite"><div class="minicolors-picker" style="top: 0px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker"></div></div><div class="minicolors-grid minicolors-sprite" style="background-color: rgb(255, 0, 0);"><div class="minicolors-grid-inner"></div><div class="minicolors-picker" style="top: 30px; left: 97px;"><div></div></div></div></div></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="ticket_text_color" class="control-label required ">Ticket Text Color</label>
                                <div class="minicolors minicolors-theme-default minicolors-position-bottom minicolors-position-left"><input class="form-control colorpicker minicolors-input" placeholder="#000000" name="ticket_text_color" type="text" value="{!! $event->ticket_text_color!!}" id="ticket_text_color" size="7"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="background-color: rgb(0, 0, 0);"></span></span><div class="minicolors-panel minicolors-slider-hue"><div class="minicolors-slider minicolors-sprite"><div class="minicolors-picker" style="top: 0px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker"></div></div><div class="minicolors-grid minicolors-sprite" style="background-color: rgb(255, 0, 0);"><div class="minicolors-grid-inner"></div><div class="minicolors-picker" style="top: 150px; left: 0px;"><div></div></div></div></div></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="ticket_sub_text_color" class="control-label required ">Ticket Sub Text Color</label>
                                <div class="minicolors minicolors-theme-default minicolors-position-bottom minicolors-position-left"><input class="form-control colorpicker minicolors-input" placeholder="#000000" name="ticket_sub_text_color" type="text" value="{!! $event->ticket_sub_text_color!!}" id="ticket_sub_text_color" size="7"><span class="minicolors-swatch minicolors-sprite minicolors-input-swatch"><span class="minicolors-swatch-color" style="background-color: rgb(153, 153, 153);"></span></span><div class="minicolors-panel minicolors-slider-hue"><div class="minicolors-slider minicolors-sprite"><div class="minicolors-picker" style="top: 0px;"></div></div><div class="minicolors-opacity-slider minicolors-sprite"><div class="minicolors-picker"></div></div><div class="minicolors-grid minicolors-sprite" style="background-color: rgb(255, 0, 0);"><div class="minicolors-grid-inner"></div><div class="minicolors-picker" style="top: 60px; left: 0px;"><div></div></div></div></div></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="is_1d_barcode_enabled" class="control-label required">Show 1D barcode on tickets</label>
                                <select class="form-control" id="is_1d_barcode_enabled" name="is_1d_barcode_enabled"><option value="1" <?php if($event->is_1d_barcode_enabled==1) echo 'selected="selected"' ?>>Yes</option><option value="0" <?php if($event->is_1d_barcode_enabled==0) echo 'selected="selected"' ?>>No</option></select>
                            </div>
                        </div>
                        <div data-preview="#eventflyer" data-aspectratio="2" data-crop="1" class="form-group col-md-12 image">
    <div>
        <label>Event Flyer/Poster etc.</label>
            </div>
    <!-- Wrap the image or canvas element with a block element (container) -->
    <div class="row">
        <div class="col-sm-6" style="margin-bottom: 20px;">
            <img class ="mainImage" src="{{URL('')}}/assets/images/blog/blog-home/img3.jpg">
        </div>
                <div class="col-sm-3">
            <div class="docs-preview clearfix">
                <div id="eventflyer" class="img-preview preview-lg">
                    <img src="" style="display: block; min-width: 0px !important; min-height: 0px !important; max-width: none !important; max-height: none !important; margin-left: -32.875px; margin-top: -18.4922px; transform: none;">
                </div>
            </div>
        </div>
            </div>
    <div class="btn-group">
        <label class="btn btn-primary btn-file">
            Choose file <input type="file" accept="image/*" id="uploadImage" class="hide">
            <input type="hidden" id="hiddenImage" name="eventflyer" value="{{URL('')}}/assets/images/blog/blog-home/img3.jpg">
        </label>
                <button class="btn btn-default" id="rotateLeft" type="button" style="display: none;"><i class="fa fa-rotate-left"></i></button>
        <button class="btn btn-default" id="rotateRight" type="button" style="display: none;"><i class="fa fa-rotate-right"></i></button>
        <button class="btn btn-default" id="zoomIn" type="button" style="display: none;"><i class="fa fa-search-plus"></i></button>
        <button class="btn btn-default" id="zoomOut" type="button" style="display: none;"><i class="fa fa-search-minus"></i></button>
        <button class="btn btn-warning" id="reset" type="button" style="display: none;"><i class="fa fa-times"></i></button>
                <button class="btn btn-danger" id="remove" type="button"><i class="fa fa-trash"></i></button>
    </div>
    <link href="{{ asset('vendor/backpack/cropper/dist/cropper.min.css') }}" rel="stylesheet" type="text/css" />
        <style>
            .hide {
                display: none;
            }
            .image .btn-group {
                margin-top: 10px;
            }
            img {
                max-width: 100%; /* This rule is very important, please do not ignore this! */
            }
            .img-container, .img-preview {
                width: 100%;
                text-align: center;
            }
            .img-preview {
                float: left;
                margin-right: 10px;
                margin-bottom: 10px;
                overflow: hidden;
            }
            .preview-lg {
                width: 263px;
                height: 148px;
            }

            .btn-file {
                position: relative;
                overflow: hidden;
            }
            .btn-file input[type=file] {
                position: absolute;
                top: 0;
                right: 0;
                min-width: 100%;
                min-height: 100%;
                font-size: 100px;
                text-align: right;
                filter: alpha(opacity=0);
                opacity: 0;
                outline: none;
                background: white;
                cursor: inherit;
                display: block;
            }
        </style>
    
      </div>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <h4>Ticket Preview</h4>
                            <link media="all" type="text/css" rel="stylesheet" href="{{ asset('assets/css/ticket.css') }}">
                            
<style>
    .ticket {
        border: 1px solid {{$event->ticket_border_color}};
        background: {{$event->ticket_bg_color}} ;
        color: {{$event->ticket_sub_text_color}};
        border-left-color: {{$event->ticket_border_color}} ;
    }
    .ticket h4 {color: {{$event->ticket_text_color}};}
    .ticket .logo {
        border-left: 1px solid {{$event->ticket_border_color}};
        border-bottom: 1px solid {{$event->ticket_border_color}};


    }
</style>
<div class="ticket">
    <div class="logo">
        <img class="mainImage" style="height: 100%" src="{{URL('')}}/{{$event->image}}">
    </div>

    <div class="layout_even">
        <div class="event_details">
            <h4>EVENT</h4>
            Demo Event
            <h4>ORGANISER</h4>
            Demo Organiser
            <h4>Venue</h4>
            demovenue
            <h4>Start Date/Time</h4>
            Mar 18th 4:08PM
            <h4>End Date/Time</h4>
            Mar 18th 4:08PM
        </div>

        <div class="attendee_details">
            <h4>Name</h4>
            demo name
            <h4>Ticket Type</h4>
            General Admission
            <h4>Order Ref")</h4>
            #YLY9U73
            <h4>Getmypass Ref")</h4>
            #YLY9U73
            <h4>Price</h4>
            500 INR
        </div>
    </div>
<div class="barcode">
        {!! DNS2D::getBarcodeSVG('hello', "QRCODE", 6, 6) !!}
    </div>
    @if($event->is_1d_barcode_enabled)
        <div class="barcode_vertical">
            {!! DNS1D::getBarcodeSVG(12211221, "C39+", 1, 50) !!}
        </div>
    @endif
   
    <div class="foot">
        @lang("Ticket.footer")
    </div>
</div>

</div>
                        </div>
                    </div>
                    <div class="panel-footer mt15 text-right">
                        <input class="btn btn-success" type="submit" value="Save Changes">
                    </div>

                    </form>

                </div>

            </div>

        </div>

    </div>


@endsection
@section('after_scripts')
<link media="all" type="text/css" rel="stylesheet" href="{{ asset('assets/css/jquery-minicolors/jquery.minicolors.css') }}">

<script type="text/javascript" src="{{ asset('assets/css/jquery-minicolors/jquery.minicolors.min.js')}}"></script>
   <script>
        $(function () {

            /* Background selector */
            $('.bgImage').on('click', function (e) {
                $('.bgImage').removeClass('selected');
                $(this).addClass('selected');
                $('input[name=bg_image_path_custom]').val($(this).data('src'));

                var replaced = replaceUrlParam('{{route('page.event.show', ['event_id'=>$event->id])}}', 'bg_img_preview', $('input[name=bg_image_path_custom]').val());
                document.getElementById('previewIframe').src = replaced;
                e.preventDefault();
            });

            /* Background color */
            $('input[name=bg_color]').on('change', function (e) {
                var replaced = replaceUrlParam('{{route('page.event.show', ['event_id'=>$event->id])}}', 'bg_color_preview', $('input[name=bg_color]').val().substring(1));
                document.getElementById('previewIframe').src = replaced;
                e.preventDefault();
            });

            $('#bgOptions .panel').on('shown.bs.collapse', function (e) {
                var type = $(e.currentTarget).data('type');
                console.log(type);
                $('input[name=bg_type]').val(type);
            });

            $('input[name=bg_image_path], input[name=bg_color]').on('change', function () {
                //showMessage('Uploading...');
                //$('.customizeForm').submit();
            });

            /* Color picker */
            $('.colorpicker').minicolors();

            $('#ticket_design .colorpicker').on('change', function (e) {
                var borderColor = $('input[name="ticket_border_color"]').val();
                var bgColor = $('input[name="ticket_bg_color"]').val();
                var textColor = $('input[name="ticket_text_color"]').val();
                var subTextColor = $('input[name="ticket_sub_text_color"]').val();

                $('.ticket').css({
                    'border': '1px solid ' + borderColor,
                    'background-color': bgColor,
                    'color': subTextColor,
                    'border-left-color': borderColor
                });
                $('.ticket h4').css({
                    'color': textColor
                });
                $('.ticket .logo').css({
                    'border-left': '1px solid ' + borderColor,
                    'border-bottom': '1px solid ' + borderColor
                });
                $('.ticket .barcode').css({
                    'border-right': '1px solid ' + borderColor,
                    'border-bottom': '1px solid ' + borderColor,
                    'border-top': '1px solid ' + borderColor
                });

            });

            $('#enable_offline_payments').change(function () {
                $('.offline_payment_details').toggle(this.checked);
            }).change();
        });


    </script>

    <style type="text/css">
        .bootstrap-touchspin-postfix {
            background-color: #ffffff;
            color: #333;
            border-left: none;
        }

        .bgImage {
            cursor: pointer;
        }

        .bgImage.selected {
            outline: 4px solid #0099ff;
        }
    </style>
    <script>
        $(function () {

            var hash = document.location.hash;
            var prefix = "tab_";
            if (hash) {
                $('.nav-tabs a[href=' + hash + ']').tab('show');
            }

            $(window).on('hashchange', function () {
                var newHash = location.hash;
                if (typeof newHash === undefined) {
                    $('.nav-tabs a[href=' + '#general' + ']').tab('show');
                } else {
                    $('.nav-tabs a[href=' + newHash + ']').tab('show');
                }

            });

            $('.nav-tabs a').on('shown.bs.tab', function (e) {
                window.location.hash = e.target.hash;
            });

        });


    </script>
    <script src="{{ asset('vendor/backpack/cropper/dist/cropper.min.js') }}"></script>
        <script>
            jQuery(document).ready(function($) {
                // Loop through all instances of the image field
                $('.form-group.image').each(function(index){
                    // Find DOM elements under this form-group element
                    var $mainImage = $(this).find('.mainImage');
                    var $uploadImage = $(this).find("#uploadImage");
                    var $hiddenImage = $(this).find("#hiddenImage");
                    var $rotateLeft = $(this).find("#rotateLeft")
                    var $rotateRight = $(this).find("#rotateRight")
                    var $zoomIn = $(this).find("#zoomIn")
                    var $zoomOut = $(this).find("#zoomOut")
                    var $reset = $(this).find("#reset")
                    var $remove = $(this).find("#remove")
                    // Options either global for all image type fields, or use 'data-*' elements for options passed in via the CRUD controller
                    var options = {
                        viewMode: 2,
                        checkOrientation: false,
                        autoCropArea: 1,
                        responsive: true,
                        preview : $(this).attr('data-preview'),
                        aspectRatio : 0
                    };
                    var crop = $(this).attr('data-crop');

                    // Hide 'Remove' button if there is no image saved
                    if (!$mainImage.attr('src')){
                        $remove.hide();
                    }
                    // Initialise hidden form input in case we submit with no change
                    $hiddenImage.val($mainImage.attr('src'));


                    // Only initialize cropper plugin if crop is set to true
                    if(crop){

                        $remove.click(function() {
                            $mainImage.cropper("destroy");
                            $mainImage.attr('src','');
                            $hiddenImage.val('');
                            $rotateLeft.hide();
                            $rotateRight.hide();
                            $zoomIn.hide();
                            $zoomOut.hide();
                            $reset.hide();
                            $remove.hide();
                        });
                    } else {

                        $(this).find("#remove").click(function() {
                            $mainImage.attr('src','');
                            $hiddenImage.val('');
                            $remove.hide();
                        });
                    }

                    $uploadImage.change(function() {
                        var fileReader = new FileReader(),
                                files = this.files,
                                file;

                        if (!files.length) {
                            return;
                        }
                        file = files[0];

                        if (/^image\/\w+$/.test(file.type)) {
                            fileReader.readAsDataURL(file);
                            fileReader.onload = function () {
                                $uploadImage.val("");
                                if(crop){
                                    $mainImage.cropper(options).cropper("reset", true).cropper("replace", this.result);
                                    // Override form submit to copy canvas to hidden input before submitting
                                    $('form').submit(function() {
                                        var imageURL = $mainImage.cropper('getCroppedCanvas').toDataURL(file.type);
                                        $hiddenImage.val(imageURL);
                                        return true; // return false to cancel form action
                                    });
                                    $rotateLeft.click(function() {
                                        $mainImage.cropper("rotate", 90);
                                    });
                                    $rotateRight.click(function() {
                                        $mainImage.cropper("rotate", -90);
                                    });
                                    $zoomIn.click(function() {
                                        $mainImage.cropper("zoom", 0.1);
                                    });
                                    $zoomOut.click(function() {
                                        $mainImage.cropper("zoom", -0.1);
                                    });
                                    $reset.click(function() {
                                        $mainImage.cropper("reset");
                                    });
                                    $rotateLeft.show();
                                    $rotateRight.show();
                                    $zoomIn.show();
                                    $zoomOut.show();
                                    $reset.show();
                                    $remove.show();

                                } else {
                                    $mainImage.attr('src',this.result);
                                    $('.logoImage').attr('src',this.result);
                                    $hiddenImage.val(this.result);
                                    $remove.show();
                                }
                            };
                        } else {
                            alert("Please choose an image file.");
                        }
                    });

                });
            });
        </script>


@endsection

