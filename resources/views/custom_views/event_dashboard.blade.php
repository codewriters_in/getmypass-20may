@extends('backpack::eventmenulayout')
@section('header')
<section class="content-header">
  <h1>
  <span class="text-capitalize">Event Dashboard</span>
  <small>Your Event At a Glance.</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
    <li><a href="{{route('orgevent')}}" class="text-capitalize">Events</a></li>
    <li class="active">Event Dashboard</li>
  </ol>
</section>
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-default">
      <div class="box-header with-border">
        <div class="box-title"><span class="page-header">{{$data['event']->title}}</span></div>
        <div class="btn-group btn-xs pull-right" role="group" aria-label="Basic example">
          <a href="{{route('highlight.index', $data['event']->id)}}"><button class="btn btn-flat btn-sm btn-primary"><i class="fa fa-star"></i> | <b>HIGHLIGHTS</b></button></a>
          {{-- <a href="{{route('event.customize.create', $data['event']->id)}}"><button class="btn btn-flat btn-sm btn-primary"><i class="fa fa-code"></i> | <b>CUSTOMIZE EVENT</b></button></a> --}}
          <a href="{{route('ticket.index', $data['event']->id)}}"><button class="btn btn-flat btn-sm btn-primary"><i class="fa fa-ticket"></i> | <b>TICKETS</b></button></a>
          @if(Auth::user()->role == "1")
            <a href="{{route('order.index', $data['event']->id)}}"><button class="btn btn-flat btn-sm btn-primary"><i class="fa fa-shopping-cart"></i> | <b>ORDERS</b></button></a>
            <a href="{{route('event.order.guests', $data['event']->id)}}"><button class="btn btn-flat btn-sm btn-primary"><i class="fa fa-users"></i> | <b>Guests</b></button></a>

            <a href="{{route('order.payment.withdraw', $data['event']->id)}}"><button class="btn btn-flat btn-sm btn-primary"><i class="fa fa-eye"></i> | <b>View Details</b></button></a>
          @endif
          <a href="{{route('check-in.index', $data['event']->id)}}"><button class="btn btn-flat btn-sm btn-primary"><i class="fa fa-user"></i> | <b>CHECK-IN</b></button></a>
          
          {{-- <a href="{{route('event.order', $data['event']->id)}}"><button class="btn btn-flat btn-sm btn-primary"><i class="fa fa-eye"></i> | <b>View details</b></button></a> --}}
        </div>
      </div>
      <div class="box-body">
        At {{ucfirst($data['event']->venue_name)}} | Posted By {{ucfirst($data['event']->user->name)}}
        <br>
        <br>
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="info-box">
                <span style="background-color: #f56954" class="info-box-icon"><i style="color: white" class="fa fa-inr"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">REVENUE</span>
                  <span class="info-box-number">₹ {{$data['revenue']}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="info-box">
                <span style="background-color: #00a65a" class="info-box-icon"><i style="color: white" class="fa fa-shopping-cart"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">ORDERS</span>
                  <span class="info-box-number">{{$data['orders']}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="info-box">
                <span style="background-color: #f39c12" class="info-box-icon"><i style="color: white" class="fa fa-ticket"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">TICKETS SOLD</span>
                  <span class="info-box-number">{{$data['ticket_sold']}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            {{-- <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span style="background-color: #3c8dbc" class="info-box-icon"><i style="color: white" class="fa fa-eye"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">EVENT VIEWS</span>
                  <span class="info-box-number">120</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div> --}}
          </div>
        </div>
        
        <br>
        
        <div class="row">
          <div class="col-md-12">
            
            {{-- <div class="col-md-8">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <p class="box-title">Event Page Views</p>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body">
                  <div class="chart">
                    <canvas id="areaChart" style="height:250px"></canvas>
                  </div>
                </div>
                <!-- /.box-body -->
              </div>
            </div> --}}
            <div class="col-md-4">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <p class="box-title">Bookings by Type Of Ticket</p>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body">
                  <div class="chart">
                    <canvas id="pieChart" style="height:250px"></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="box-footer">
        <div>
          <h4>Share Event</h4>
          <!-- just add href= for your links, like this: -->
          <a href="https://facebook.com/"><button  class="btn btn-flat btn-facebook"><i class="fa fa-facebook"></i></button></a>
          <a href="https://plus.google.com/"><button  class="btn btn-flat btn-google"><i class="fa fa-google-plus"></i></button></a>
          <a href="https://twitter.com/"><button  class="btn btn-flat  btn-twitter"><i class="fa fa-twitter"></i></button></a>
          <a href="https://in.pinterest.com/"><button  class="btn btn-flat  btn-pinterest"><i class="fa fa-pinterest"></i></button></a>
          <a href="https://vk.com/"><button  class="btn btn-flat  btn-vk"><i class="fa fa-envelope"></i></button></a>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="{{ asset('vendor/adminlte') }}/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="{{ asset('vendor/adminlte') }}/plugins/chartjs/Chart.min.js"></script>
<script>
$(function () {
  var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
var pieChart = new Chart(pieChartCanvas);
var PieData = [
{
value: {{$data['ticket_stag']}},
color: "#f56954",
highlight: "#f56954",
label: "Stag"
},
{
value: {{$data['ticket_couple']}},
color: "#00a65a",
highlight: "#00a65a",
label: "Couple"
},
{
value: {{$data['ticket_ladies']}},
color: "#f39c12",
highlight: "#f39c12",
label: "Ladies"
},
{
value: {{$data['ticket_guest']}},
color: "#00c0ef",
highlight: "#00c0ef",
label: "Guest"
},
];
var pieOptions = {
//Boolean - Whether we should show a stroke on each segment
segmentShowStroke: true,
//String - The colour of each segment stroke
segmentStrokeColor: "#fff",
//Number - The width of each segment stroke
segmentStrokeWidth: 2,
//Number - The percentage of the chart that we cut out of the middle
percentageInnerCutout: 50, // This is 0 for Pie charts
//Number - Amount of animation steps
animationSteps: 100,
//String - Animation easing effect
animationEasing: "easeOutBounce",
//Boolean - Whether we animate the rotation of the Doughnut
animateRotate: true,
//Boolean - Whether we animate scaling the Doughnut from the centre
animateScale: false,
//Boolean - whether to make the chart responsive to window resizing
responsive: true,
// Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
maintainAspectRatio: true,
//String - A legend template
legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
};
//Create pie or douhnut chart
// You can switch between pie and douhnut using the method below.
pieChart.Doughnut(PieData, pieOptions);
//AREA Chart
// Get context with jQuery - using jQuery's .get() method.
var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
// This will get the first returned node in the jQuery collection.
var areaChart = new Chart(areaChartCanvas);
var areaChartData = {
labels: ["January", "February", "March", "April", "May", "June", "July"],
datasets: [
{
label: "Electronics",
fillColor: "rgba(210, 214, 222, 1)",
strokeColor: "rgba(210, 214, 222, 1)",
pointColor: "rgba(210, 214, 222, 1)",
pointStrokeColor: "#c1c7d1",
pointHighlightFill: "#fff",
pointHighlightStroke: "rgba(220,220,220,1)",
data: [65, 59, 80, 81, 56, 55, 40]
},
]
};
var areaChartOptions = {
//Boolean - If we should show the scale at all
showScale: true,
//Boolean - Whether grid lines are shown across the chart
scaleShowGridLines: false,
//String - Colour of the grid lines
scaleGridLineColor: "rgba(0,0,0,.05)",
//Number - Width of the grid lines
scaleGridLineWidth: 1,
//Boolean - Whether to show horizontal lines (except X axis)
scaleShowHorizontalLines: true,
//Boolean - Whether to show vertical lines (except Y axis)
scaleShowVerticalLines: true,
//Boolean - Whether the line is curved between points
bezierCurve: true,
//Number - Tension of the bezier curve between points
bezierCurveTension: 0.3,
//Boolean - Whether to show a dot for each point
pointDot: false,
//Number - Radius of each point dot in pixels
pointDotRadius: 4,
//Number - Pixel width of point dot stroke
pointDotStrokeWidth: 1,
//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
pointHitDetectionRadius: 20,
//Boolean - Whether to show a stroke for datasets
datasetStroke: true,
//Number - Pixel width of dataset stroke
datasetStrokeWidth: 2,
//Boolean - Whether to fill the dataset with a color
datasetFill: true,
//String - A legend template
legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
//Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
maintainAspectRatio: true,
//Boolean - whether to make the chart responsive to window resizing
responsive: true
};
areaChart.Line(areaChartData, areaChartOptions);
});
</script>
@endsection