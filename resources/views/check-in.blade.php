<!DOCTYPE html>
<html lang="en">
<html>
<head>
  <title>Check In | getmypass</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
  <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/bootstrap/css/bootstrap.min.css">
  <div class="row">
    <div class="col-md-offset-3 col-md-6 col-xs-12 text-center">
      <h3>getmypass | Check-In Tool</h3>
      <video style="width: 100%;" autoplay id="video"></video>
      <br>
      <div class="text-center">
        <div id="verification"></div>
        <button class="btn btn-success btn-lg" id="restart">Restart</button>
        <button class="btn btn-primary btn-lg" id="reset">Reset</button>
        <button class="btn btn-danger btn-lg" id="stop" >Stop</button>
      </div>
    </div>
    

  </div>
  
  <!-- <script src="{{ asset('vendor/adminlte') }}/plugins/jQuery/jQuery-2.2.3.min.js"></script> -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="build/qcode-decoder.min.js"></script>
  <script type="text/javascript">

    (function () {
      'use strict';

      window.onload = function() {

      // Normalize the various vendor prefixed versions of getUserMedia.
        navigator.getUserMedia = (navigator.getUserMedia ||
                                  navigator.webkitGetUserMedia ||
                                  navigator.mozGetUserMedia || 
                                  navigator.msGetUserMedia);

      };

      if (navigator.getUserMedia) {
        navigator.getUserMedia(
          {
            video: true
          },
          function(localMediaStream) {
            var vid = document.getElementById('video');
            vid.src = window.URL.createObjectURL(localMediaStream);
          },
          function(err) {
            console.log('The following error occurred when trying to use getUserMedia: ' + err);
          }
        );
      } else {
        alert('Sorry, your browser does not support getUserMedia');
      }


/*      if (!(qr.isCanvasSupported() && qr.hasGetUserMedia())) {
        alert('Your browser doesn\'t match the required specs.');
        throw new Error('Canvas and getUserMedia are required');
      }
*/
      var qr = new QCodeDecoder();
      var video = document.querySelector('video');
      var reset = document.querySelector('#reset');
      var stop = document.querySelector('#stop');


      $("#restart").click(function(){
        $('#video').show();
        $('#verification').hide();
      });

      function resultHandler (err, result) {

        if (err)
          return console.log(err.message);
        // 
        var result = result;
        $.ajax({
            url:"check-in", //the page containing php script
            type: "POST", //request type,
            dataType: 'json',
            data: { reference_id: result, },
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success:function(response){

              $('#video').hide();

              $('#verification').show();
              $('#verification').html(response.response);
            }
          });
      }

    // prepare a canvas element that will receive
    // the image to decode, sets the callback for
    // the result and then prepares the
    // videoElement to send its source to the
    // decoder.

    qr.decodeFromCamera(video, resultHandler);


    // attach some event handlers to reset and
    // stop whenever we want.

    reset.onclick = function () {
      qr.decodeFromCamera(video, resultHandler);
    };

    stop.onclick = function () {
      qr.stop();
    };

  })();
</script>
</body>
</html>
