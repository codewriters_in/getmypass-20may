@extends('f_layout')

@section('content')
<div class="static-slider10" style="background-image:url({{asset($eventData->image)}})">
	<div class="container">
		<!-- Row  -->
		<div class="row justify-content-center ">
			<!-- Column -->
			<div class="col-md-6 align-self-center text-center" data-aos="fade-down" data-aos-duration="1200">
				<span class="label label-rounded label-inverse">getmypass</span>
				<h1 class="title">{{$eventData->title}}</h1>
				<h4 class="subtitle font-light">{{$eventData->venue_name}}</h4>
			</div>
			<!-- Column -->

		</div>
	</div>
</div>
<br/>
<div class="container">
	<div class="row">
		<div class="col-md-5">
			<div class="card card-shadow">
				<div class="card-body">
					<h3 class="card-title">Event Description</h3>
					<p class="card-text"><b>{{ $eventData->title }}</b></p>
					<p class="card-text">{!! $eventData->description !!}</p>
					<a href="#" class="btn btn-success">Visit Organiser's Page</a>
				</div>
			</div>
		</div>
		<div class="col-md-7">
			<div class="card card-shadow">
				<div class="card-body">
					<h3 class="card-title">Event Details</h3>
					<p class="card-text">Venue Name: <br/><b>{{ $eventData->venue_name}}</b></p>
					<p class="card-text">Venue Address: <br/><b>{{ $eventData->location_address_line_1}}, {{ $eventData->location_address_line_2}}, {{ $eventData->location_state}}, PIN - {{ $eventData->location_post_code}}</b></p>
					<p class="card-text">From <b>{{ $eventData->start_date->format('M d Y g:i A') }}</b> to <b>{{ $eventData->end_date->format('M d Y g:i A') }}</b></p>
					
					<a href="#" class="btn btn-primary">View Map</a>
				</div>
			</div>
		</div>
	</div>
</div>
<br>
<div class="pricing8 b-t">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8 text-center">
				<h3 class="title">Tickets</h3>
				<h6 class="subtitle">We offer 100% satisafaction and Money back Guarantee</h6>

			</div>
		</div>
			<!-- row  -->
		<div class="row m-t-40">
				<!-- column  -->
			<div class="col-md-2"></div>
			<div class="col-md-8 pricing-box align-self-center">
				@foreach($eventData->ticket as $data)
					<div class="card b-all">
						<div class="card-body p-15">
							<form class="form-horizontal" role="form" action="{{ url('/cart') }}" method="POST">
								{!! csrf_field() !!}
								{{ Form::hidden('id', $data->id) }}
								{{ Form::hidden('name', $eventData->title) }}
								{{ Form::hidden('price', $data->price) }}
								{{ Form::hidden('event_id', $data->event_id) }}
								<div class="row">
									<div class="col-md-9">
										<div class="row">
											<div class="col-md-6">
												<h5>{{ucfirst($eventData->title)}}</h5>
												<span class="text-dark display-7">₹ {{$data->price}}</span>
											</div>
											<div class="col-md-6">
												<h5>{{ucfirst($data->ticket_type)}}</h5>
												<h6>Entry for {{$data->no_of_person}}</h6>
											</div>
										</div>
									</div>
									<div class="col-md-3 m-t-40 pull-right">
										@if($eventData->start_date > Carbon\Carbon::now())
											<button type="submit" class="btn btn-success-gradiant btn-arrow ">Add To Cart</button>	
										@else
											Expired!
										@endif	
									</div>
								</div>
							</form>
						</div>

					</div>
				@endforeach
			</div>
		<div class="col-md-2"></div>
					<!-- column  -->
		</div>   
	</div>
</div>
@endsection