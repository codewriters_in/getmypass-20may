@extends('f_layout')
@section('content')
<div class="new-slider" style="background-image:url({!! asset('assets/images/static-slider/slider10/img1.jpg') !!})">
	<div class="container">
		<!-- Row  -->
		<div class="row justify-content-center ">
			<div class="col-md-8 text-center">
				<h3 class="title">Find your event</h3>
				<h6 class="subtitle">Enter your location & month to browse events around you.</h6>
			</div>
			<div class="row">
				<div class="col-md-12">
					<form class="row">
						<div class="form-group col-md-4">
							<input type="text" class="form-control" id="state" placeholder="Region/State">
						</div>
						<div class="form-group col-md-4">
							<input type="city" class="form-control" id="city" placeholder="City">
						</div>
						<div class="form-group col-md-4">
							<input type="choose-month" class="form-control" id="month" placeholder="Choose Month">
						</div>
						<div class="col-md-12 text-center">
							<br>
							<button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
							<button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<br>
<div>
<div class="container">
	<div class="row justify-content-center">
		<!-- Column -->
		<!-- Column -->
	</div>
	<br>
	<div class="container">
		
		<hr>
		<div class="row justify-content-center">
			<!-- Column -->
			
			<!-- Column -->
		</div>
		<br/>
		<div class="row justify-content-center">
			<button type="button" class="btn btn-sm waves-effect waves-light btn-rounded btn-secondary">All Events</button>
			&nbsp;
			<button type="button" class="btn btn-sm waves-effect waves-light btn-rounded btn-secondary">This Week</button>
			&nbsp;
			<button type="button" class="btn btn-sm waves-effect waves-light btn-rounded btn-secondary">This Month</button>
		</div>
		<div class="row m-t-40 blog-home2">
			<!-- Column -->
			@foreach($eventData as $eventData)
			<div class="col-md-4">
				<div class="card" data-aos="flip-left" data-aos-duration="1200">
					<a href="#"><img class="card-img-top" src="{!! asset('assets/images/blog/blog-home/img3.jpg') !!}" alt="wrappixel kit"></a>
					<div class="date-pos bg-info-gradiant">{{ $eventData->start_date->format('M') }}<span>{{ $eventData->start_date->format('d') }}</span></div>
					<h5 class="font-medium m-t-30"><a href="#" class="link">{{ $eventData->title }}</a></h5>
					{{ $eventData->venue_name}}<br><b>Starting from Rs. {{ $eventData->getTicket->min('price') }}</b>
					<p class="m-t-20">{!! $eventData->description !!}</p>
					<a href="{{ route('page.event.show', $eventData->id) }}"><button class="btn btn-sm btn-secondary">View Details <i class="fa fa-chevron-right"></i></button></a>
				</div>
			</div>
			@endforeach
			
		</div>
		<nav aria-label="...">
			<ul class="pagination">
				<li class="page-item disabled">
					<a class="page-link" href="#" tabindex="-1">Previous</a>
				</li>
				<li class="page-item"><a class="page-link" href="#">1</a></li>
				<li class="page-item"><a class="page-link" href="#">2</a></li>
				<li class="page-item"><a class="page-link" href="#">3</a></li>
				<li class="page-item">
					<a class="page-link" href="#">Next</a>
				</li>
			</ul>
		</nav>
	</div>
</div>
@endsection