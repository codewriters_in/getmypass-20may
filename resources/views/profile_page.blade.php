@extends('f_layout')

@section('content')
<div class="new-slider" style="background-image:url({!! asset('assets/images/static-slider/slider10/img1.jpg') !!})">
	<div class="container">
		<!-- Row  -->
		<div class="row justify-content-center ">
			<!-- Column -->
			<div class="col-md-6 align-self-center text-center" data-aos="fade-down" data-aos-duration="1200">
				<span class="label label-rounded label-inverse">profile</span>
				<h1 class="title">My Account</h1>
			</div>
			<!-- Column -->

		</div>
	</div>
</div>

<div class="contact1">
	<div class="row">
		<div class="container">
			<div class="spacer">
				<div class="row m-0">
					<div class="col-lg-7">
						<div class="contact-box p-r-40">
							<h3 class="title">My Account</h3>
							<form class="form-horizontal" role="form" method="POST" action="{{ route('create_user') }}">
								{!! csrf_field() !!}

								<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
									<label class="col-md-4 control-label">{{ trans('backpack::base.name') }}</label>

									<div class="col-md-10">
										<input type="text" class="form-control" name="name" value='{{Auth::User()->name}}'>

										@if ($errors->has('name'))
										<span class="help-block">
											<strong>{{ $errors->first('name') }}</strong>
										</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
									<label class="col-md-4 control-label">{{ trans('backpack::base.email_address') }}</label>

									<div class="col-md-10">
										<input type="email" class="form-control" name="email" value='{{Auth::User()->email}}'>

										@if ($errors->has('email'))
										<span class="help-block">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
										@endif
									</div>
								</div>



							<!--	<div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
									<label class="col-md-4 control-label">Old Password</label>

									<div class="col-md-10">
										<input type="password" class="form-control" name="old_password">

										@if ($errors->has('old_password'))
										<span class="help-block">
											<strong>{{ $errors->first('old_password') }}</strong>
										</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
									<label class="col-md-4 control-label">New Password</label>

									<div class="col-md-10">
										<input type="password" class="form-control" name="new_password">

										@if ($errors->has('new_password'))
										<span class="help-block">
											<strong>{{ $errors->first('new_password') }}</strong>
										</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
									<label class="col-md-4 control-label">{{ trans('backpack::base.confirm_password') }}</label>

									<div class="col-md-10">
										<input type="password" class="form-control" name="password_confirmation">

										@if ($errors->has('password_confirmation'))
										<span class="help-block">
											<strong>{{ $errors->first('password_confirmation') }}</strong>
										</span>
										@endif
									</div>
								</div>-->

								<div class="form-group">
									<div class="col-md-10">
										<button type="submit" class="btn btn-primary">
											<i class="fa fa-btn fa-user"></i> {{ trans('backpack::base.register') }}
										</button>
									</div>
								</div>
							</form>
							
							
							<div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <h3 class="title">Change password</h3>

                <div class="panel-body">
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                    <form class="form-horizontal" method="POST" action="{{ route('changePassword') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                            <label for="new-password" class="col-md-12 control-label">Current Password</label>

                            <div class="col-md-12">
                                <input id="current-password" type="password" class="form-control" name="current-password" required>

                                @if ($errors->has('current-password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('current-password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                            <label for="new-password" class="col-md-12 control-label">New Password</label>

                            <div class="col-md-12">
                                <input id="new-password" type="password" class="form-control" name="new-password" required>

                                @if ($errors->has('new-password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('new-password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="new-password-confirm" class="col-md-12 control-label">Confirm New Password</label>

                            <div class="col-md-12">
                                <input id="new-password-confirm" type="password" class="form-control" name="new-password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Change Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection