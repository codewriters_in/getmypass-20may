@extends('frontend.layouts.master')
@section('content')
<section id="subheader" {{--style="background-image: url({{asset('assets/images/static-slider/slider10/img1.jpg')}});"--}}>
	<div class="container">
	  <div class="row">
	    <div class="col-md-12">
	      <h1>
	        Order
	      </h1>
	    </div>
	    
	    <!-- devider -->
	    <div class="col-md-12">
	      <div class="devider-page">
	        <div class="devider-img-right">
	        </div>
	      </div>
	    </div>

	    <div class="col-md-12">
	      <ul class="subdetail">
	        <li>
	          <a href="{{route('home')}}">Home</a>
	        </li>

	        <li class="sep">/
	        </li>

	        <li>Order
	        </li>
	      </ul>
	    </div>
	  </div>
	</div>
</section>
<section class="whitepage">
	<div class="row">
		<div class="container">
			<div class="spacer">
				<div class="row m-0">
					<div class="col-lg-7">
						<div class="contact-box p-r-40">
							<div class="col-md-12">
								@if(Auth::User())
									<h3 class="title">Your Details</h3>
									<p>Name: <b>{{Auth::User()->name }}</b></p>
									<p>Email ID: <b>{{Auth::User()->email }}</b></p>
									<p>Phone Number: <b>{{Auth::User()->phone }}</b></p>
									<form action="{{ (Cart::total() == 0) ? route('payment.zero_payment') : url(env('PAYMENT_URL')) }}" method="POST">
										{!! csrf_field() !!}
										<input type="hidden" name="checksum" value="{{$data['checksum']}}">
										<input type="hidden" name="merchant_name" value="{{$data['merchant_name']}}">
										<input type="hidden" name="merchant_message" value="{{$data['merchant_message']}}">
										<input type="hidden" name="merchant_txn_id" value="{{$data['merchant_txn_id']}}">
										<input type="hidden" name="amount" value="{{$data['amount']}}">
										<input type="hidden" name="mobile" value="{{$data['mobile']}}">
										<input type="hidden" name="callbackurl" value="{{$data['callbackurl']}}">
										<input type="submit" class="btn btn-danger-gradiant m-t-20 btn-arrow" value="Proceed To Payment">
									</form>
								
								@else
									<p><a href="{{URL::to('login')}}">Login</a> or Checkout as guest.</p>
									<h3 class="title">Your Details</h3>
									<form class="form-horizontal" data-aos="fade-left" data-aos-duration="1200" method="POST" action="{{ route('post.create_user') }}">
										{!! csrf_field() !!}
										<div class="row">
											<div class="col-lg-12">
												<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} m-t-15">
													<label for="name">Name</label>
													{{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Enter the name', 'required'])}}
													@if ($errors->has('name'))
													<span class="help-block">
														<strong>{{ $errors->first('name') }}</strong>
													</span>
													@endif
												</div>
											</div>
											<div class="col-lg-12">
												<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} m-t-15">
													<label for="email">Email ID</label>
													{{Form::email('email', '', ['class' => 'form-control', 'placeholder' => 'Enter the email address', 'required'])}}
													@if ($errors->has('email'))
													<span class="help-block">
														<strong>{{ $errors->first('email') }}</strong>
													</span>
													@endif
												</div>
											</div>
											<div class="col-lg-12">
												<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }} m-t-15">
													<label for="phone">Phone Number</label>
													{{Form::tel('phone', '', ['class' => 'form-control', 'placeholder' => 'Enter the phone number', 'required'])}}
													@if ($errors->has('phone'))
													<span class="help-block">
														<strong>{{ $errors->first('phone') }}</strong>
													</span>
													@endif
												</div>
											</div>
											<div class="col-lg-12">
												<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} m-t-15">
													<label for="password">Password</label>
													<input type="password" class="form-control" name="password" placeholder="Enter the password" minlength="5" maxlength="12" required>
													@if ($errors->has('password'))
													<span class="help-block">
														<strong>{{ $errors->first('password') }}</strong>
													</span>
													@endif
												</div>
											</div>
											<div class="col-lg-12">
												<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} m-t-15">
													<label for="password_confirmation">Password</label>
													<input type="password" class="form-control" name="password_confirmation" placeholder="Confirm the password" minlength="5" maxlength="12" required>
													@if ($errors->has('password'))
													<span class="help-block">
														<strong>{{ $errors->first('password') }}</strong>
													</span>
													@endif
												</div>
											</div>
											<div class="col-lg-12">
												<button type="submit" class="btn btn-danger-gradiant m-t-20 btn-arrow">
													<span>
														@php echo Cart::instance('default')->subtotal() > 0 ? "Proceed To Payment" : "Confirm guest booking" @endphp 
														<i class="ti-arrow-right"></i>
													</span>
												</button>
											</div>
										</div>
									</form>
								@endif
							</div>
						</div>

					</div>
					<div class="col-lg-5">
						@if(Cart::instance('default')->subtotal() > 0)
							<div class="detail-box p-40 bg-info" data-aos="fade-right" data-aos-duration="1200">
								<h3 class="text-white">Order Summary</h3>
								@foreach (Cart::content() as $item)
								<p class="text-white m-t-30 op-8">{{ $item->name }} [Rs. {{ $item->subtotal }}] x {{ $item->qty }}</p>
								@endforeach
								<!--<p class="text-white op-8">Ticket 2 [Rs.400] x2</p>-->
								<h5 class="text-white">	<b style="float: right;">Subtotal:   ₹ {{ Cart::instance('default')->subtotal() }}</b><br><br>
								{{-- <b style="float: right">GST:   ₹ {{ Cart::instance('default')->tax() }}</b><br><br> --}}
								<b style="float: right">
								<b>Your Total:   ₹ {{ Cart::total() }}</b>
								</b></h5>
								
								
								
								
								
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection