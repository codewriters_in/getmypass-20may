<header class="main-header">
 <nav class="navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
        </li>

    </ul>
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
    @if(Auth::guard('super')->check() && count($notifications) > 0)
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">{{count($notifications)}}</span>
            </a>
            <ul class="dropdown-menu">
                <li class="header">You have {{count($notifications)}} notifications</li>
                <li>
                    <ul class="menu">

                    @foreach($notifications as $key => $notification)
                        @if(isset($notification['email']) ) {{-- # Checking the array is model PaymentRequest  --}}   

                            <li><a href="{{route('super.orgs')}}"> You have organizer joining request by  {{$notification['name']}}</a></li>

                        @elseif(isset($notification['bank_detail_id']))

                            <li><a href="{{route('payment.all_request')}}"> You have withdrawal request of ₹ {{$notification['beneficiary_amount']}}</a></li>
                        @elseif(isset($notification['venue_name']))
                            <li><a href="{{route('super.events')}}"> You have approval request for {{ucfirst($notification['title'])}}</a></li>
                        @endif
                    @endforeach
                    </ul>
                </li>
            </ul>
          </li>
 
    @endif
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="fa fa-user"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <a href="#" class="dropdown-item">
                    <i class="fa fa-user mr-2"></i> Profile
                </a>
                <div class="dropdown-divider"></div>
                <a href="{{route('logout_user')}}" class="dropdown-item">
                    <i class="fa fa-sign-out mr-2"></i> Signout
                </a>
            </div>
        </li>
    </ul>
</nav>
</header>