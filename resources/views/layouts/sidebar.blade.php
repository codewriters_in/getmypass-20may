<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{!! route('super.dashboard') !!}" class="brand-link">
        <img src="{!!asset('super1/img/logo.png')!!}" alt="Laravel Starter" class="brand-image img-circle elevation-3"
        style="opacity: .8">
        <span class="brand-text font-weight-light">Get My Pass</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{!!asset('super1/img/profile.png')!!}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block"> {!!Auth::guard('super')->user()->name !!} </a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item has-treeview ">
                    <a href="{!! route('super.dashboard') !!}" class="nav-link ">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                    
                    
                </li>
                <li class="nav-item">
                    <a href="{!! route('super.orgs')!!}" class="nav-link">
                        <i class="nav-icon fa fa-users"></i>
                        <p>
                            Organizers
                            
                        </p>
                    </a>
                </li>
            </li>
            <li class="nav-item">
                <a href="{!! route('super.users')!!}" class="nav-link">
                    <i class="nav-icon fa fa-users"></i>
                    <p>
                        Users
                        
                    </p>
                </a>
            </li>
        </li>
        <li class="nav-item">
            <a href="{!! route('super.events')!!}" class="nav-link">
                <i class="nav-icon fa fa-star"></i>
                <p>
                    Events
                    
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('super.order.index')}}" class="nav-link">
                <i class="nav-icon fa fa-first-order"></i>
                <p>
                    Orders
                    
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{!! route('payment.all_request')!!}" class="nav-link">
                <i class="nav-icon fa fa-money"></i>
                <p>
                    Payment Requests
                    
                </p>
            </a>
        </li>
    </li>
    <li class="nav-item">
        <a href="{!! route('super.cities')!!}" class="nav-link">
            <i class="nav-icon fa fa-map-marker"></i>
            <p>
                Cities
                
            </p>
        </a>
    </li>
    <li class="nav-item has-treeview">
        <a href="#" class="nav-link">
            <i class="nav-icon fas fa-chart-pie"></i>
            <p>
                Reports
                <i class="right fa fa-angle-left"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="fa fa-circle-o nav-icon"></i>
                    <p>Daily Report</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="fa fa-circle-o nav-icon"></i>
                    <p>Monthly Report</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="fa fa-circle-o nav-icon"></i>
                    <p>Yearly Report</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="fa fa-circle-o nav-icon"></i>
                    <p>Custom Report</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="fa fa-circle-o nav-icon"></i>
                    <p>Organizer Report</p>
                </a>
            </li>
        </ul>
    </li>
</ul>
</nav>
<!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>